import jquery from 'jquery';

window.$ = window.jQuery = jquery;
require('@fancyapps/fancybox');
require('../../../../node_modules/bootstrap/dist/js/bootstrap.min');
require('../../../../node_modules/bootstrap-select/js/bootstrap-select');
require('../../../../node_modules/node-waves/dist/waves');
require('../../../../node_modules/tablesorter/dist/js/jquery.tablesorter');
require('../../../../node_modules/tablesorter/dist/js/jquery.tablesorter.widgets.min');
require('../../../../node_modules/tablesorter/dist/js/widgets/widget-pager.min');
require('../../../../node_modules/jquery-ui-bundle/jquery-ui');
require('../../../../node_modules/jquery-ui-touch-punch/jquery.ui.touch-punch');
require('../../mics/js/app');
require('../../../../node_modules/jquery.maskedinput/src/jquery.maskedinput');
// Import TinyMCE
import tinymce from 'tinymce/tinymce';

// Default icons are required for TinyMCE 5.3 or above
import 'tinymce/icons/default';

// A theme is also required
import 'tinymce/themes/silver';

// Any plugins you want to use has to be imported
import 'tinymce/plugins/paste';
import 'tinymce/plugins/link';
import 'tinymce/plugins/code';
import 'tinymce/plugins/table';

import {jsend} from "../../mics/js/app";

$(function () {


    $('.mask-tel').mask("+7\(999\)999-99-99", {placeholder: " "});

    Waves.attach('.flat-buttons', ['waves-button']);
    Waves.init();
    tinymce.init({
        selector: '.editor',
        menubar: false,
        plugins: ['paste', 'link', 'code', 'table'],
        toolbar: 'undo redo | formatselect | ' +
            'bold italic backcolor | alignleft aligncenter ' +
            'alignright alignjustify | bullist numlist outdent indent | ' +
            'removeformat | code | table tabledelete | tableprops tablerowprops tablecellprops | tableinsertrowbefore tableinsertrowafter tabledeleterow | tableinsertcolbefore tableinsertcolafter tabledeletecol',
    });


    $(document).on("click", ".clickNexts", function () {
        location.href = $(".nextlink", this).prop("href");
    });


    window.init_maps = function () {

        $("textarea").each(function () {

            if ($(this).css("display") == "block") {
                tinymce.init({
                    selector: '.editor',
                    menubar: false,
                    plugins: ['paste', 'link', 'code'],
                    toolbar: 'undo redo | formatselect | ' +
                        'bold italic backcolor | alignleft aligncenter ' +
                        'alignright alignjustify | bullist numlist outdent indent | ' +
                        'removeformat | code',
                });
            }
        });
    }

    window.control = false;
    $(document).on("click", ".add_test", function () {

        var $this = $(this);
        window.control = true;
        jsend($this.data("url"), {}, '$(".q_main").append(data);window.init_maps();window.control=false;', "window.control=false;");


    });


    $(document).on("click", ".filechoose_clear", function () {
        var $this = $(this);
        $this.closest(".form-link").remove();
    });


    window.control2 = false;
    $(document).on("click", ".add_test_otvet", function () {
        var $this = $(this);
        window.control2 = true;
        var id_rand = $(".ot_list", $this.closest(".language_grup_hid")).prop("id");


        jsend($this.data("url"), {
            "type": $this.data("type"),
            "id": $this.data("id")
        }, '$("#' + id_rand + '").append(data);window.control2=false;', "window.control2=false;");
    });


    $(document).on("click", "#next_btn", function () {
        $("#send_form_check").submit();
    });


    window.ckis = function () {
        var dataid = [];
        $(".is_ckeck").each(function () {
            if (dataid.indexOf($(this).data("grup")) == -1) {
                dataid.push($(this).data("grup"));
            }
        })


        $.each(dataid, function (index, el) {

            if ($(".is_ckeck[data-grup=" + el + "]:checked").length > 0) {
                $(".is_ckeck[data-grup=" + el + "]").prop("required", false);
            } else {
                $(".is_ckeck[data-grup=" + el + "]").prop("required", true);
            }

        })

    }

    setInterval(function () {
        window.ckis();
    }, 500);
    $(document).on("change", ".is_ckeck", function () {
        window.ckis();
    });

    $(document).on("change", ".checcontrol", function () {
        if ($("input:checked", $(this).closest(".answer_list")).length > 0) {
            $("input", $(this).closest(".answer_list")).prop("required", false);
        } else {
            $("input", $(this).closest(".answer_list")).prop("required", true);
        }
    });

    $(document).on("change", ".checcontrolas", function () {
        if ($("input:checked", $(this).closest(".answer_list")).length > 0) {
            $("input", $(this).closest(".answer_list")).prop("required", false);
        } else {
            $("input", $(this).closest(".answer_list")).prop("required", true);
        }
    });

    $(document).on("click", ".cliCurse", function () {
        $(this).addClass("current").siblings().removeClass("current")
        $(".cour_box .main_surce").removeClass("active").eq($(this).index()).addClass("active");

    });


    $(document).on("change", ".filecontrols", function () {
        if ($(this).get(0).files.length) {
            $(".boxLImaege", $(this).parent()).addClass("active");
        } else {
            $(".boxLImaege", $(this).parent()).removeClass("active");
        }
    });


    $(document).on("click", ".cli", function (e) {
        var
            $this = $(this),
            closest = $this.data("closest"),
            closestCli = $this.data("closestcli"),
            unDefain = $this.data("un"),
            target = $this.data("target"),
            $target = $this.data("target");
        if (typeof closest != "undefined") {
            $this = $($this.closest(closest));
        }
        if (typeof unDefain == "undefined") {
            unDefain = 0;
        } else {
            unDefain = 1;
        }
        if (typeof target != "undefined") {
            $target = $(target);
        }
        if (typeof closestCli != "undefined") {
            $(".cli", $this.closest(closestCli)).removeClass("active");
            $this.addClass("active");
        } else if (unDefain == 0) {
            if ($this.hasClass("active")) {
                $this.removeClass("active");
            } else {
                $this.addClass("active");
            }
        } else if (unDefain != 0) {
            $this.addClass("active").siblings().removeClass("active");
            if (typeof target != "undefined") {
                $target.eq($this.index() - 1).addClass("active").siblings().removeClass("active");
            }
        }
    });


    $(".multiselect").selectpicker({
        noneSelectedText: 'Ничего не выбрано'
    });
    $(".selectpicker").selectpicker({
        noneSelectedText: 'Ничего не выбрано'
    });


    $(document).on("click", ".quqis_remove", function () {
        $(this).closest(".quqis").remove();

    });

    $(document).on("click", ".openModel", function () {
        var
            $this = $(this);
        //     $target = $this.data("target"),
        //     $closest = $this.data("closest");
        //
        // if (typeof $closest != "undefined") {
        //     var
        //         $_closest = $($target, $($this.closest($closest)));
        //
        //     if ($_closest.length > 0) {
        //         $target = $_closest;
        //     }
        //
        // }
        //
        // $.fancybox.open($target.html());
        $this.fancybox({
            'type': 'iframe'
        });

        return false;

    });


    $(".form-link").each(function () {

        var
            $this = $(this);

        if ($(".input-line", $this).length == 0) {
            $this.append(" <div class='input-line'></div> ")
        }

    });


    $(".button-logout").on("click", function (event) {
        event.preventDefault();
        document.getElementById('logout-form').submit();
    });

//Table
//     $('.js-table').each(function (key, index) {
//         var
//             $this = $(this);
//         var $table = 'table_' + key;
//
//         $table = $('.js-table').DataTable({
//             // dom: 'Bfrltip',
//             dom: 'Bfrtip',
//             pageLength: 100,
//             // bPaginate: false,
//             buttons: [
//                 'copy', 'csv', 'excel', 'pdf', 'print'
//             ]
//         });
//
//         if (typeof $this.data('order-column') !== 'undefined' && typeof $this.data('order-type') !== 'undefined') {
//             $table.order([$this.data('order-column') - 1, $this.data('order-type')]).draw();
//         }
//     });


    var fixHelperModified = function (e, tr) {
        var $originals = tr.children();
        var $helper = tr.clone();
        $helper.children().each(function (index) {
            $(this).width($originals.eq(index).width());
        });

        return $helper;
    };
    var updateIndex = function (e, ui) {
        $('td.index', ui.item.parent()).each(function (i) {
            var position = i + 1;


            var id = $(this).data('id');

            console.log(id, "", position);

            $.ajax({
                url: '',
                method: 'POST',
                data: {id: id, position: position},
                headers: {
                    'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')
                },
                success: function (res) {
                },
                error: function (msg) {
                    console.log(msg);
                }
            });
        });

    };


    $(".sortable tbody").sortable({
        helper: fixHelperModified,
        stop: updateIndex,
        distance: 35,
        start: function (e, ui) {
            ui.placeholder.height(ui.helper.height());

        }
    });


    $(".sear_c").keyup(function () {
        var _this = this;

        $.each($(".table tbody tr"), function () {
            if ($(this).text().toLowerCase().indexOf($(_this).val().toLowerCase()) === -1) {
                $(this).hide();
            } else {
                $(this).show();
            }
        });
    });

    $(".sortTables")
        .tablesorter({
            widthFixed: true, widgets: ['zebra', "filter"],
            widgetOptions: {
                // filter_anyMatch replaced! Instead use the filter_external option
                // Set to use a jQuery selector (or jQuery object) pointing to the
                // external filter (column specific or any match)
                filter_external: '.search',
                // add a default type search to the first name column
                filter_defaultFilter: {1: '~{query}'},
                // include column filters
                filter_columnFilters: false,
                filter_placeholder: {search: 'ПОИСК...'},
            }
        });


    var update_position = function (e, ui) {
        $("li", $(ui.item).closest("ul")).each(function (i) {
            var position = i + 1;


            var id = $(this).data('id');

            console.log(id, "", position);

            $.ajax({
                url: $(".sort_data").data("url"),
                method: 'POST',
                data: {id: id, position: position},
                headers: {
                    'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')
                },
                success: function (res) {
                },
                error: function (msg) {
                    console.log(msg);
                }
            });
        });

    };
    $(".sort_data").sortable({
        stop: update_position
    });


});
