import jquery from 'jquery';
import Masonry from 'masonry-layout'
import Swiper from 'swiper'


window.$ = window.jQuery = jquery;
require('@fancyapps/fancybox');
require('../../../../node_modules/jquery-ui-bundle/jquery-ui');
import {init, mapinit} from '../../mics/js/mapsMy.js';
import {jsend, getFormData} from '../../mics/js/app';
//$.fancybox.open('htmlCode');


// var timer = null;
// var position = [[0, 0], [0, 0]];
// function moveNewPorisiton() {
//     var StepX = (position[1][0] - position[0][0]) / 10;
//     if (StepX > 0.5 || StepX < -0.5) {
//         position[0][0] += StepX;
//     }
//
//     var StepY = (position[1][1] - position[0][1]) / 10;
//     if (StepY > 0.5 || StepY < -0.5) {
//         position[0][1] += StepY;
//     }
//     if ((StepX > -0.5 && (StepY > -0.5)) && (StepX < 0.5 && (StepY < 0.5))) {
//         clearInterval(timer);
//         timer = null;
//     }
//     $(".point").css("transform", 'translate(' + position[0][0] + 'px, ' + position[0][1] + 'px)');
// }
// function moveControll() {
//     if (timer == null) {
//         timer = setInterval(moveNewPorisiton, 30);
//     }
// }
// $(function () {
//     $(document).on("mousemove", function (e) {
//         position[1][0] = (e.pageX)-($(".point").width()/2);
//         position[1][1] = (e.pageY)-($(".point").height()/2);
//         moveControll();
//     });
// });


$(document).on("click", ".btn-prev", function (e) {
    window.history.back();
});
$(document).on("click", ".cli", function (e) {

    var
        $this = $(this),
        closest = $this.data("closest"),
        closestCli = $this.data("closestcli"),
        unDefain = $this.data("un"),
        target = $this.data("target"),
        $target = $this.data("target");

    if (typeof closest != "undefined") {
        $this = $($this.closest(closest));
    }


    if (typeof unDefain == "undefined") {
        unDefain = 0;
    } else {
        unDefain = 1;
    }

    if (typeof target != "undefined") {
        $target = $(target);
    }


    if (typeof closestCli != "undefined") {


        $(".cli", $this.closest(closestCli)).removeClass("active");
        $this.addClass("active");


    } else if (unDefain == 0) {

        if ($this.hasClass("active")) {
            $this.removeClass("active");
        } else {
            $this.addClass("active");
        }

    } else if (unDefain != 0) {


        $this.addClass("active").siblings().removeClass("active");


        if (typeof target != "undefined") {


            $target.eq($this.index()).addClass("active").siblings().removeClass("active");

        }

    }
});

$(document).on("click", ".open", function (e) {
    var
        $this = $(this);
    var
        $target = $($this.data("target"));
    $.fancybox.open($target.html());

});


window.initMaps = function (data) {
    var cors = (data["city"]["coordinates"]).split(",");

    if (cors.length == 2) {
        window.maspInit.setCenter("maps", cors);

        $(".conshops").html("");
        ymaps.ready(function () {
            $.each(data["shop"], function (key, shop) {

                var shopSing = (shop["coordinates"].split(","));
                if (shopSing.length == 2) {
                    window.maspInit.addPointCustom("maps", {
                        "coordinates": shopSing,
                        "hintContent": shop["name"]
                    }, "/public/media/client/images/placeholder-filled-point.png");
                }

                $(".conshops").append('<a href="javascript:void(0)" class="li_maps openShop" data-id="' + shop["id"] + '"> <p> <img src="/public/media/client/images/placeholder-filled-point.png" alt=""> <span class="text text-s16"><b>' + shop["name"] + '</b></span> </p> </a>');

            });
            if (data["shop"].length == 0) {
                $(".conshops").append('<div class="li_maps" style="padding-left: 1rem;"> <p>  <span class="text text-s16"><b>Товар в этом городе не найден</b></span> </p> </div>');
            }
        });
        //
    }

}


window.sas = function (data) {

    $(".catalog-load").html(data);

}

window.shopSet = function (data) {

    if (data != "false") {

        $(".shopGet").html(data);
        $(".product_maps_nav").addClass("active");

    }


}

$(function () {

    $(document).on("click", "a[href*='#']", function () {
        $("body").removeClass("body_menu_open")
        $("body").removeClass("menu");
        $("html, body").animate({scrollTop: ($("#" + ($(this).prop("href").split('#')[1])).offset().top)}, 1300);

        return false;
    });

    if ($("*").is("#maps")) {
        ymaps.ready(function () {

            window.maspInit = new mapinit();
            window.maspInit.init("maps");
        });
    }


    $(document).on("click", ".data-selecters", function () {

        $("#selecters option").prop("selected", false);
        $("#selecters option[value=" + $(this).data("id") + "]").prop("selected", true);

    });

    $(document).on("click", ".prevBack", function () {
        $(".product_maps_nav").removeClass("active");
        $(".shopGet").html('');
    });

    $(document).on("click", ".menu_open", function () {
        $("body").addClass("body_menu_open");
    });

    $(document).on("click", ".close", function () {
        $("body").removeClass("body_menu_open");
    });


    $(document).on("click", ".openShop", function () {
        jsend($(".product_maps_nav").data("url"), {
            "id": $(this).data("id")
        }, "window.shopSet(data);");
    });

    $(document).on("change", ".selcity", function () {
        jsend($(this).data("url"), {
            "id": $(this).val(),
            "product_id": $("#city_select").data("product")
        }, "window.initMaps(data);");
    });


    $(document).on("submit", ".filters", function () {

        var $this = $(this);
        var inputs = getFormData($this);

        console.log(inputs);

        jsend($this.prop("action"), inputs, "window.sas(data);", "");

        return false;
    });

    $(".slider_ui").each(function () {


        var $this = $(this);
        var min = $this.data("min");
        var max = $this.data("max");
        var step = $this.data("step");

        $this.slider({
            min: min,
            max: max,
            step: step,
            create: function () {
                $(".custom-handle", $this).text($(this).slider("value"));
                $(".select_input", $this.parent()).val($(this).slider("value"));
            },
            slide: function (event, ui) {
                $(".custom-handle", $this).text(ui.value);
                $(".select_input", $this.parent()).val(ui.value);

            },
            stop: function (event, ui) {
                $this.closest("form").submit();
            }
        });

    });

    $(document).on("change", ".cheketitem input", function () {

        $(this).closest("form").submit();
    });

    $(".filters").submit();

    $('.swiper-container').each(function () {
        var
            $this = $(this),
            lg = $this.data("lg"),
            nb = $this.data("nb"),
            md = $this.data("md"),
            sm = $this.data("sm"),
            xs = $this.data("xs"),
            bw = $this.data("bw"),
            slidesPerColumn = $this.data("col"),
            pagination = $this.data("pagi"),
            directionData = $this.data("direction"),
            next = $this.data("next"),
            $nav = {};

        if (typeof next == "undefined") {
            next = false;
        }
        if (typeof directionData == "undefined") {
            directionData = "horizontal";

        }

        if (typeof pagination == "undefined") {
            pagination = "";
        }


        if (typeof lg == "undefined") {
            lg = 1;
        }

        if (typeof nb == "undefined") {
            nb = lg;
        }

        if (typeof md == "undefined") {
            md = nb;
        }

        if (typeof sm == "undefined") {
            sm = md;
        }

        if (typeof xs == "undefined") {
            xs = sm;
        }

        if (typeof bw == "undefined") {
            bw = 0;
        }
        if (typeof slidesPerColumn == "undefined") {
            slidesPerColumn = 1;
        }

        if (next == true) {
            $nav = {
                nextEl: $('.swiper-button-next', $(this).parent()),
                prevEl: $('.swiper-button-prev', $(this).parent()),
            };
        }

        var swiper = new Swiper(this, {
            slidesPerView: lg,
            spaceBetween: bw,
            slidesPerColumn: slidesPerColumn,
            direction: directionData,
            loop: false,
            lazy: true,
            navigation: $nav,
            slideToClickedSlide: true,


            pagination: {
                el: $('.swiper-pagination', $(this).parent()),
            },

            breakpoints: {
                2400: {
                    slidesPerView: lg,
                },
                1375: {
                    slidesPerView: nb,
                },
                1030: {
                    slidesPerView: md,
                },
                780: {
                    slidesPerView: sm,
                },
                640: {
                    slidesPerView: xs,
                }
            }
        });

        $this.addClass("active");
        if (pagination != "") {
            $(".swiper-pagination-bullet", $this).click(function () {
                swiper.slideTo($(this).index());
                $(this).addClass('active').siblings().removeClass("active");
            });
        }
    });


});



