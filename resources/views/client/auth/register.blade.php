@extends('auth.layouts.app')

@section('content')

<div class="container catalog">
    <div class="breadcrumbs text text-s12">

        <a href="/"> Главная</a> <i>⟩</i> <a href="/login">Логин</a> <i>⟩</i> <b>Регистрация</b>

    </div>
    <h1 class="titleh2 text text-s26">
        РЕГИСТРАЦИЯ
    </h1>
    <div class="bkSingPodloska bkSingPodloska-mark ">
        <div class="container_product">
            <div class="loginCOntrol">


                <div class="bulpForm ">

                    <div class="bulpForm_body ">
                        <div class="bulpForm_body_head">
                            <h2 class="msg text text-s18">ВВЕДИТЕ ВАШИ ДАННЫЕ</h2>
                        </div>
                        <p class="text text-s12 bulpForm_body_after" style="color: #ff0000;">
                            *Поля обязательные для заполнения
                        </p>


                        <form id="sign_in" role="form" class="bulpForm_body_form" method="POST" autocomplete="off"
                              action="{{ url('/register') }}">
                            {{ csrf_field() }}


                            <div class="formBing hiding active">

                                <div class="checjNedias" style="    margin-bottom: 0.5rem;">
                                    {{maskInputClient($errors,["name"=>"name","type"=>"text","placeholder"=>"Имя","auto"=>false])}}
                                    {{maskInputClient($errors,["name"=>"surname","type"=>"text","placeholder"=>"ФАМИЛИЯ","auto"=>false])}}
                                </div>
                                {{maskInputClient($errors,["name"=>"tel","type"=>"tel","placeholder"=>"ТЕЛЕФОН","auto"=>false])}}
                                <div class="checjNedias" style="    margin-bottom: 0.5rem;">
                                    {{maskInputClient($errors,["name"=>"email","type"=>"email","placeholder"=>"ЭЛ.
                                    АДРЕС","auto"=>false])}}
                                </div>
                                {{maskInputClient($errors,["name"=>"password","type"=>"password","placeholder"=>"Пароль","auto"=>false])}}
                                {{maskInputClient($errors,["name"=>"password_confirmation","type"=>"password","placeholder"=>"Повторите
                                пароль","auto"=>false])}}

                            </div>

                            <div class="codeSMS hiding">
                                <div class="form-group form-link   ">

                                    <label for="smscodedis" class="text text-s13 ">
                                        <p>
                                            SMS код <span class="warningDots"
                                                          style="color: #ff0000;align-self: flex-start;">*</span>
                                        </p>
                                    </label>
                                    <input type="text" name="sms_code"
                                           class="form-control text text-s13"
                                           id="smscodedis" value="" disabled>

                                </div>
                            </div>

                            <div class="form-group form-link">
                                <label class="text text-s13 "></label>
                                <button class="btn  btn-success flat-buttons   waves-effect" type="submit">Регистрация
                                </button>
                            </div>
                        </form>


                    </div>
                </div>


                <div class="bulpForm ">

                    <div class="bulpForm_body ">
                        <div class="bulpForm_body_head">
                            <h2 class="msg text text-s18">ВАШ ПИТОМЕЦ</h2>
                        </div>

                        <p class="text text-s12 bulpForm_body_after">
                            Получайте лучшиие товары для вшего питомца. Узнавайте об интересных предложениях и скидках.
                            <br>
                            <br>
                            Выберите ваши категорию.
                        </p>

                        <div class="categoriRegisterList">

                            <div class="catRegLink cli">
                                <div class="prop catRegLink_img">
                                    <div class="prop_img">
                                        <div class="prop_img_src"
                                             style="background-image: url('/public/media/client/img/iconOut/dog.png');">

                                        </div>
                                    </div>
                                </div>
                                <div class="catRegLink_title">
                                    <p class="text text-s15">
                                        Собаки
                                    </p>
                                </div>
                            </div>

                            <div class="catRegLink cli">
                                <div class="prop catRegLink_img">
                                    <div class="prop_img">
                                        <div class="prop_img_src"
                                             style="background-image: url('/public/media/client/img/iconOut/bird.png');">

                                        </div>
                                    </div>
                                </div>
                                <div class="catRegLink_title">
                                    <p class="text text-s15">
                                        Птицы
                                    </p>
                                </div>
                            </div>

                            <div class="catRegLink cli">
                                <div class="prop catRegLink_img">
                                    <div class="prop_img">
                                        <div class="prop_img_src"
                                             style="background-image: url('/public/media/client/img/iconOut/cat.png');">

                                        </div>
                                    </div>
                                </div>
                                <div class="catRegLink_title">
                                    <p class="text text-s15">
                                        Кошки
                                    </p>
                                </div>
                            </div>


                            <div class="catRegLink cli">
                                <div class="prop catRegLink_img">
                                    <div class="prop_img">
                                        <div class="prop_img_src"
                                             style="background-image: url('/public/media/client/img/iconOut/grizun.png');">

                                        </div>
                                    </div>
                                </div>
                                <div class="catRegLink_title">
                                    <p class="text text-s15">
                                        Маленькие питомцы
                                    </p>
                                </div>
                            </div>


                            <div class="catRegLink cli">
                                <div class="prop catRegLink_img">
                                    <div class="prop_img">
                                        <div class="prop_img_src"
                                             style="background-image: url('/public/media/client/img/iconOut/fish.png');">

                                        </div>
                                    </div>
                                </div>
                                <div class="catRegLink_title">
                                    <p class="text text-s15">
                                        Рыбы
                                    </p>
                                </div>
                            </div>


                        </div>


                    </div>
                </div>


            </div>


        </div>
    </div>
</div>

@endsection


