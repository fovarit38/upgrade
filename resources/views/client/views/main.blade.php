@extends('views.layouts.app')

@section('content')


    <div id="main" class="prop slider-gl" style="    border-bottom: 1px solid #eaeaea;">
        <div class="prop_img prop_img-45"
             style="background-image: url({!! s_("Слайдер картинка","Главная","https://img5.goodfon.ru/wallpaper/nbig/a/56/karandashi-skrepki-stepler-kantstovary.jpg","images") !!}); background-position: 31rem -14rem;">
            <div class="prop_img_src" style="background-image: url('/public/media/client/images/slider.png');">

                <div class="text-block">
                    <div class="text-block_main">
                        <div class="text-block_main_befor">
                            <p class="text text-s18">
                                {!! s_("Слайдер мини заголовок","Главная","У нас есть работа для вас!","") !!}
                            </p>
                        </div>
                        <div class="text-block_main_name">
                            <p class="text text-s68">
                                <b> {!! s_("Слайдер основной заголовок","Главная","Компания \"Алматы Кенсе\" Набирает сотрудников","") !!}</b>
                            </p>
                        </div>
                        <div class="text-block_main_after">
                            <p class="text text-s18">
                                {!! s_("Слайдер основное описание","Главная","Присоединяйся в компанию профессионалов в компанию с 28-летним стажем на рынке Казахстана","") !!}
                            </p>
                        </div>
                        <a href="{!! s_("Слайдер кнопка ссылка","Главная",url_custom("#request"),"") !!}" class="btn btn-def">
                            <span class="text text-s20">{!! s_("Слайдер кнопка текст","Главная","Присоединиться","") !!}</span>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <div id="compani" class="container">
        <div class="section">
            <div class="section_text">
                <div class="before">
                    <p class="text text-s16">
                        {!! s_("Второй блок мини заголовок","Главная","О КОМПАНИИ","") !!}
                    </p>
                </div>
                <div class="title title-p1">
                    <p class="text text-s42">
                        <b>
                            {!! s_("Второй блок основной заголовок","Главная","Компания \"Алматы Кенсе\"","") !!}
                        </b>
                    </p>
                </div>
                <div class="content">
                    <p class="text text-s16">
                        {!! s_("Второй блок основное описание","Главная","На сегодняший день наша компания - лидер на рынке канцелярский товаров Республики Казахстан","") !!}
                    </p>
                </div>

                <div class="out_list">
                    <div class="out_list_img">
                        <img src="{!! s_("Второй блок иконка 1","Главная","/public/media/client/images/icon/goal.png","images") !!}" alt="">
                    </div>
                    <div class="out_list_text">
                        <div class="title title-pb1">
                            <p class="text text-s25">
                                {!! s_("Второй блок иконка 1 Заголовок","Главная","Наша Миссия","") !!}
                            </p>
                        </div>
                        <p class="text text-s16">
                            {!! s_("Второй блок иконка 1 Описание","Главная","\"Алматы Кенсе\" предлагает своим клиентам конкурентоспособные цены, гибкие и удобные условия работы. Гарантируев качественный сервис","") !!}
                        </p>

                    </div>
                </div>
                <div class="out_list">
                    <div class="out_list_img">
                        <img src="{!! s_("Второй блок иконка 2","Главная","/public/media/client/images/icon/history.png","images") !!}" alt="">
                    </div>
                    <div class="out_list_text">
                        <div class="title title-pb1">
                            <p class="text text-s25">
                                {!! s_("Второй блок иконка 2 Заголовок","Главная","Успехи Компании","") !!}
                            </p>
                        </div>
                        <p class="text text-s16">
                            {!! s_("Второй блок иконка 2 Описание","Главная","\"Алматы Кенсе\" предлагает своим клиентам конкурентоспособные цены, гибкие и удобные условия работы. Гарантируев качественный сервис","") !!}
                        </p>
                    </div>
                </div>
                <div class="out_list_btn">
                    <a href="{!! s_("Второй блок кнопка ссылка","Главная",url_custom('/about'),"") !!}" class="btn btn-def">
                        <span class="text text-s20">{!! s_("Второй блок кнопка текст","Главная","Узнать больше","") !!}</span>
                    </a>
                </div>

            </div>
            <div class="section_img">
                <div class="prop">
                    <div class="prop_img prop_img-100">
                        <div class="prop_img_src"
                             style="background-image: url({!! s_("Второй блок картинка","Главная","/public/media/client/images/dedicated.jpg","images") !!});">

                            <div class="text-block text-block-center">
                                <div class="point-text">
                                    <div class="point-text_border">
                                        <div class="point-text_title">
                                            <p class="text text-s80">
                                                {!! s_("Второй блок цыфры","Главная","28","") !!}
                                            </p>
                                        </div>
                                        <div class="point-text_after">
                                            <p class="text text-s16">
                                                {!! s_("Второй блок текст","Главная","Лет безупречной работы","") !!}
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div id="gallery" class="gallery">
        <div class="gallery_head">
            <div class="title title-big">
                <span class="text text-s42"> {!! s_("Блок новости заголовок","Главная","Новости","") !!}</span>
            </div>
        </div>
        <div class="container">
            <div class="gallery_main">

                @foreach(\App\Gallery::get() as $gal)
                    <a href="{{url_custom('/news/'.$gal->id)}}"
                        class="prop img-gal">
                        <div class="prop_img prop_img-88">
                            <div class="prop_img_src"
                                 style="background-image: url('{{$gal->images}}');">

                            </div>
                        </div>
                        <div class="text-cs ">
                            <p  class="text text-s16">
                                {{$gal->name}}
                            </p>
                        </div>
                    </a>
                @endforeach

            </div>
        </div>
    </div>

    <div class="choose-us ">
        <div class="container">
            <div class="choose-us_head">
                <div class="title title-big">
                    <span class="text text-s42"> {!! s_("Блок 4 преимущества заголовок","Главная","Почему выбирают нас?","") !!}</span>
                </div>
                <div class="content content-big">
                    <span class="text text-s16">{!! s_("Блок 4 преимущества описание","Главная","Все наши кандидаты впечатлились нашим преимуществом","") !!}</span>
                </div>
            </div>
            <div class="choose-us_main">
                @for($i=0;$i<4;$i++)
                    <div class="work_item">
                        <div class="work_item_icon">
                            <i class="{!! s_("Блок 4 список ".($i+1)." иконка","Главная","icon-desktop","") !!}"></i>

                        </div>
                        <div class="work_item_name">
                            <h2 class="text text-s20">{!! s_("Блок 4 список ".($i+1)." Заголовок","Главная","Перспектива карьерного роста","") !!}</h2>
                        </div>
                        <div class="work_item_content">
                            <p class="text text-s16">
                                {!! s_("Блок 4 список ".($i+1)." Описание","Главная","Этот небольшой текст показывает как будет выглядеть описание данного преимущества","") !!}
                            </p>
                        </div>
                    </div>
                @endfor
            </div>
        </div>
    </div>

    @php
        $oficials=  \App\Official::where("set_kit","1")->get();
    @endphp

    <div id="request" class="request ">
        <div class="container">
            <div class="request_head">
                <div class="title title-big">
                    <span class="text text-s42">{!! s_("Блок 5 оставить заявку мини заголовок","Главная","Оставьте заявку и мы с вами свяжемся","") !!}</span>
                </div>
                <div class="content content-big">
                    <span class="text text-s16">{!! s_("Блок 5 оставить заявку заголовок","Главная","Наш менеджер свяжется с вами в течении одного рабочего дня","") !!}</span>
                </div>
            </div>
            <div class="request_main">
                <div class="request_main_detalis">
                    <div class="deserve-item">
                        <h3 class="text text-s32">{!! s_("Блок 5 оставить заявку заголовок о приёме на работу","Главная","Заголовок о приеме на работу","") !!}</h3>

                        <div class="steps-list">
                            <div class="text text-s16" style="margin-top: 2rem;">
                                {!! s_("Блок 5 оставить заявку описание о приёме на работу","Главная","демо текст","textarea") !!}
                            </div>
                        </div>


                    </div>
                </div>
                <div class="request_main_order">

                    <div class="form-request">
                        <div class="form-request_head">
                            <h3 class="title title-big text text-s32">
                                {!! s_("Блок 5 текст над формой","Главная","Заполнитекраткую форм","") !!}
                            </h3>
                        </div>
                        <form method="post" action="{{url_custom("/send_se")}}" class="form-request_main ">
                            @csrf
                            <div class="label">
                                <div class="label_name">
                                    <p class="text text-s16">
                                        {!! s_("Блок 5 форма ФИО","Главная","ФИО","") !!}
                                    </p>
                                </div>
                                <input type="text" name="fio" required class="label_input text text-s16">
                            </div>

                            <div class="label">
                                <div class="label_name">
                                    <p class="text text-s16">
                                        {!! s_("Блок 5 форма Телефон","Главная","Телефон","") !!}
                                    </p>
                                </div>
                                <input type="text" name="tel" required class="label_input text text-s16">
                            </div>
                            <div class="label">
                                <div class="label_name">
                                    <p class="text text-s16">
                                        {!! s_("Блок 5 форма E-mail","Главная","E-mail","") !!}
                                    </p>
                                </div>
                                <input type="email" name="email" required class="label_input text text-s16">
                            </div>

                            <div class="label">
                                <div class="label_name">
                                    <p class="text text-s16">
                                        {!! s_("Блок 5 форма Дата Рождения","Главная","Дата Рождения","") !!}
                                    </p>
                                </div>
                                <input type="date" name="data" required class="label_input text text-s16">
                            </div>

                            <div class="label">
                                <div class="label_name">
                                    <p class="text text-s16">
                                        {!! s_("Блок 5 форма Должность","Главная","Должность","") !!}
                                    </p>
                                </div>
                                @if(count($oficials)>0)
                                    <select id="selecters" name="oficials_id" required
                                            class="label_input text text-s16">
                                        <option value="">{!! s_("Блок 5 форма Выберите должность","Главная","Выберите должность","") !!}</option>
                                        @foreach($oficials as $ofucuak)
                                            <option value="{{$ofucuak->id}}">{{LC($ofucuak->name)}}</option>
                                        @endforeach
                                    </select>
                                @else
                                    <input type="text" name="oficials" required class="label_input text text-s16">
                                @endif
                            </div>
                            <div class="label">
                                <div class="label_name">
                                    <p class="text text-s16">
                                        {!! s_("Блок 5 форма Стаж работы","Главная","Стаж работы","") !!}
                                    </p>
                                </div>
                                <input type="text" name="sfera" required class="label_input text text-s16">
                            </div>


                            <button type="submit" class="btn btn-send">
                                <span class="text text-s16"> {!! s_("Блок 5 форма кнопка текст","Главная","Отправить заявку","") !!}</span>
                            </button>

                        </form>
                    </div>

                </div>
            </div>
        </div>
    </div>


    @if(count($oficials)>0)
        <div class="vacancy">
            <div class="vacancy_head">
                <div class="title title-big">
                    <span class="text text-s42">{!! s_("Блок 6 вакансии заголовок","Главная","Открытые вакансии","") !!}</span>
                </div>
                <div class="content content-big">
                    <span class="text text-s16">{!! s_("Блок 6 вакансии описание","Главная","Мы ждем ответсвенных сотрудников, желающих работать в команде","") !!}</span>
                </div>
            </div>
            <div class="vacancy_main">
                <div class="swiper-container" data-lg="4" data-sm="2" data-xs="1" data-bw="40">
                    <div class="swiper-wrapper">
                        @foreach($oficials as $ofucuak)
                            <div class="swiper-slide">
                                <div class="exp-pricing-plans-inner shadow-hover">
                                    <div class="exp-pricing-plans-top">
                                        <h6 class="text text-s20">{!! LC($ofucuak->name) !!}</h6>

                                        <div class="exp-price-mrp text text-s42">
                                            {!! LC($ofucuak->price) !!}
                                        </div>
                                    </div>

                                    <div class="exp-pricing-plans-mid">
                                        <div class="content text text-s16">
                                            {!! LC($ofucuak->content) !!}
                                        </div>
                                    </div>

                                    <div class="exp-pricing-btn">

                                        <a href="{{url_custom('#request')}}" data-id="{!! ($ofucuak->id) !!}"
                                           class="btn data-selecters theme-border-btn mx-auto"><span
                                                class="text text-s16">Оставить заявку</span>
                                            <i
                                                class="fas fa-long-arrow-alt-right"></i></a>

                                    </div>

                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    @endif

    <div class="team " style="display: none;">
        <div class="container">
            <div class="team_head">
                <div class="before before-team">
                    <p class="text text-s16">
                        КОМАНДА
                    </p>
                </div>
                <div class="title title-big">
                    <span class="text text-s42">Почему выбирают нас?</span>
                </div>
                <div class="content content-big">
                    <span class="text text-s16">Все наши кандидаты впечатлились нашим преимуществом</span>
                </div>
            </div>
            <div class="team_main">
                <div class="swiper-container" data-lg="3" data-sm="2" data-xs="1" data-bw="40">
                    <div class="swiper-wrapper">
                        @for($i=0;$i<8;$i++)
                            <div class="swiper-slide">
                                <div class="prop">
                                    <div class="prop_img prop_img-90">
                                        <div class="prop_img_src"
                                             style="background-image: url('/public/media/Update/image_jpeg/iS6bU_file.jpg');">

                                        </div>
                                    </div>
                                </div>
                            </div>
                        @endfor
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
