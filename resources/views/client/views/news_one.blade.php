@extends('views.layouts.app')

@section('content')


    <div class="choose-us " style="padding-top: 8rem;">
        <div class="container">
            <div class="choose-us_head" style=" text-align: left; ">
                <div class="title title-big">
                    <span class="text text-s42">{{$news->name}}</span>
                </div>
                <div class="content content-big" style="padding-top: 2rem;">
                    <div class="text text-s16">
                        {!! $news->content !!}
                    </div>
                </div>
            </div>

        </div>
    </div>


@endsection
