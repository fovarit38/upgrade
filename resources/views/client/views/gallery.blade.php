@extends('views.layouts.app')

@section('content')

<section class="gallery gallery-top container">
    <h2 class="text text-s25" style="margin-bottom: 2rem;"><span>Галерея</span></h2>
    <div class="gallery_main" style="padding:0;">
        @foreach(\App\Gallery::get() as $gal)
        <a href="{{$gal->images}}"
           data-fancybox="gallery" class="prop img-gal">
            <div class="prop_img prop_img-88">
                <div class="prop_img_src"
                     style="background-image: url('{{$gal->images}}');">

                </div>
            </div>
            <div class="text-cs ">
                <div class="text text-s16">
                    {{$gal->name}}
                </div>
            </div>
        </a>
        @endforeach
    </div>
</section>

@endsection
