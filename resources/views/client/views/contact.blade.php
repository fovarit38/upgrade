@extends('views.layouts.app')

@section('content')

<section class="contact container">
    <div class="contact_main" style="padding:0;">
        <div class="contact_text">
                <div class="text text-s25" style="padding:0 0 15px 0"><span>КОНТАКТЫ</span></div>
                <div class="text text-s16">Казахстан, город Алматы<br>Сейфуллина проспект 00<br>Офис 506</div>
                <div class="jockey"></div>
                <div class="text text-s16 stupid"><i class="fa fa-phone"></i>+7 (727) 300 44 96</div>
                <div class="text text-s16 stupid"><i class="fa fa-envelope"></i>mail@mail.ru</div>
        </div>
        <div class="contact_maps">
            <div class="prop">
                <div class="prop_img prop_img-75">
                    <div class="prop_img_src">
                        <div id="maps" ></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

@endsection
