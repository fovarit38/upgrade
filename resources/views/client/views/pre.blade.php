<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>

<div>
    <header>
        <div class="main_container">
            <img width="134" src="/public/media/client/images/logo_n.png"/>
        </div>
    </header>

    <div class="ban_s">
        <div class="ban_s_con main_container">
            <div class="ban_s_text">
                <div class="text" style="color: #fd0e82; font-size: 76px; font-family: 'DrukTextWideTT',Arial,sans-serif; line-height: 1.55; font-weight: bold;">
                    AVATARIYA
                </div>
                <div class="text" style="color: #000000; font-size: 22px; font-family: 'TildaSans',Arial,sans-serif; line-height: 1.55; font-weight: 500;">
                    <span style=" font-size: 24px;font-weight: bold;">UPGRADE</span> - система работы с персоналом компании
                </div>
                <div class="text" style="color: #000000; margin: 20px 0; margin-bottom: 45px; font-size: 18px; font-family: 'TildaSans',Arial,sans-serif; line-height: 1.55; font-weight: 500;">
                    AVATARIYA - это сеть activity- парков с ресторанами и залами проведения праздников для всей семьи, в
                    которых дети играются погружаясь в мир сказочных героев
                </div>
                <div class="btns">
                    <div class="btns_main">
                        <a href="{{url_custom('/admin')}}" class="btn_1">Войти</a>
                    </div>
                </div>
            </div>
            <div class="ban_s_img">
                <div class="ban_s_img_props">
                    <div class="ban_s_img_props_src ban_s_img_props_src-fon">
                        <div class="ban_s_img_props_src-im"></div>
                    </div>
                    <div class="ban_s_img_props_src ban_s_img_props_src-man"></div>
                </div>
            </div>
        </div>
    </div>
</div>

<style>
    header {
        position: absolute;
        width: 100%;
        z-index: 10;
        padding: 10px 0;
    }

    .btn_1 {
        color: #fcf9f9;
        font-size: 19px;
        font-family: 'TildaSans', Arial, sans-serif;
        line-height: 1.55;
        font-weight: 600;
        border-radius: 30px;
        background-color: #7ed957;
        background-position: center center;
        border-color: transparent;
        border-style: solid;
        transition: background-color 0.2s ease-in-out, color 0.2s ease-in-out, border-color 0.2s ease-in-out;
        height: 70px;
        box-shadow: 0 5px 0px 0px #4d8534;
        width: 183px;
        text-decoration: none;
        display: flex;
        align-items: center;
        justify-content: center;
        box-sizing: border-box;
    }

    .btn_1-2 {
        width: 296px;
        background-color: #fd0e82;
        box-shadow: 0 5px 0px 0px #a60c59;
        margin-left: 1.5rem;

    }

    .ban_s {
        position: relative;
        display: flex;
        flex-direction: column;
        width: 100%;
        background-image: url("/public/media/client/images/bg-2.webp");
        min-height: 50vh;
    }

    .ban_s_con {
        position: relative;
        flex-grow: 2;
        padding-top: 40px;

    }

    .ban_s_text {
        padding: 80px 0;
        width: 50%;
    }

    .ban_s_img {
        position: absolute;
        right: 0;
        top: 0;
        width: 55%;
        min-height: 100%;
        overflow: hidden;
        display: flex;
        align-items: flex-end;
    }

    .ban_s_img_props {
        width: 100%;
        padding-top: 88%;
        position: relative;
        overflow: hidden;
    }

    .ban_s_img_props_src {
        position: absolute;
        width: 100%;
        height: 100%;
        top: 0;
        left: 0;
        background-repeat: no-repeat;
        background-position: bottom center;
    }

    .ban_s_img_props_src-fon {
        top: 15%;
    }

    .ban_s_img_props_src-fon .ban_s_img_props_src-im {
        position: absolute;
        width: 100%;
        height: 100%;
        top: 0;
        left: 0;
        background-image: url("/public/media/client/images/blob.svg");
        transform: rotate(325deg);
        background-size: 130%;
        background-position: center;
    }

    .ban_s_img_props_src-man {
        background-image: url("/public/media/client/images/business-3d-business.webp");

    }

    .main_container {
        max-width: 1140px;
        margin: auto;
        width: 95%;
    }

    .btns {
        display: flex;
        width: 100%;
    }

    .btn {
        width: 320px;
        text-align: center;
        padding: 1.25rem 0;
        background-color: #0467ad;
        font-family: Arial;
        color: #fff;
        text-decoration: none;
    }

    .btn + .btn {
        margin-left: 1.5rem;
    }

    body {
        margin: 0;
        padding: 0;
    }

    .btns_main {
        display: flex;
        align-items: center;
    }
</style>

</body>
</html>
