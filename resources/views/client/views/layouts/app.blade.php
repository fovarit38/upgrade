<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>{{ config('app.name', 'Laravel') }} </title>

    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link rel="stylesheet" href="/public/media/client/css/style.css?v=0.3">
    <meta name="description" content="">
    <meta name="Keywords" content="">
    <!-- Bootstrap CSS -->
    <link href="https://fonts.googleapis.com/css2?family=Rubik:wght@300;400;500;700;900&display=swap" rel="stylesheet">
    <link rel="shortcut icon" href="/favicon.ico" type="image/x-icon">
    <style>
        .menu {
            position: fixed;
            height: 100%;
            width: 100%;
            z-index: 4;
            background-color: #fff;
            visibility: hidden;
            transform: translateX(-100%);
            transition: 0.6s;
        }
    </style>
</head>
<body>

@php
    $nav=[
      ["Главная",url_custom("#main")],
      ["О Компании",url_custom("/about")],
      ["Галерея",url_custom("/gallery")],
      ["Контакты",url_custom("/contact")],
    ];
@endphp
<div class="menu">
    <div class="container">
        <div class="menu_block">

            <a href="javascript:void(0)" class="close">
                <img src="/public/media/client/images/close.png" alt="">
            </a>


            <a href="/" class="logo">
                <img src="/public/media/client/images/logo.png" alt="">
            </a>

            <div class="login ">
                <a href="{{'/admin'}}"><span class="text text-s18">войти в систему</span></a>
            </div>
            <nav class="nav">
                <ul>
                    @foreach($nav as $n)
                        <li>
                            <a href="{{$n[1]}}">  <span
                                    class="text text-s25"> {{$n[0]}}</span>
                            </a>
                        </li>
                    @endforeach

                </ul>
            </nav>

        </div>
    </div>
</div>

<main>
    <header class="header">
        <div class="header_main">

            <div class="menu_click">
                <a href="javascript:void(0)" class="menu_icon menu_open">
                    <div></div>
                    <div></div>
                    <div></div>
                </a>
            </div>

            <a href="/" class="logo logo-head">
                <img src="/public/media/client/images/logo.png" alt="">
            </a>


            <nav class="nav nav-head">
                <ul>
                    @foreach($nav as $n)
                        <li>
                            <a href="{{$n[1]}}">  <span
                                    class="text text-s18"> {{$n[0]}}</span>
                            </a>
                        </li>
                    @endforeach
                </ul>
            </nav>

            <div class="phone">
                <div class="phone_icon">
                    <img src="/public/media/client/images/phone.png" alt="">
                </div>
                <div class="phone_call">
                    <div class="phone_call_name">
                        <p class="text text-s16">
                            Позвоните нам
                        </p>
                    </div>
                    <div class="phone_call_phone">
                        <a href="" class="text text-s18">
                            +7 708 968 1134
                        </a>
                    </div>
                </div>

            </div>

            <div class="login login-none">
                <a href="{{'/admin'}}"><span class="text text-s18">войти в систему</span></a>
            </div>

            <a href="{{url_custom("#request")}}" class="btn  btn-def ">
                <span class="text text-s20">Оставить заявку</span>
            </a>
        </div>
    </header>


    @yield('content')

    <footer id="footer" class="footer">
        <div class="container">
            <div class="footer_main">
                <div class="footer_main-left">
                    <div class="it-footer">
                        <div class="it-footer_head">
                            <p class="title text text-s18">
                                {!!  s_("О нас Заголовок","Подвал","Коротко о нас","textarea")!!}
                            </p>
                        </div>
                        <div class="it-footer_content text text-s16">

                            {!!  s_("О нас описание","Подвал",'<p class=""> Реквизиты ТОО "Fkvfns rtyct" </p>',"textarea")!!}
                        </div>
                    </div>
                </div>
                <div class="footer_main-right">
                    <div class="it-footer">
                        <div class="it-footer_head">
                            <p class="title text text-s18">
                                Навигация
                            </p>
                        </div>
                        <div class="it-footer_content it-footer_content-menu">
                            @foreach($nav as $n)
                                <a href="{{$n[1]}}">  <span
                                        class="text text-s16"> {{$n[0]}}</span>
                                </a>
                            @endforeach
                        </div>
                    </div>
                    <div class="it-footer">
                        <div class="it-footer_head">
                            <p class="title text text-s18">
                                {!!  s_("Заголовок Контакты/Адрес","Подвал","Коротко о нас","textarea")!!}
                            </p>
                        </div>
                        <div class="it-footer_content ppex">
                            <div class="text text-s16">
                                {!!  s_("Контакты/Адрес","Подвал","<p> Адрес улю Наурызбай </p> <p>Батыра, 99.1</p> <p></p> <p> Адрес улю Наурызбай </p> <p>Батыра, 99.1</p>","textarea")!!}
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </footer>
</main>


<script src="https://api-maps.yandex.ru/2.1/?lang=ru_RU" type="text/javascript"></script>
<script src="/public/media/client/js/app.js?v=0.3"></script>
<script>

    $(function () {
        <?php
        if(\Session::has('alert')){
        ?>
        $.fancybox.open('<?=Session::get('alert')?>');
        <?
        }
        ?>
    });

</script>
</body>
</html>
