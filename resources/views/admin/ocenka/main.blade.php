@extends('views.layouts.app')

@section('content')


<style>
    .main_box {
        background-image: url("/public/media/admin/img/ocenka/main.svg");
        background-repeat: no-repeat;
        background-position: 28rem 1rem;
        background-size: 50rem;
    }

    .main_box:before {
        content: '';
        position: absolute;
        left: 0;
        top: 0;
        width: 100%;
        height: 100%;
        /*background-image: url("/public/media/admin/img/recruitment/dots.svg");*/
        background-repeat: no-repeat;
        background-position: 23rem 10rem;
        background-size: 11rem;
    }

    .mains_controls {
        position: relative;
        width: 100%;
    }

    .mains_controls_text {
        font-size: 4.4rem;
        color: #5E24D4;
        line-height: 120%;
        font-weight: bold;
        position: absolute;
        left: 6rem;
        top: 6rem;
        text-transform: uppercase;
    }

    .btnsBox {
        margin-top: 35rem;
        position: relative;
        width: 100%;
        display: flex;
        flex-wrap: wrap;
        align-items: flex-end;

    }

    .btn_s {
        margin: auto;
        background-image: url(/public/media/admin/img/btn.png);
        background-size: 186%;
        border: none;
        color: #fff !important;
        background-position: center;
        border-radius: 36px;
        font-size: 20px;
        min-width: 240px;
        padding: 8px 0px;
        padding-left: 2rem;
        padding-right: 2rem;
        text-align: center;
        text-decoration: none;
    }

    .btnsBox_main {
        display: flex;
        flex-direction: column;
        width: 33%;
    }

    .btn_s:hover {
        color: #fff;
        text-decoration: none;
    }

    .btn_s + .btn_s {
        margin-top: 1.5rem;
    }
</style>

<div class="mains_controls">
    <div class="mains_controls_text">
        ОЦЕНКА <br>
        ПЕРСОНАЛА
    </div>

</div>

<div class="btnsBox">

    <div class="btnsBox_main">
        <a href="<?=url_custom('/admin/courses')?>" class="btn_s">
            Курсы
        </a>
        <a class="btn_s">
            Работа с новичками
        </a>
        <a class="btn_s">
            План обучения
        </a>
    </div>


    <div class="btnsBox_main">
        <a href="<?=url_custom('/admin/library')?>" class="btn_s">
            Корпоративная библиотека
        </a>
        <a class="btn_s">
            Материалы по адаптации
        </a>
    </div>

    <div class="btnsBox_main">
        <a class="btn_s">
            ИПР
        </a>
        <a class="btn_s">
            Отчетность и аналитика
        </a>
        <a class="btn_s">
            Коммуникация
        </a>
    </div>

</div>


@endsection
