@extends('views.layouts.app')

@section('content')


<style>
    .main_box {
        background-repeat: no-repeat;
        background-position: 35rem 3.5rem;
        background-size: 68rem;
        background-image: url("/public/media/admin/img/candrovoe/page/comandirovka.svg");
    }


    .mains_controls {
        position: relative;
        width: 100%;
    }

    .mains_controls_text {
        font-size: 4.4rem;
        color: #5E24D4;
        line-height: 120%;
        font-weight: bold;
        position: absolute;
        left: 5rem;
        top: 15rem;
        text-transform: uppercase;
    }

    .btnsBox {
        margin-top: 18rem;
        position: relative;
        width: 100%;
        display: flex;
        flex-direction: column;
        align-items: flex-start;
        justify-content: flex-start;
    }

    .btn_s {
        margin: auto;
        background-image: url(/public/media/admin/img/btn.png);

        border: none;
        color: #fff !important;
        background-position: center;
        border-radius: 36px;
        font-size: 20px;
        min-width: 320px;
        padding: 8px 0px;
        padding-left: 2rem;
        padding-right: 2rem;
        text-align: center;
        position: relative;
    }

    .btn_s + .btn_s {
        margin-top: 1.5rem;
    }

    .btn_s-title:before {
        content: '';
        position: absolute;
        left: 0;
        top: 50%;
        transform: translate(-30%, -50%);
        background-image: url("/public/media/admin/img/dots_btn.svg");
        width: 14rem;
        height: 6.6rem;
        background-repeat: no-repeat;
        background-size: contain;
    }

    .box_control {
        display: flex;
        flex-direction: column;
        margin-top: 13rem;
        margin-left: 4rem;
    }
</style>

<div class="mains_controls">
    <div class="mains_controls_text">
        командировки
    </div>

</div>
<div class="btnsBox">

    <div class="box_control">
        <div class="btn_s">
            Найти
        </div>
        <div class="btn_s">
            Сформировать
        </div>


    </div>
</div>


@endsection
