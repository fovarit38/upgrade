@extends('views.layouts.app')

@section('content')


<style>
    .main_box {
        background-image: url("/public/media/admin/img/kadrovoe/main.svg");
        background-repeat: no-repeat;
        background-position: 34rem 4rem;
        background-size: 49rem;
    }

    .main_box:before {
        content: '';
        position: absolute;
        left: 0;
        top: 0;
        width: 100%;
        height: 100%;
        /*background-image: url("/public/media/admin/img/recruitment/dots.svg");*/
        background-repeat: no-repeat;
        background-position: 23rem 10rem;
        background-size: 11rem;
    }

    .mains_controls {
        position: relative;
        width: 100%;
    }

    .mains_controls_text {
        font-size: 4.4rem;
        color: #5E24D4;
        line-height: 120%;
        font-weight: bold;
        position: absolute;
        left: 6rem;
        top: 32rem;
        text-transform: uppercase;
    }

    .btnsBox {
        margin-top: 45rem;
        position: relative;
        width: 100%;
        display: flex;
        flex-wrap: wrap;

    }

    .btn_s {
        background-image: url(/public/media/admin/img/btn.png);
        background-size: 186%;
        border: none;
        color: #fff !important;
        background-position: center;
        border-radius: 36px;
        font-size: 20px;
        min-width: 240px;
        padding: 8px 0px;
        padding-left: 2rem;
        padding-right: 2rem;
        text-align: center;
        text-decoration: none;
    }

    .btnsBox_main {
        display: flex;
        flex-wrap: wrap;
        justify-content: flex-start;
        align-items: center;
        width: 100%;
        margin-left: 6rem;
    }

    .btn_s:hover {
        color: #fff;
        text-decoration: none;
    }

    .btn_s + .btn_s {
    }

    .btn_lef {
        position: absolute;
        left: 5rem;
        top: 10rem;
        display: flex;
        flex-direction: column;
        align-items: flex-start;
    }
</style>

<div class="btn_lef">
    <a class="btn_s">
        Учет рабочего времени
    </a>
    <a class="btn_s" style="margin-top: 4rem;">
        Личные дела
    </a>
    <a class="btn_s" style="margin-top: 4rem;">
        Отпуска
    </a>
    <a class="btn_s" href="<?= url_custom('/admin/ecp') ?>" style="margin-top: 4rem;">
        ЭЦП
    </a>
</div>
<div class="mains_controls">
    <div class="mains_controls_text">
        КАДРОВОЕ <br>
        АДМИНИСТРИРОВАНИЕ
    </div>

</div>

<div class="btnsBox">

    <div class="btnsBox_main">
        <a href="<?=url_custom('/admin/kadrovoe/base') ?>" class="btn_s">
            Базы данных сотрудников
        </a>
        <a href="<?=url_custom('/admin/kadrovoe/commandirovka') ?>"  class="btn_s" style="margin-left: 8rem;">
            Командировки
        </a>
        <a href="<?=url_custom('/admin/kadrovoe/document') ?>" class="btn_s" style="margin-left: 8rem;">
            Договора и соглашения
        </a>
    </div>


</div>


@endsection
