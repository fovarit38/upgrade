@extends('views.layouts.app')

@section('content')


<style>
    .main_box {
        background-image: url("/public/media/admin/img/recruitment/work.svg");
        background-repeat: no-repeat;
        background-position: 27rem 1.8rem;
        background-size: 72rem;
    }


    .mains_controls {
        position: relative;
        width: 100%;
    }

    .mains_controls_text {
        font-size: 4.4rem;
        color: #5E24D4;
        line-height: 120%;
        font-weight: bold;
        position: absolute;
        left: 5rem;
        top: 21rem;
        text-transform: uppercase;
    }

    .btnsBox {
        margin-top: 42rem;
        position: relative;
        width: 100%;
        display: flex;
        flex-direction: column;
        align-items: flex-start;
        justify-content: flex-start;
    }

    .btn_s {
        margin: auto;
        background-image: url(/public/media/admin/img/btn.png);
        background-size: 186%;
        border: none;
        color: #fff !important;
        background-position: center;
        border-radius: 36px;
        font-size: 20px;
        min-width: 240px;
        padding: 8px 0px;
        padding-left: 2rem;
        padding-right: 2rem;
        text-align: center;
    }

    .box_control {
        display: flex;
        flex-wrap: wrap;
    }
</style>

<div class="mains_controls">
    <div class="mains_controls_text">
        Работа <br>
        с кандидатами
    </div>

</div>
<div class="btnsBox">
    <a href="{{url_custom('/admin/recruitment/order')}}" class="btn_s" style="margin-left: 3.5rem;">
        Заявки на подбор
    </a>
    <div class="box_control">
        <a href="{{url_custom('/admin/recruitment/candidates')}}" class="btn_s" style="margin-left: 36.5rem;">
            Кандидататы
        </a>

        <a href="{{url_custom('/admin/recruitment/job')}}" class="btn_s" style="margin-left: 9rem;">
            Job offer
        </a>
    </div>
</div>


@endsection
