@extends('views.layouts.app')

@section('content')


<style>
    .main_box {
        background-image: url("/public/media/admin/img/recruitment/otchet.svg");
        background-repeat: no-repeat;
        background-position: 55rem 1.8rem;
        background-size: 40rem;
    }


    .mains_controls {
        position: relative;
        width: 100%;
    }

    .mains_controls_text {
        font-size: 4.4rem;
        color: #5E24D4;
        line-height: 120%;
        font-weight: bold;
        position: absolute;
        left: 5rem;
        top: 17rem;
        text-transform: uppercase;
    }

    .btnsBox {
        margin-top: 38rem;
        position: relative;
        width: 100%;
        display: flex;
        flex-direction: column;
        align-items: flex-start;
        justify-content: flex-start;
    }

    .btn_s {
        margin: auto;
        background-image: url(/public/media/admin/img/btn.png);
        background-size: 186%;
        border: none;
        color: #fff !important;
        background-position: center;
        border-radius: 36px;
        font-size: 20px;
        min-width: 240px;
        padding: 8px 0px;
        padding-left: 2rem;
        padding-right: 2rem;
        text-align: center;
    }

    .box_control {
        display: flex;
        flex-wrap: wrap;
        margin-top: 5rem;
        margin-left: 3.5rem;
    }
</style>

<div class="mains_controls">
    <div class="mains_controls_text">
        ОТЧЕТНОСТЬ <br>
        И АНАЛИТИКА
    </div>

</div>
<div class="btnsBox">
    <a href="{{url_custom('/admin/recruitment/npc')}}" class="btn_s" style="margin-left: 3.5rem;">
        Отчет по NPS
    </a>
    <div class="box_control">
        <a href="{{url_custom('/admin/recruitment/personala')}}" class="btn_s" >
            Отчет текучести персонала
        </a>

        <a href="{{url_custom('/admin/recruitment/recruting')}}" class="btn_s" style="margin-left: 9rem;">
            Отчет по рекрутмент
        </a>

        <a href="{{url_custom('/admin/recruitment/otcperconal')}}" class="btn_s" style="margin-left: 9rem;">
            Отчет по персоналу
        </a>
    </div>
</div>


@endsection
