@extends('views.layouts.app')

@section('content')


<style>
    .main_box {
        background-repeat: no-repeat;
        background-position: 27rem 1.8rem;
        background-size: 72rem;
    }


    .mains_controls {
        position: relative;
        width: 100%;
    }

    .mains_controls_text {
        font-size: 4.4rem;
        color: #5E24D4;
        line-height: 120%;
        font-weight: bold;
        position: absolute;
        left: 5rem;
        top: 21rem;
        text-transform: uppercase;
    }

    .btnsBox {
        margin-top: 4rem;
        position: relative;
        width: 100%;
        display: flex;
        flex-direction: column;
        align-items: flex-start;
        justify-content: flex-start;
        margin-left: 5rem;
    }

    .btn_s {
        margin: auto;
        background-image: url(/public/media/admin/img/btn.png);

        border: none;
        color: #fff !important;
        background-position: center;
        border-radius: 36px;
        font-size: 20px;
        min-width: 240px;
        padding: 8px 0px;
        padding-left: 2rem;
        padding-right: 2rem;
        text-align: center;
        position: relative;
    }

    .btn_s-title:before {
        content: '';
        position: absolute;
        left: 0;
        top: 50%;
        transform: translate(-30%, -50%);
        background-image: url("/public/media/admin/img/dots_btn.svg");
        width: 14rem;
        height: 6.6rem;
        background-repeat: no-repeat;
        background-size: contain;
    }

    .box_control {
        display: flex;
        flex-wrap: wrap;
        margin-top: 36rem;
        margin-left: 5rem;
    }
</style>


<div class="btnsBox">
    <div class="btn_s btn_s-title" style="margin-left: 3.5rem;">
        ЗАЯВКИ НА ПОДБОР
    </div>


    <table data-v-3551e161="" data-v-65ef4d7e="" class="app__table">
        <thead data-v-3551e161="">
        <tr data-v-3551e161=""><!---->
            <th data-v-3551e161=""><span data-v-3551e161="" class="">ID</span><!----></th>
            <th data-v-3551e161=""><span data-v-3551e161="" class="pointer">Сотрудник</span><!----></th>
            <th data-v-3551e161=""><span data-v-3551e161="" class="">Локация</span><!----></th>
            <th data-v-3551e161=""><span data-v-3551e161="" class="">Отдел</span><!----></th>
            <th data-v-3551e161=""><span data-v-3551e161="" class="">Вход в систему</span><!----></th>
            <th data-v-3551e161=""><span data-v-3551e161="" class="">Роли</span><!----></th>
            <th data-v-3551e161=""><span data-v-3551e161="" class="">Способы отметки</span><!----></th>
        </tr>
        </thead>
        <tbody data-v-3551e161="">
        <tr data-v-3551e161="" class=""><!---->
            <td data-v-65ef4d7e="" data-v-3551e161="">1</td>
            <td data-v-65ef4d7e="" data-v-3551e161="">
                <div data-v-43bb3243="" data-v-65ef4d7e="" class="app-person" data-v-3551e161="">
                    <div data-v-600777a9="" data-v-43bb3243="" class="ui-avatar">
                        <div data-v-600777a9="" class="ui-avatar__bg flex flex--center"
                             style="background-color: var(--color-blue); width: 2rem; height: 2rem; cursor: default; font-size: 1em;">
                            ОС
                        </div><!----></div>
                    <div data-v-43bb3243="" class="app-person__bio"><a data-v-43bb3243=""
                                                                       href="/profile/51804/statistics"
                                                                       class="app-person__name"> Олжас Султан </a>
                        <p data-v-43bb3243="" class="app-person__position"></p></div>
                </div>
            </td>
            <td data-v-65ef4d7e="" data-v-3551e161=""><span data-v-65ef4d7e="" data-v-3551e161=""> Офис </span>
                <!----></td>
            <td data-v-65ef4d7e="" data-v-3551e161=""></td>
            <td data-v-65ef4d7e="" data-v-3551e161=""> 9 июл., 16:38</td>
            <td data-v-65ef4d7e="" data-v-3551e161=""> Администратор <!----></td>
            <td data-v-65ef4d7e="" data-v-3551e161=""><span data-v-65ef4d7e="" class="tooltip-icon has-tooltip"
                                                            data-original-title="null" data-v-3551e161=""><i
                        data-v-65ef4d7e="" class="biometrics icon-fingerprints"></i></span><span data-v-65ef4d7e=""
                                                                                                 class="tooltip-icon has-tooltip"
                                                                                                 data-original-title="null"
                                                                                                 data-v-3551e161=""><i
                        data-v-65ef4d7e="" class="biometrics icon-hand"></i></span><span data-v-65ef4d7e=""
                                                                                         class="tooltip-icon has-tooltip"
                                                                                         data-original-title="null"
                                                                                         data-v-3551e161=""><i
                        data-v-65ef4d7e="" class="biometrics icon-face"></i></span></td>
            <td data-v-65ef4d7e="" data-v-3551e161="">
                <div data-v-5d45b998="" data-v-65ef4d7e="" class="app-actions" data-v-3551e161="">
                    <div data-v-5d45b998="" class="app-actions__current">
                        <div data-v-5d45b998="" class="dots">
                            <div data-v-5d45b998=""></div>
                            <div data-v-5d45b998=""></div>
                            <div data-v-5d45b998=""></div>
                        </div>
                    </div>
                    <div data-v-5d45b998="" class="ui__dropdown ui__dropdown--unset" style="display: none;">
                        <div data-v-5d45b998="" class="ui__dropdown__options">
                            <div data-v-5d45b998="" class="ui__dropdown__item"><i data-v-5d45b998=""
                                                                                  class="app-actions__icon icon-edit"></i><span
                                    data-v-5d45b998="">Редактировать</span></div>
                            <div data-v-5d45b998="" class="ui__dropdown__item"><i data-v-5d45b998=""
                                                                                  class="app-actions__icon icon-close-round"></i><span
                                    data-v-5d45b998="">Деактивировать</span></div>
                            <div data-v-5d45b998="" class="ui__dropdown__item"><i data-v-5d45b998=""
                                                                                  class="app-actions__icon icon-trash"></i><span
                                    data-v-5d45b998="">Удалить</span></div>
                        </div>
                    </div>
                </div>
            </td>
        </tr>
        </tbody>
    </table>


    <div class="box_control">
        <div class="btn_s" style="">
            Принять
        </div>




        <div class="btn_s" style="margin-left: 9rem;">
            Сформировать
        </div>
    </div>
</div>


@endsection
