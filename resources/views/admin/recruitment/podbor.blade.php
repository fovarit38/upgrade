@extends('views.layouts.app')

@section('content')


<style>
    .main_box {
        background-image: url("/public/media/admin/img/recruitment/podbor.svg");
        background-repeat: no-repeat;
        background-position: 12rem 1.8rem;
        background-size: 75rem;
        width: 100%;
    }


    .mains_controls {
        position: relative;
        width: 100%;
    }

    .mains_controls_text {
        font-size: 4.4rem;
        color: #5E24D4;
        line-height: 120%;
        font-weight: bold;
        position: absolute;
        left: 5rem;
        top: 37rem;
        text-transform: uppercase;
    }

    .btnsBox {
        margin-top: 45rem;
        position: relative;
        width: 100%;
        display: flex;
        flex-direction: column;
        align-items: flex-start;
        justify-content: flex-start;
    }

    .btn_s {
        margin: auto;
        background-image: url(/public/media/admin/img/btn.png);
        background-size: 186%;
        border: none;
        color: #fff !important;
        background-position: center;
        border-radius: 36px;
        font-size: 20px;
        min-width: 240px;
        padding: 8px 0px;
        padding-left: 2rem;
        padding-right: 2rem;
        text-align: center;
    }

    .box_control {
        display: flex;
        flex-wrap: wrap;
    }
</style>

<div class="mains_controls">
    <div class="mains_controls_text">
        Организация подбора
    </div>

</div>
<div class="btnsBox">

    <div class="box_control">
        <a href="{{url_custom('/admin/recruitment/vakansi')}}" class="btn_s" style="margin-left: 5rem;">
            Актуальные вакансии
        </a>
        <a href="{{url_custom('/admin/recruitment/basa')}}" class="btn_s" style="margin-left: 6rem;">
            База резюме
        </a>
        <a href="{{url_custom('/admin/recruitment/cron')}}" class="btn_s" style="margin-left: 6rem;">
            Планировщик
        </a>
    </div>
</div>


@endsection
