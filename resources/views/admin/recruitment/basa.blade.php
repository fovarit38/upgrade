@extends('views.layouts.app')

@section('content')


<style>
    .main_box {
        background-image: url(/public/media/admin/img/recruitment/base.svg);
        background-repeat: no-repeat;
        background-position: 6rem 11rem;
        background-size: 39rem;
    }


    .mains_controls {
        position: relative;
        width: 100%;
    }

    .mains_controls_text {
        font-size: 4.4rem;
        color: #5E24D4;
        line-height: 120%;
        font-weight: bold;
        position: absolute;
        left: 5rem;
        top: 21rem;
        text-transform: uppercase;
    }

    .btnsBox {
        margin-top: 4rem;
        position: relative;
        width: 100%;
        display: flex;
        flex-direction: column;
        align-items: flex-start;
        justify-content: flex-start;
        padding-left: 5rem;
    }

    .btn_s {
        margin: auto;
        background-image: url(/public/media/admin/img/btn.png);

        border: none;
        color: #fff !important;
        background-position: center;
        border-radius: 36px;
        font-size: 20px;
        min-width: 240px;
        padding: 8px 0px;
        padding-left: 2rem;
        padding-right: 2rem;
        text-align: center;
        position: relative;
    }

    .btn_s-title:before {
        content: '';
        position: absolute;
        left: 0;
        top: 50%;
        transform: translate(-30%, -50%);
        background-image: url("/public/media/admin/img/dots_btn.svg");
        width: 14rem;
        height: 6.6rem;
        background-repeat: no-repeat;
        background-size: contain;
    }

    .box_control {
        display: flex;
        flex-wrap: wrap;
        margin-top: 38rem;
        margin-left: 2rem;
    }
</style>


<div class="btnsBox">
    <div class="btn_s btn_s-title" style="margin-left: 3.5rem;">
        БАЗА РЕЗЮМЕ
    </div>
    <div class="box_control">
        <div class="btn_s" style="margin-left: 0rem;">
            Сформировать
        </div>
    </div>
</div>


@endsection
