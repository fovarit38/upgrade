@extends('views.layouts.app')

@section('content')


<style>
    .main_box {
        background-image: url("/public/media/admin/img/recruitment/reglamint.svg");
        background-repeat: no-repeat;
        background-position: 50rem 3.5rem;
        background-size: 45rem;
    }


    .mains_controls {
        position: relative;
        width: 100%;
    }

    .mains_controls_text {
        font-size: 4.4rem;
        color: #5E24D4;
        line-height: 120%;
        font-weight: bold;
        position: absolute;
        left: 5rem;
        top: 17rem;
        text-transform: uppercase;
    }

    .btnsBox {
        margin-top: 25rem;
        position: relative;
        width: 100%;
        display: flex;
        flex-direction: column;
        align-items: flex-start;
        justify-content: flex-start;
    }

    .btn_s {
        margin: auto;
        background-image: url(/public/media/admin/img/btn.png);
        background-size: 186%;
        border: none;
        color: #fff !important;
        background-position: center;
        border-radius: 36px;
        font-size: 20px;
        min-width: 240px;
        padding: 8px 0px;
        padding-left: 2rem;
        padding-right: 2rem;
        text-align: center;
    }

    .box_control {
        display: flex;
        flex-direction: column;
        margin-top: 5rem;
        margin-left: 0rem;
    }
</style>

<div class="mains_controls">
    <div class="mains_controls_text">
        РЕГЛАМЕНТЫ
    </div>

</div>
<div class="btnsBox">
    <div class="box_control">
        <a href="{{url_custom('/admin/recruitment/bisnes')}}" class="btn_s">
            Бизнес-процессы
        </a>

        <a href="{{url_custom('/admin/recruitment/docent')}}" class="btn_s" style="margin-left: 6rem;margin-top: 3rem;">
            Документы
        </a>

        <a href="{{url_custom('/admin/recruitment/tamplase')}}" class="btn_s" style="margin-left: 9rem;margin-top: 3rem;">
            Шаблоны
        </a>
    </div>
</div>


@endsection
