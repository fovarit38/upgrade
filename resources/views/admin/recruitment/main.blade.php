<?php
/*
?>
<style>
    .main_box {
        background-image: url("/public/media/admin/img/recruitment/main.svg");
        background-repeat: no-repeat;
        background-position: 36rem 2.7rem;
        background-size: 40rem;
    }

    .main_box:before {
        content: '';
        position: absolute;
        left: 0;
        top: 0;
        width: 100%;
        height: 100%;
        background-image: url("/public/media/admin/img/recruitment/dots.svg");
        background-repeat: no-repeat;
        background-position: 23rem 10rem;
        background-size: 11rem;
    }

    .mains_controls {
        position: relative;
        width: 100%;
    }

    .mains_controls_text {
        font-size: 4.4rem;
        color: #5E24D4;
        line-height: 120%;
        font-weight: bold;
        position: absolute;
        left: 10rem;
        top: 21rem;
        text-transform: uppercase;
    }

    .btnsBox {
        margin-top: 25rem;
        position: relative;
        width: 100%;
        display: flex;
        flex-direction: column;
        align-items: flex-start;
        justify-content: flex-start;
    }

    .btn_s {
        margin: auto;
        background-image: url(/public/media/admin/img/btn.png);
        background-size: 186%;
        border: none;
        color: #fff !important;
        background-position: center;
        border-radius: 36px;
        font-size: 20px;
        min-width: 240px;
        padding: 8px 0px;
        padding-left: 2rem;
        padding-right: 2rem;
        text-align: center;
        text-decoration: none;
    }

    .btn_s:hover {
        color: #fff;
        text-decoration: none;
    }
</style>

<div class="mains_controls">
    <div class="mains_controls_text">
        ПОДБОР <br>
        ПЕРСОНАЛА
    </div>

</div>

<div class="btnsBox">
    <a href="<?=url_custom('/admin/recruitment/reglamenti')?>" class="btn_s" style="margin-left: 80rem;">
        Регламенты
    </a>

    <a href="{{url_custom('/admin/recruitment/work')}}" class="btn_s" style="margin-left: 4rem;margin-top: 11rem;">
        Работа с кандидатами
    </a>

    <a href="<?=url_custom('/admin/recruitment/podbor')?>" class="btn_s" style="margin-left: 69rem;margin-top: -1rem;">
        Организация подбора
    </a>


    <a href="<?=url_custom('/admin/recruitment/otchet')?>" class="btn_s" style="margin-left: 35rem;margin-top: -1rem;">
        Отчетность и аналитика
    </a>
</div>


@endsection
<?
*/
?>
<div class="preinfo">
    <img class="preinfo_icon" src="/public/media/admin/img/module/list.png"/>

    <div class="btns_bo">
        <a href="/ru/almaty/admin/book" class="btn_text">
            <img style="width: 44px;" src="/public/media/admin/img/module/adapt.png"/>
            <span style="margin-left: 30px;">
            Адаптация <br>
            новых сотрудников
            </span>
        </a>
        <a href="/ru/almaty/admin/courses" class="btn_text">
            <img style="width: 44px;" src="/public/media/admin/img/module/obt.png"/>
            <span style="margin-left: 30px;">
                Обучение и развитие
            </span>
        </a>
    </div>
</div>
<style>
    body, html {
        margin: 0;
        padding: 0;
        background-color: #F5E49D;
    }

    .preinfo {
        display: flex;
        flex-direction: column;
        align-items: center;
        justify-content: center;
    }

    .preinfo_icon {
        width: 900px;
    }

    .btns_bo {
        display: flex;
        justify-content: center;
        align-items: center;
        margin-top: 24px;
    }

    .btn_text {
        width: 400px;
        height: 65px;
        background: #A6C437;
        border-radius: 16px;
        font-weight: 400;
        font-size: 18px;
        line-height: 24px;
        color: #FFFFFF;
        display: flex;
        align-items: center;
        padding: 0 30px;
        box-sizing: border-box;
        text-decoration: none;
        font-family: Arial;
    }

    .btn_text + .btn_text {
        margin-left: 90px;
    }
</style>
