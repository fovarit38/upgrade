@php
    $columns=[];
        if($model_name!="table"){
           $model = app("\App\\" . $model_name);
           $columns = Schema::getColumnListing($model->getTable());
           }
@endphp
<table
    class="table sortTables @if(in_array("sort",$columns)) sortable  @endif   table-bordered table-striped js-table dataTable order-table no-footer"
    id="DataTables_Table_0" role="grid" aria-describedby="DataTables_Table_0_info">
    <thead>
    <tr role="row">

        @foreach($thead as $keyxs=>$head)
            <th class="sorting {{$head=="id"?"fitwidth fitwidth-mini":""}}" tabindex="0"
                aria-controls="DataTables_Table_0" rowspan="1" colspan="1" aria-label="">
                @if($keyxs=="name" && $model_name=="Official")
                    Название должности
                @else
                    {{$head}}
                @endif
            </th>
        @endforeach
        <th class="fitwidth">
            Действие
        </th>
    </tr>
    </thead>
    <tbody class="ui-sortable">


    @foreach($tbody as $body)
        <tr role="row" class="even clickNexts ui-sortable-handle">
            @foreach($thead as $keyxs=>$head)
                <td class=" sorting_1 {{$keyxs=="id"?'fitwidth fitwidth-mini index':""}}" {{$keyxs=="id"?" data-id=".$body->id."":""}}>
                    @php
                        $return_data=($body->{$keyxs});
                    @endphp

                    @if($keyxs=="Product_id")
                        @php
                            $product=\App\Product::find($body->{$keyxs});
                                if(!is_null($product)){
                                $return_data=$product->title;
                                }
                        @endphp
                    @endif

                    @if($keyxs=="user_id")
                        @php
                            $product=\App\User::find($body->{$keyxs});
                                if(!is_null($product)){
                                $return_data=$product->name;
                                }
                        @endphp
                    @endif

                    @if(isset($exegesis))
                        @if(isset($exegesis[$keyxs]))
                            @if(isset($exegesis[$keyxs][$body->{$keyxs}]))
                                @php
                                    $return_data=  $exegesis[$keyxs][$body->{$keyxs}];
                                @endphp
                            @endif
                        @endif
                    @endif


                    {!! LC($return_data,"ru") !!}

                </td>
            @endforeach
            <td class="fitwidth">

                <a class=" btn btn-sm btn-danger" style="color:#fff;margin-right: 0.5rem;"
                   href="{{url_custom("/admin/delete/".$model_name."/".$body->id)}}">Удалить</a>
                @if(count($table_link)>0)
                    <a class="nextlink btn btn-sm btn-info"
                       href="{{url_custom($table_link[0].$body->{$table_link[1]})}}">Изменить</a>
                @endif
                {{--                <a href="/admin/catalog/remove/2">Удалить</a>--}}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>
