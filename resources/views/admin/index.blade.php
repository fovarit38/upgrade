<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>{{ config('app.name', 'Laravel') }} :: Dashboard</title>
    <link rel="stylesheet" href="https://unicons.iconscout.com/release/v2.1.11/css/unicons.css">
    <link href="https://fonts.googleapis.com/css2?family=Open+Sans:wght@300;400;600;700&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="/public/media/admin/css/style.css?v=9">
</head>
<body style="background-image: url('/public/media/client/bk.png');background-size: cover;">

<div class="romb_box">
    <div class="romb" style="">
    </div>

    <?php
    $nav = [
        ['Адаптация <br> новых сотрудников', '/public/media/client/icons/adaptiv.png', 'btn-controls-x1 ',url_custom('/admin/adaptacia')],
        ['Обучение<br> и развитие', '/public/media/client/icons/traning.png', 'btn-controls-x2 ',url_custom('/admin/trating')],
        ['Оценка<br> персонала', '/public/media/client/icons/person.png', 'btn-controls-x3 ',url_custom('/admin/ocenka')],
        ['Управление', '/public/media/client/icons/control.png', 'btn-controls-x4 opacity',''],
        ['Кадровое<br> администрирование', '/public/media/client/icons/admin.png', 'btn-controls-x5 ',url_custom('/admin/kadrovoe')],
        ['Корпоративная<br> культура', '/public/media/client/icons/corporativ.png', 'btn-controls-x6 opacity',""],
        ['Мотивация<br> труда', '/public/media/client/icons/motivation.png', 'btn-controls-x7 opacity',''],
        ['Подбор<br> персонала', '/public/media/client/icons/podbot.png', 'btn-controls-x8 ',url_custom('/admin/recruitment')],
    ];
    foreach ($nav as $n) {
        ?>
        <a href="{{$n[3]}}" class="btn btn-controls {!!$n[2]!!}">
            <img src="{!!$n[1]!!}" class="icon_box" alt="">
            <p>
                {!!$n[0]!!}
            </p>
        </a>
        <?php
    }
    ?>
</div>

<style>
    .romb_box {
        min-height: 100vh;
        width: 100%;
        display: flex;
        align-content: center;
        justify-content: center;
        position: relative;
    }

    .romb {
        width: 50rem;
        height: 33.8rem;
        background-image: url("/public/media/client/romb.png");
        position: absolute;
        left: 50%;
        top: 50%;
        transform: translate(-50%, -50%);
        background-size: contain;
        background-position: center center;
        background-repeat: no-repeat;
    }

    .btn-controls {
        background-image: url("/public/media/client/pramo.png");
        font-style: normal;
        font-weight: bold;
        font-size: 1.44rem;
        line-height: 140%;
        text-transform: uppercase;
        display: flex;
        flex-wrap: wrap;
        align-items: center;
        color: #fff;
        background-size: 124%;
        background-position: center center;
        overflow: hidden;
        border-radius: 1rem;
        min-width: 22rem;
        text-align: left;
        padding-left: 1.33rem;
        padding-right: 1rem;
        padding-top: 1rem;
        padding-bottom: 1rem;
        position: absolute;
        left: 50%;
        top: 50%;
        height: 7rem;
        transform: translate(-50% -50%);
        min
    }

    .btn-controls-x1 {
        left: calc(50% - 30rem);
        top: calc(50% - 28rem);
    }

    .btn-controls-x2 {
        left: calc(50% + 0rem);
        top: calc(50% - 28rem);
    }

    .btn-controls-x3 {
        left: calc(50% + 25rem);
        top: calc(50% - 14rem);
    }

    .btn-controls-x4 {
        left: calc(50% + 25rem);
        top: calc(50% + 4rem);
    }

    .btn-controls-x5 {
        left: calc(50% + 0rem);
        top: calc(50% + 22rem);
    }

    .btn-controls-x6 {
        left: calc(50% - 30rem);
        top: calc(50% + 22rem);
    }

    .btn-controls-x7 {
        left: calc(50% - 54rem);
        top: calc(50% + 4rem);
    }

    .btn-controls-x8 {
        left: calc(50% - 54rem);
        top: calc(50% - 14rem);
    }

    .btn-controls:hover {
        color: #fff;
    }

    .opacity {
        opacity: 0.3;
    }

    .icon_box {
        width: 3.8888rem;
        margin-right: 1.5rem;
    }

    html {
        font-size: 12px;
    }

    @media screen and (max-width: 1400px) {
        html {
            font-size: 9px;
        }
    }
</style>
<script src="/public/media/admin/js/app.js?v=10"></script>
</body>
</html>
