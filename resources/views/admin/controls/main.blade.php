@extends('views.layouts.app')

@section('content')


<style>
    .main_box {
        background-image: url("/public/media/admin/img/recruitment/main.svg");
        background-repeat: no-repeat;
        background-position: 36rem 2.7rem;
        background-size: 40rem;
    }

    .main_box:before {
        content: '';
        position: absolute;
        left: 0;
        top: 0;
        width: 100%;
        height: 100%;
        background-image: url("/public/media/admin/img/recruitment/dots.svg");
        background-repeat: no-repeat;
        background-position: 23rem 10rem;
        background-size: 11rem;
    }

    .mains_controls {
        position: relative;
        width: 100%;
    }

    .mains_controls_text {
        font-size: 4.4rem;
        color: #5E24D4;
        line-height: 120%;
        font-weight: bold;
        position: absolute;
        left: 10rem;
        top: 21rem;
        text-transform: uppercase;
    }

    .btnsBox {
        margin-top: 25rem;
        position: relative;
        width: 100%;
        display: flex;
        flex-direction: column;
        align-items: flex-start;
        justify-content: flex-start;
    }

    .btn_s {
        margin: auto;
        background-image: url(/public/media/admin/img/btn.png);
        background-size: 186%;
        border: none;
        color: #fff !important;
        background-position: center;
        border-radius: 36px;
        font-size: 20px;
        min-width: 240px;
        padding: 8px 0px;
        padding-left: 2rem;
        padding-right: 2rem;
        text-align: center;
        text-decoration: none;
    }

    .btn_s:hover {
        color: #fff;
        text-decoration: none;
    }
</style>

<div class="mains_controls">
    <div class="mains_controls_text">
        ПОДБОР <br>
        ПЕРСОНАЛА
    </div>

</div>

<div class="btnsBox">
    <a href="<?=url_custom('/admin/recruitment/reglamenti')?>" class="btn_s" style="margin-left: 80rem;">
        Регламенты
    </a>

    <a href="{{url_custom('/admin/recruitment/work')}}" class="btn_s" style="margin-left: 4rem;margin-top: 11rem;">
        Работа с кандидатами
    </a>

    <a href="<?=url_custom('/admin/recruitment/podbor')?>" class="btn_s" style="margin-left: 69rem;margin-top: -1rem;">
        Организация подбора
    </a>


    <a href="<?=url_custom('/admin/recruitment/otchet')?>" class="btn_s" style="margin-left: 35rem;margin-top: -1rem;">
        Отчетность и аналитика
    </a>
</div>


@endsection
