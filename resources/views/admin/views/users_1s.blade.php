@extends('views.layouts.app')

@section('content')


    <div class="manager">
        <div class="manager_main">
            <div class="manager_main_head">
                <h1 class="interface_view_title with_toggle">
                    <span class="header_title_toggle"></span>
                    Пользователи в Битрикс
                </h1>
                <form method="get" class="interface_view_header_filters"
                      style="margin-left: auto;margin-right: 2rem;position: relative;">
                    <input type="search" name="search" class="text sear_c input_text"
                           value="{{isset($_REQUEST["search"])?$_REQUEST["search"]:''}}" placeholder="Поиск ">
                </form>

            </div>

            <div class="manager_main_curses" style="padding: 0 2rem;">



                <style>
                    .select-sa {
                        width: 30%;
                        flex-grow: 2;
                    }
                </style>
                @php
                    if(isset($_REQUEST["search"])){
                      $users=\App\User::where("name","LIKE",'%'.$_REQUEST["search"].'%')->get();
                     }

                    if(isset($_REQUEST["compani"])){
                     if($_REQUEST["compani"]!=""){
                        $compani_list= array_keys(App\CompaniUser::where("compani_id",$_REQUEST["compani"])->get()->groupby("user_id")->toarray());
                        $users=$users->wherein("id",$compani_list);
                      }
                     }

                        if(isset($_REQUEST["official"])){
                            if($_REQUEST["official"]!=""){
                            $users=$users->wherein("jobtitle_id",$_REQUEST["official"]);
                            }
                        }

                @endphp

                <table
                    class="table sortTables  table-bordered table-striped js-table dataTable order-table no-footer"
                    id="DataTables_Table_0" style="width: 100%;" role="grid"
                    aria-describedby="DataTables_Table_0_info">
                    <thead>
                    <tr>

                        <th role="row">Имя</th>
                        <th role="row">Email</th>
                        <th role="row">Телефон</th>
                        <th role="row">Должность</th>
                        <th role="row">Подразделение</th>
                        <th role="row">Статус</th>
                        <th role="row">Создан в системе</th>
                        <th role="row"></th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($users as $user)

                        <tr role="row" class="even  ui-sortable-handle">

                            <td class=" sorting_1">
                                <div class="img-prof_box">
                                    <div href="<?=$user->personal_photo?>" data-fancybox="gallery" class="img-prof">
                                        <img src="<?=$user->personal_photo?>" alt="">
                                    </div>
                                    <?=$user->name?></a>
                                </div>
                            </td>
                            <td>
                                <?=$user->email?>
                            </td>
                            <td>
                                <?=$user->personal_phone?>
                            </td>

                            <td>
                                {{$user->work_position}}

                            </td>
                            <td> {{$user->work_company}}</td>

                            <td>
                                <div class="options-edit">
                                    @if($user->active=="1")
                                        <span style="color:green;">Работает</span>
                                    @else
                                        <span style="color:red;">Уволен</span>
                                    @endif

                                </div>
                            </td>
                            <td>
                                <?php
                                $user_email = \App\User::where("email", $user->email)->get();
                                $user_tel = [];
                                if (validate_phone_number($user->personal_phone)) {
                                    $user_tel = \App\User::where("email", validate_phone_number($user->personal_phone))->get();
                                }
                                ?>

                                @if(count($user_email)>0 || count($user_tel)>0)
                                    <p>
                                        Найден в системе как
                                    </p>
                                @else
                                    <p>
                                        Не найден
                                    </p>
                                @endif

                                @foreach($user_email as $usr)
                                    <p>
                                        <a href="{{url_custom('/admin/users/'.$usr->id)}}"
                                           style="text-decoration: none;color: #0467ad;">
                                            {{$usr->name}}
                                        </a>
                                    </p>
                                @endforeach
                                @foreach($user_tel as $usr)
                                    <p>
                                        <a href="{{url_custom('/admin/users/'.$usr->id)}}"
                                           style="text-decoration: none;color: #0467ad;">
                                            {{$usr->name}}
                                        </a>
                                    </p>
                                @endforeach
                            </td>
                            <td>
                                <p>
                                    <a href="{{url_custom('/admin/model/_1c_User/'.$user->id)}}"
                                       style="text-decoration: none;color: #0467ad;">
                                        Уведомления
                                    </a>
                                </p>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>

            </div>
        </div>
    </div>

@endsection
