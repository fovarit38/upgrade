@extends('views.layouts.app')

@section('content')


    <div class="manager">


        <div class="manager_main">
            <div class="manager_main_head"
                 style="width: 100%; background-color: #ebebeb; border-bottom: 1px solid #ebebeb; height: auto; min-height: auto; padding: 1rem 2rem; ">
                <h1 class="interface_view_title with_toggle" style=" font-size: 1rem; ">
                    <span class="header_title_toggle"></span>
                    Проверка прохождение курсов
                </h1>
            </div>
            <div class="manager_main_nav">
                <div class="nav_bbl">
                    <a href="{{url_custom('/admin/courses/check/'.$code)}}" class="btn btn-curseadd ">
                        Результаты
                    </a>
                    <a href="{{url_custom('/admin/courses/check/'.$code.'/bonus')}}" class="btn btn-curseadd active">
                        Начисление бонусов
                    </a>


                    @php
                        $user=\App\User::find($info->user_id);
                           $user_email = \App\_1c_User::where("email", $user->email)->first();
                    @endphp
                        @if(!is_null($user_email))
<!--                            <a href="{{url_custom('/admin/model/_1c_User/'.$user_email->id)}}"-->
<!--                               class="btn btn-curseadd " style="text-decoration: none;color: #0467ad;">-->
<!--                                Уведомить-->
<!--                            </a>-->
                        @else
<!--                            <a href="javascript:void(0)" class="btn btn-curseadd "-->
<!--                               style="color: red;text-decoration: revert;">-->
<!--                                Не найден в системе Битрикс-->
<!--                            </a>-->
                    @endif

                </div>
            </div>

            <div class="menulistin">

                <div class="manager_main_curses addMenuData"
                     style=" align-items: flex-start; justify-content: flex-start; ">

                    <div class="addMenuData_main minimaiz"
                         style="align-items: flex-start;width: auto;min-width: 100%;flex-direction: row;flex-wrap: wrap;">

                        @if(!isset($_REQUEST["add"]))
                            <div class="basdsa"
                                 style="display: flex;justify-content: flex-end;align-items: center;width: 100%;">
                                <a href="{{url_custom('/admin/courses/check/'.$code.'/bonus?add=0')}}"
                                   class="btn btn-info">Добавить</a>
                            </div>

                            <div class="userboxas" style="width: 100%;">

                                <div style=" width: 100%; ">
                                    <div class="col" style="padding: 0rem;">
                                        <div class="ofl_table">
                                            <table class="table td_middle td_highlight">
                                                <thead>
                                                <tr>
                                                    <th class=" tcol_metrics">Баллов</th>
                                                    <th class=" tcol_metrics">Медалей</th>
                                                    <th class=" tcol_metrics">Эффективность</th>
                                                    <th class=" tcol_metrics">Выдано</th>
                                                    <th class=" tcol_menu" style="width: 70px;"></th>
                                                </tr>
                                                </thead>
                                                <tbody>

                                                @php
                                                    $user_list=  (\App\BonusAdd::where("code",$code)->get());
                                                @endphp
                                                @foreach($user_list as $us)
                                                    @php
                                                        $userInd=\App\User::find($us->user_id);
                                                    @endphp
                                                    <tr class="">

                                                        <td class=" tcol_metrics">
                                                            <div class="tcol_metrics_value"><span>{{$us->bonus}}</span>
                                                            </div>
                                                        </td>
                                                        <td class=" tcol_metrics">
                                                            <div class="tcol_metrics_value"><span
                                                                    class="clr_gray">{{$us->medal}}</span>
                                                            </div>
                                                        </td>
                                                        <td class=" tcol_metrics">
                                                            <div class="tcol_metrics_value"><span
                                                                    class="clr_gray">{{$us->efficiency}}</span>
                                                            </div>
                                                        </td>
                                                        <td class=" tcol_metrics">
                                                            <div class="tcol_metrics_value"><span
                                                                    class="clr_gray">{{$us->created_at}}</span>
                                                            </div>
                                                        </td>
                                                        <td class=" tcol_menu" style="width: 70px;">
                                                            <div class="flex flex_row"><a style="text-decoration: none;"
                                                                                          href="{{url_custom('/admin/courses/check/'.$code.'/bonus?add='.$us->id)}}"><i
                                                                        style="font-size: 1.5rem;"
                                                                        class="notranslate icn icn-edit "
                                                                        aria-hidden="true"
                                                                        role="presentation"></i></a></div>
                                                        </td>
                                                    </tr>
                                                @endforeach
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @else

                            @php

                                @endphp
                            <form action="{{url_custom('/admin/courses/check/add')}}" method="post"
                                  style="width: 100%;text-align: left;">
                                @csrf
                                <input type="hidden" name="code" value="{{$code}}">
                                <input type="hidden" name="id" value="{{$_REQUEST["add"]}}">

                                <input type="hidden" name="user_id" value="{{$info->user_id}}">
                                <div class="basdsa"
                                     style="display: flex;justify-content: flex-end;align-items: center;width: 100%;">
                                    <a href="{{url_custom('/admin/courses/check/'.$code.'/bonus')}}"
                                       class="btn btn-info">Назад</a>
                                    <button type="submit"
                                            class="btn btn-success" style="margin-left: 2rem;color:#fff;">Сохранить
                                    </button>
                                </div>

                                @php
                                    $curseUser=  \App\BonusAdd::find($_REQUEST["add"]);
                                @endphp

                                {{maskInput($errors,["name"=>"bonus_save","type"=>"number","placeholder"=>"Бонусов"],$curseUser)}}
                                {{maskInput($errors,["name"=>"medal_save","type"=>"number","placeholder"=>"Медалей"],$curseUser)}}
                                {{maskInput($errors,["name"=>"efficiency_save","type"=>"number","placeholder"=>"Эффективность"],$curseUser)}}

                            </form>
                        @endif

                    </div>

                </div>
            </div>
        </div>

        <style>
            .manager_main_curses {
                width: 50%;
                flex-grow: 2;
                padding-top: 2.5rem;
            }

            .timeRes {
                position: absolute;
                left: 0;
                top: 0;
                width: 100%;
                height: 2.5rem;
                font-size: 1rem;
                padding-left: 1rem;
                display: flex;
                align-items: center;
                background-color: #0467ad;
                color: #fff;
            }

            .table .tcol_metrics {
                width: auto;
            }
        </style>

@endsection
