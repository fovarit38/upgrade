@extends('views.layouts.app')

@section('content')


    <div class="manager">


        <div class="manager_main">
            <div class="manager_main_head"
                 style="width: 100%; background-color: #ebebeb; border-bottom: 1px solid #ebebeb; height: auto; min-height: auto; padding: 1rem 2rem; ">
                <h1 class="interface_view_title with_toggle" style=" font-size: 1rem; ">
                    <span class="header_title_toggle"></span>
                    Проверка прохождение курсов
                </h1>
            </div>
            <div class="manager_main_nav">
                <div class="nav_bbl">
                    <a href="{{url_custom('/admin/courses/check/'.$code)}}" class="btn btn-curseadd active">
                        Результаты
                    </a>
                    <a href="{{url_custom('/admin/courses/check/'.$code.'/bonus')}}" class="btn btn-curseadd">
                        Начисление бонусов
                    </a>

                    @php
                        $user=\App\User::find($info->user_id);
                              $user_email = \App\_1c_User::where("email", $user->email)->first();
                    @endphp
                        @if(!is_null($user_email))
<!--                            <a href="{{url_custom('/admin/model/_1c_User/'.$user_email->id)}}"-->
<!--                               class="btn btn-curseadd " style="text-decoration: none;color: #0467ad;">-->
<!--                                Уведомить-->
<!--                            </a>-->
                        @else
<!--                            <a href="javascript:void(0)" class="btn btn-curseadd "-->
<!--                               style="color: red;text-decoration: revert;">-->
<!--                                Не найден в системе Битрикс-->
<!--                            </a>-->
                    @endif

                </div>
            </div>
            @php

                $include_type=['test','video','pdf','text','task'];
              $pre=null;
              $next=isset($_REQUEST["next"])?$_REQUEST["next"]:null;
              $include=null;

            @endphp

            @php
                $icon=["video"=>"icn-videoplayer","text"=>"icn-book","pdf"=>"icn-pdf","test"=>"icn-test","task"=>"icn-task"];
               $all_test=  $model_list->groupby("end_sing_code");
               $keylis=array_keys($all_test->toarray());
               $ch_list=isset($_REQUEST["list"])?$_REQUEST["list"]:0;
               $urdop='&list='.$ch_list;
            @endphp
            <div class="menulistin">
                <div class="sitebarleft">
                    <div class="testlist">
                        @foreach($keylis as $keyxs=>$ksa)
                            <a href="{{url_custom('/admin/courses/check/'.$code.'?list='.$keyxs)}}"
                               class="li_link {{$ch_list==$keyxs?'active':''}}">
                                Пересдача {{$keyxs+1}}
                            </a>
                        @endforeach
                        <style>
                            .li_link {
                                width: calc(50% - 1rem);
                                margin-left: 1rem;
                                margin-top: 1rem;
                                display: flex;
                                align-items: center;
                                justify-content: center;
                                padding: 0.5rem 0;
                                border: 1px solid #0467ad;
                                font-size: 0.9rem;
                                color: #0467ad;
                                font-weight: bold;
                                text-decoration: none;
                            }

                            .li_link.active {
                                background-color: #0467ad;
                                color: #fff;
                            }


                            .li_link.active:hover {
                                color: #fff;
                            }


                            .testlist {
                                display: flex;
                                flex-wrap: wrap;
                                margin-left: -1rem;
                                padding: 0 1rem;
                            }
                        </style>
                    </div>
                    <div class="lessons_list">
                        <ul class="lessons_list_ul">

                            @foreach($all_test[$keylis[$ch_list]] as $index=>$coursMeta)
                                @php

                                    if(is_null($next)?$index==0:$next==$coursMeta->id){
                                      $include=$coursMeta;
                                    }
                                    if(is_null($include) ){
                                       $pre=$coursMeta;
                                    }
                                @endphp
                                <li class="cards_board_item_material {{!is_null($include)?($include->id==$coursMeta->id?'active':''):''}} ">
                                    <a
                                        href="{{url_custom('/admin/courses/check/'.$code.'?next='.$coursMeta->id.$urdop)}}"
                                        class="item_link"></a>
                                    <div style="text-decoration: none;" class="lessons_list_icon"><i
                                            class="notranslate icn {{$icon[$coursMeta->icon_type]}} "
                                            aria-hidden="true" role="presentation"></i>
                                    </div>
                                    <div style="text-decoration: none;flex-direction: column;align-items: flex-start;"
                                         class="lessons_list_info">
                                        <div class="lessons_list_title">{{$coursMeta->importance}}</div>
                                        @if($coursMeta->icon_type=="test")
                                            @php
                                                $children_prs=array_keys(\App\UserMetaCheckItem::where("userMetaCheckItem_id",$coursMeta->id)->get()->groupby("id")->toarray());
                                                $brews_true=\App\UserMetaCheckItem::wherein("userMetaCheckItem_id",$children_prs)->where("boolead","1")->where("boolead_true",1)->count();
                                                $brews_false=\App\UserMetaCheckItem::wherein("userMetaCheckItem_id",$children_prs)->where("boolead","0")->where("boolead_true",1)->count()+\App\UserMetaCheckItem::wherein("userMetaCheckItem_id",$children_prs)->where("boolead","1")->where("boolead_true",0)->count();
                                            @endphp
                                            <div style="display: flex;">
                                                <span style="color:green;">Верных {{$brews_true}}</span>
                                                <span
                                                    style="color:red;margin-left: 1rem;">Не верных {{$brews_false}}</span>
                                            </div>
                                        @endif
                                    </div>
                                </li>
                            @endforeach
                        </ul>

                    </div>
                </div>
                <div class="manager_main_curses addMenuData" style="position: relative;">

                    @php
                        $time="";
                            if(!is_null($pre) && !is_null($include)){
                               $now = DateTime::createFromFormat("Y-m-d H:i:s", date('Y-m-d H:i:s',  strtotime($include->created_at))); // текущее время на сервере
                               $date = DateTime::createFromFormat("Y-m-d H:i:s", date('Y-m-d H:i:s',  strtotime($pre->created_at)));
                               $interval = $now->diff($date);
                               $time.=$interval->d.' дн, '.$interval->h.' ч, '.$interval->i.' мин, '.$interval->s.' сек';
                            }
                            $children=\App\UserMetaCheckItem::where("userMetaCheckItem_id",$include->id);
                    @endphp
                    @if($time!="")
                        <div class="timeRes">
                            На данной страницы находился {{($time)}}
                        </div>
                    @endif
                    @include('views.courses.check.type_items.'.$include->icon_type)
                </div>
            </div>
        </div>

        <style>
            .manager_main_curses {
                width: 50%;
                flex-grow: 2;
                padding-top: 2.5rem;
            }

            .timeRes {
                position: absolute;
                left: 0;
                top: 0;
                width: 100%;
                height: 2.5rem;
                font-size: 1rem;
                padding-left: 1rem;
                display: flex;
                align-items: center;
                background-color: #0467ad;
                color: #fff;
            }
        </style>

@endsection
