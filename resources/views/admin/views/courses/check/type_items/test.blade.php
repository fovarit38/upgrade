@php
    $client=true;
    $children=$children->get();
    $indexSave=0;
@endphp
<div class="containerfull">
    @foreach($children->where("courseMetaData_id",null) as $index=>$parents)
        @php
            $brews=\App\UserMetaCheckItem::where("userMetaCheckItem_id",$parents->id)->get();
        @endphp
        @include('views.courses.check.testing.one')
        @php
            $indexSave++;
        @endphp
    @endforeach
</div>
