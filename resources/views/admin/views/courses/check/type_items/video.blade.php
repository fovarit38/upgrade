@php
    $linkurl=explode("v=",$children->first()->importance);
    $linkurl=explode("&",end($linkurl));
    $linkurl=[$linkurl[0]];
@endphp

<iframe width="560" height="315" src="https://www.youtube.com/embed/{{end($linkurl)}}" frameborder="0"
        allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
