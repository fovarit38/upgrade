@php
    $client=true;
    $children=$children->get();
    $indexSave=0;
@endphp
<div class="containerfull" style="margin-top: 2rem;">
    @foreach($children->where("courseMetaData_id",null) as $index=>$parents)
        @php
            $brews=\App\UserMetaCheckItem::where("userMetaCheckItem_id",$parents->id)->get();
        @endphp
        @include('views.courses.check.task.one')
        @php
            $indexSave++;
        @endphp
    @endforeach
</div>
