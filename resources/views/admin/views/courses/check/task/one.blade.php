@php
    $codeRoditel=Str::random(5);
    $type="new";

    $valuaPare="";
    if(isset($parents)){
        if(isset($parents->importance)){
            $valuaPare=$parents->importance;
            $codeRoditel=$parents->id;
        }
    }
@endphp
<div class="task_list" >
    <div class="task_item">
        <div class="task_item_header"
             style="background-color: #0467ad; color: #fff; padding: 0.5rem; margin-bottom: 1rem;align-items: flex-start;">
            <div class="task_item_header_icon"><i class="notranslate icn icn-task " aria-hidden="true"
                                                  role="presentation"></i></div>
            <div class="task_item_header_info">{!! $valuaPare !!}</div>
        </div>
        @if(isset($brews))
            @foreach($brews as $bre)
                <div
                          style="width: 100%;min-height: 10rem;border: 1px solid #e6e6e6;padding: 0.5rem 1rem;max-width: 100%;min-width: 100%;">
                    {{$bre->importance}}
                </div>
            @endforeach
        @endif
    </div>
</div>
