@php
    $codeRoditel=Str::random(5);
    $type="new";

    $valuaPare="";
    if(isset($parents)){
        if(isset($parents->importance)){
            $valuaPare=$parents->importance;
            $codeRoditel=$parents->id;
        }
    }
@endphp
<div class="task_list">
    <div class="task_item">
        <div class="task_item_header">
            <div class="task_item_header_icon"><i class="notranslate icn icn-test " aria-hidden="true"
                                                  role="presentation"></i></div>
            <div class="task_item_header_info">Вопрос №<span class="csscounter_item">{{$indexSave+1}}</span></div>
        </div>
        <div class="task_item_question">
            <div class="typo typo-text-rendered ">
                <p>{!! $valuaPare !!}</p></div>
        </div>
        <div class="task_item_answer">
            <div class="answer_list">
                @if(isset($brews))
                    @foreach($brews as $bre)
                        <label
                            class="answer_item answer_item-new {{$bre->boolead==1 && $bre->boolead_true==0 ?"answer_item_false":""}} {{$bre->boolead==1 || $bre->boolead_true==1?'answer_item_true':''}}">
                            <div class="checkcustom">
                                <div class="checkcustom-label control-flexsis">
                                    <div class="checkcustom" style=" width: auto;padding: 0;margin: 0; "><label
                                            class="checkcustom-label noselect"
                                            style=" padding:  0;margin: 0; "><input
                                                name="list[{{$codeRoditel}}][]"
                                                type="checkbox"
                                                disabled
                                                {{$bre->boolead==1?'checked':''}}
                                                class="checkcustom-checkbox checcontrol" required
                                                value="{{$bre->id}}"><span
                                                class="checkcustom-thumbler "></span></label></div>
                                    <div class="text-chech">
                                        {{$bre->importance}}
                                    </div>
                                </div>
                            </div>
                        </label>
                    @endforeach
                @endif

            </div>
        </div>
    </div>
</div>
