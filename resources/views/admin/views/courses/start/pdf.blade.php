@php
    $linkurl=$children->first()->importance;
@endphp

<input type="hidden" name="save[text]" value="{{$children->first()->importance}}">
<input type="hidden" name="save[id]" value="{{$children->first()->id}}">

<iframe
    src="{{$linkurl}}#page=2"
    width="100%"
    height="100%"
    style="border: none;">
    <p>Your browser does not support PDFs.
        <a href="{{$linkurl}}">Download the PDF</a>.</p>
</iframe>
