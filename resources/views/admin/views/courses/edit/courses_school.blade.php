@extends('views.layouts.app')

@section('content')

    @php

        $types_in="curse";

    if(isset($_REQUEST["type"])){
        if($_REQUEST["type"]=="library"){
            $types_in="library";
        }
    }
    if(!is_null($course)){
        $types_in=($course->type);
    }

    @endphp

    <div class="manager">


        <div class="manager_main">
            <div class="manager_main_head"
                 style="width: 100%; background-color: #ebebeb; border-bottom: 1px solid #ebebeb; height: auto; min-height: auto; padding: 1rem 2rem; ">
                <h1 class="interface_view_title with_toggle" style=" font-size: 1rem; ">
                    <span class="header_title_toggle"></span>
                    Редактор
                </h1>
            </div>
            <div class="manager_main_nav">
                <div class="nav_bbl">
                    <a href="{{url_custom('/admin/courses/add/info/'.$course->id)}}" class="btn btn-curseadd ">
                        Информация
                    </a>
                    <a href="{{url_custom('/admin/courses/add/school/'.$course->id)}}" class="btn btn-curseadd active">
                        @if($types_in=="library")
                            Материалы
                        @else
                            Уроки
                        @endif
                    </a>
                    @if($types_in!="library")
                        <a @if(!is_null($course)) href="{{url_custom('/admin/courses/add/collaborator/'.$course->id)}}"
                           @endif class="btn btn-curseadd">
                            Сотрудники
                        </a>
                    @endif
                </div>
            </div>
            @php

                $include_type=['test','video','pdf','text','task'];
                $navInclude=isset($_REQUEST["add"])?$_REQUEST["add"]:false;
                $navEdit=isset($_REQUEST["edit"])?$_REQUEST["edit"]:0;

                if($navInclude){
                  if(!in_array($navInclude,$include_type)){
                    $navInclude=false;
                  }
                }

            @endphp
            <div class="menulistin">
                <div class="sitebarleft">
                    <div class="lessons_list">
                        <ul data-url="{{url_custom('/admin/update/position')}}" class="lessons_list_ul sort_data">
                            @php
                                $icon=["video"=>"icn-videoplayer","text"=>"icn-book","pdf"=>"icn-pdf","test"=>"icn-test","task"=>"icn-task"];
                            @endphp
                            @foreach(\App\CourseMeta::where("course_id",$id)->orderby("sort")->get() as $coursMeta)

                                <li data-id="{{$coursMeta->id}}"
                                    class="cards_board_item_material {{$navEdit!=0?($navEdit==$coursMeta->id?'active':''):''}}">
                                    <a
                                        href="{{'/admin/courses/add/school/' . $id . "?add=" . $coursMeta->type . "&edit=" . $coursMeta->id}}"
                                        class="item_link"></a>
                                    <div style="text-decoration: none;" class="lessons_list_icon"><i
                                            class="notranslate icn {{$icon[$coursMeta->type]}} "
                                            aria-hidden="true" role="presentation"></i>
                                    </div>
                                    <div style="text-decoration: none;" class="lessons_list_info">
                                        <div class="lessons_list_title">{{$coursMeta->name}}</div>

                                    </div>
                                </li>
                            @endforeach
                        </ul>
                        <div class="addblock">
                            <a href="{{url_custom('/admin/courses/add/school/'.$course->id)}}" class="btn btn-add">
                                добавить новый
                            </a>
                        </div>
                    </div>
                </div>
                <div class="manager_main_curses addMenuData">


                    @if($navInclude==false)
                        <div class="addMenuData_main">
                            <div class="addMenuData-title">
                                <h2>Добавление урока</h2>
                            </div>
                            <div class="addMenuData-after">
                                <p>
                                    Выберите, какой урок вы хотите выбрать в данный курс
                                </p>
                            </div>
                            <div class="addMenuData-add">
                                @if($types_in!="library")
                                    <a href="{{url_custom('/admin/courses/add/school/'.$course->id.'?add=test')}}"
                                       class="item-add">
                                        <i class="icn icn-test"></i>
                                        <p>
                                            Тест
                                        </p>
                                    </a>
                                @endif
                                <a href="{{url_custom('/admin/courses/add/school/'.$course->id.'?add=video')}}"
                                   class="item-add">
                                    <i class="icn icn-videoplayer"></i>
                                    <p>
                                        Видео
                                    </p>
                                </a>
                                <a href="{{url_custom('/admin/courses/add/school/'.$course->id.'?add=pdf')}}"
                                   class="item-add">
                                    <i class="icn icn-pdf"></i>
                                    <p>
                                        PDF
                                    </p>
                                </a>
                                <a href="{{url_custom('/admin/courses/add/school/'.$course->id.'?add=text')}}"
                                   class="item-add">
                                    <i class="icn icn-book"></i>
                                    <p>
                                        Текст
                                    </p>
                                </a>
                                @if($types_in!="library")
                                    <a href="{{url_custom('/admin/courses/add/school/'.$course->id.'?add=task')}}"
                                       class="item-add">
                                        <i class="icn icn-task"></i>
                                        <p>
                                            Задание
                                        </p>
                                    </a>
                                @endif
                            </div>
                        </div>
                    @else

                        <form method="post" enctype="multipart/form-data" action="{{url_custom('/admin/itemadd')}}"
                              class="includeControl"
                              style="flex-grow: 2;width: 100%">
                            @csrf

                            @php
                                $meta_catalog=\App\CourseMeta::find($navEdit);
                                $children=\App\CourseMetaData::where("CourseMeta_id",!is_null($meta_catalog)?$meta_catalog->id:0);
                            @endphp


                            <div class="boxForm"
                                 style="display: flex;justify-content: space-between;align-items: center;margin-bottom: 2rem;">
                                <h2 style="font-size: 1.25rem;font-weight: 700;">
                                    Добавить {{$navInclude}}
                                </h2>
                                @if(!is_null($meta_catalog))
                                    <button type="submit" value="{{$meta_catalog->id}}" name="remove"
                                            class="btn btn-danger"
                                            style="font-size: 1rem;margin-left: auto;margin-right: 1rem;">Удалить
                                    </button>
                                @endif
                                <button type="submit" class="btn btn-success" style="font-size: 1rem;">Сохранить
                                </button>
                            </div>
                            {{maskInput($errors,["name"=>"name_save","type"=>"text","placeholder"=>"Название"],$meta_catalog)}}
                            <input type="hidden" name="type" value="{{$navInclude}}">
                            <input type="hidden" name="id" value="{{!is_null($meta_catalog)?$meta_catalog->id:0}}">
                            <input type="hidden" name="courses[id]" value="{{$id}}">

                            @include('views.courses.edit.type_items.'.$navInclude)
                        </form>

                    @endif

                </div>
            </div>
        </div>

        <style>
            .manager_main_curses {
                width: 50%;
                flex-grow: 2;
            }
        </style>

@endsection
