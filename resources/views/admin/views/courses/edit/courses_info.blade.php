@extends('views.layouts.app')

@section('content')

    @php

        $types_in="curse";

    $type_controls='library';
    $controls=false;
    if(isset($_REQUEST["type"])){
        $type_controls=$_REQUEST["type"];
        $controls=true;
    }
    $courseType_id=0;
     if(isset($_REQUEST["courseType_id"])){
            $courseType_id=$_REQUEST["courseType_id"];
    }

    if(!is_null($course)){
        $types_in=($course->type);
    }



    @endphp

    <div class="manager">


        <form enctype="multipart/form-data" method="post" action="{{url_custom("/admin/update")}}" class="manager_main">
            @csrf
            <input type="hidden" name="model_name" value="{{$model_name}}">
            <input type="hidden" name="id" value="{{$id}}">
            <input type="hidden" name="path" value="{{url_custom("/admin/courses/add/school/{id}")}}">

            <div class="manager_main_head"
                 style="width: 100%; background-color: #ebebeb; border-bottom: 1px solid #ebebeb; height: auto; min-height: auto; padding: 1rem 2rem; ">
                <h1 class="interface_view_title with_toggle" style=" font-size: 1rem; ">
                    <span class="header_title_toggle"></span>
                    Редактор
                </h1>
            </div>
            <div class="manager_main_nav">
                <div class="nav_bbl">

                    <a href="{{url_custom('/admin/courses/add/info/0'.($controls?'?type='.$type_controls:''))}}"
                       class="btn btn-curseadd active">
                        Информация
                    </a>

                    <a @if(!is_null($course)) href="{{url_custom('/admin/courses/add/school/'.$course->id.($controls?'?type='.$type_controls:''))}}"
                       @endif class="btn btn-curseadd">
                        @if($controls)
                            Материалы
                        @else
                            Уроки
                        @endif
                    </a>
                    @if(!$controls)
                        <a @if(!is_null($course)) href="{{url_custom('/admin/courses/add/collaborator/'.$course->id)}}"
                           @endif class="btn btn-curseadd">
                            Сотрудники
                        </a>
                    @endif
                </div>
            </div>
            <div class="menulistin">

                <div class="manager_main_curses addMenuData"
                     style=" align-items: flex-start; justify-content: flex-start; ">

                    <div class="addMenuData_main">
                        {{maskInput($errors,["name"=>"images_save","type"=>"file","placeholder"=>"Обложка"],$course)}}
                        {{maskInput($errors,["name"=>"name_save","type"=>"text","placeholder"=>"Название"],$course)}}
                        @if(!$controls)
                            {{maskInput($errors,["name"=>"mini_save","type"=>"text","placeholder"=>"Мини описание"],$course)}}
                            {{maskInput($errors,["name"=>"content_save","type"=>"text","placeholder"=>"Описание"],$course)}}

                            <div class="btn-group" style="width: 100%;flex-wrap: wrap">
                                <label for="basic-url" class="label_control" style="width: 100%;"> <b class="text_bigs">Тип
                                        курса</b>
                                     </label>
                                <div class="row-fluid" style="width: 100%;">
                                    <select class="selectpicker" name="courseType_id_save" required
                                            data-show-subtext="true"
                                            data-live-search="true" style="width: 100%;">
                                        @foreach(\App\CourseType::where("type","courses")->get() as $c_type)
                                            <option value="{{$c_type->id}}"
                                                    {{ !is_null($course)?($course->courseType_id==$c_type->id?'selected':'' ): ''}} data-subtext="">{{strip_tags(LC($c_type->name))}}
                                            </option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        @else
                            <div class="btn-group" style="width: 100%;flex-wrap: wrap">
                                <label for="basic-url" class="label_control" style="width: 100%;"> <b class="text_bigs">Тип
                                        Материала</b>
                                    </label>
                                <div class="row-fluid" style="width: 100%;">
                                    <select class="selectpicker" required name="courseType_id_save"
                                            data-show-subtext="true"
                                            data-live-search="true" style="width: 100%;">
                                        @foreach(\App\CourseType::where("type",$type_controls)->get() as $c_type)
                                            <option value="{{$c_type->id}}"  {{$courseType_id==$c_type->id?"selected":""}}
                                                    {{ !is_null($course)?($course->courseType_id==$c_type->id?'selected':'' ): ''}} data-subtext="">{{strip_tags(LC($c_type->name))}}
                                            </option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        @endif
                        <div class="btn-group" style="width: 100%;margin-top: 1rem;flex-wrap: wrap;">
                            <label for="basic-url" class="label_control" style="width: 100%;"> <b class="text_bigs">Видимость</b></label>
                            <div class="row-fluid" style="width: 100%;">
                                <select class="selectpicker" name="visable_save" data-show-subtext="true"
                                        data-live-search="true" style="width: 100%;">
                                    <option value="0"
                                            {{ !is_null($course)?($course->visable==0?'selected':'' ): ''}} data-subtext="">
                                        не показывать
                                    </option>
                                    <option value="1"
                                            {{ !is_null($course)?($course->visable==1?'selected':'' ): ''}} data-subtext="">
                                        показывать
                                    </option>
                                </select>
                            </div>
                        </div>
                        @if(!$controls)
                            <div class="btn-group" style="width: 100%;margin-top: 1rem;flex-wrap: wrap;">
                                <label for="basic-url" class="label_control" style="width: 100%;"> <b class="text_bigs">Доступ
                                        к курсу</b></label>
                                <div class="row-fluid" style="width: 100%;">
                                    <select class="selectpicker" name="shared_save" data-show-subtext="true"
                                            data-live-search="true" style="width: 100%;">
                                        <option value="1"
                                                {{ !is_null($course)?($course->shared==1?'selected':'' ): ''}} data-subtext="">
                                            По приглашение
                                        </option>
                                        <option value="0"
                                                {{ !is_null($course)?($course->shared==0?'selected':'' ): ''}} data-subtext="">
                                            Свободное посещение
                                        </option>
                                    </select>
                                </div>
                            </div>
                        @else
                            <input type="hidden" name="type_save" value="<?=$type_controls?>">
                        @endif

                        <div class="btn-group" style="width: 100%;margin-top: 1rem;">
                            @if($id>0)
                                <div class="row-fluid"
                                     style="display: flex;margin-right: 2rem; justify-content: flex-end;margin-left: auto;">
                                    <input type="submit" name="remove" class="btn btn-danger" value="Удалить">
                                </div>
                                <div class="row-fluid" style="display: flex;justify-content: flex-end;">
                                    <input type="submit" class="btn btn-success" value="сохранить">
                                </div>
                            @else
                                <div class="row-fluid"
                                     style="display: flex;justify-content: flex-end;margin-left: auto;">
                                    <input type="submit" class="btn btn-success" value="сохранить">
                                </div>
                            @endif

                        </div>
                    </div>


                </div>
            </div>
        </form>


        <style>
            .addMenuData_main, .language_grup {
                width: 100%;
                text-align: left;
            }

            .bootstrap-select {
                width: 100% !important;
            }
        </style>

@endsection
