@extends('views.layouts.app')

@section('content')


    <div class="manager">


        <div class="manager_main">
            <div class="manager_main_head"
                 style="width: 100%; background-color: #ebebeb; border-bottom: 1px solid #ebebeb; height: auto; min-height: auto; padding: 1rem 2rem; ">
                <h1 class="interface_view_title with_toggle" style=" font-size: 1rem; ">
                    <span class="header_title_toggle"></span>
                    Редактор
                </h1>
            </div>
            <div class="manager_main_nav">
                <div class="nav_bbl">
                    <a href="{{url_custom('/admin/courses/add/info/'.$course->id)}}" class="btn btn-curseadd ">
                        Информация
                    </a>
                    <a href="{{url_custom('/admin/courses/add/school/'.$course->id)}}" class="btn btn-curseadd">
                        Уроки
                    </a>
                    <a @if(!is_null($course)) href="{{url_custom('/admin/courses/add/collaborator/'.$course->id)}}"
                       @endif class="btn btn-curseadd active">
                        Сотрудники
                    </a>
                </div>
            </div>
            <div class="menulistin">

                <div class="manager_main_curses addMenuData"
                     style=" align-items: flex-start; justify-content: flex-start; ">

                    <div class="addMenuData_main minimaiz"
                         style="align-items: flex-start;width: auto;min-width: 100%;flex-direction: row;flex-wrap: wrap;">

                        @if(!isset($_REQUEST["add"]))
                            <div class="basdsa"
                                 style="display: flex;justify-content: flex-end;align-items: center;width: 100%;">
                                <a href="{{url_custom('/admin/courses/add/collaborator/'.$course->id.'?add=0')}}"
                                   class="btn btn-info">Добавить</a>
                            </div>

                            <div class="userboxas" style="width: 100%;">
                                <div class="userboxas_head" style=" margin-bottom: 1rem; font-weight: 600; ">
                                    Список пользователей
                                </div>
                                <div style=" width: 100%; ">
                                    <div class="col" style="padding: 0rem;">
                                        <div class="ofl_table">
                                            <table class="table td_middle td_highlight">
                                                <thead>
                                                <tr>
                                                    <th class=" tcol_status"></th>
                                                    <th class=" tcol_user">ФИО работника</th>
                                                    <th class=" tcol_metrics">Баллов</th>
                                                    <th class=" tcol_metrics">Эффективность</th>
                                                    <th class=" tcol_metrics">Медалей</th>
                                                    <th class=" tcol_metrics">Пересдач</th>
                                                    <th class=" tcol_icon"></th>
                                                    <th class=" tcol_metrics">Назначено</th>
                                                    <th class=" tcol_metrics">лимит до</th>
                                                    <th class=" tcol_metrics">Начало</th>
                                                    <th class=" tcol_metrics">Завершение</th>
                                                    <th class=" tcol_menu"></th>
                                                </tr>
                                                </thead>
                                                <tbody>

                                                @php
                                                    $user_list=  (\App\CourseUser::where("course_id",$id)->get());
                                                @endphp
                                                @foreach($user_list as $us)
                                                    @php
                                                        $userInd=\App\User::find($us->user_id);
                                                    @endphp
                                                    <tr class="">
                                                        <td class=" tcol_status">
                                                            <div class="round_status">
                                                                <div class="round_status_icon " data-tooltipped=""
                                                                     aria-describedby="tippy-tooltip-2"
                                                                     data-original-title="Курс еще не начат"><i
                                                                        class="notranslate icn icn-hourglass "
                                                                        aria-hidden="true" role="presentation"></i>
                                                                </div>
                                                            </div>
                                                        </td>
                                                        <td class=" tcol_user">
                                                            <div title="mr.chacov@gmail.com"
                                                                 class="userinfo_block  undefined">
                                                                <div class="userinfo_avatar ">
                                                                    <div class="userinfo_avatar_status offline"></div>
                                                                </div>
                                                                <div class="userinfo_details">
                                                                    <div class="userinfo_name">
                                                                        {{$userInd->name}}
                                                                    </div>
                                                                    <div class="userinfo_desc"><span
                                                                            class="clr_gray">{{$userInd->email}}</span>
                                                                    </div>
                                                                    <div class="userinfo_desc">Админ</div>
                                                                    <div class="userinfo_desc"></div>
                                                                </div>
                                                            </div>
                                                        </td>
                                                        <td class=" tcol_metrics">
                                                            <div class="tcol_metrics_value"><span
                                                                    class="clr_gray">—</span>
                                                            </div>
                                                        </td>
                                                        <td class=" tcol_metrics">
                                                            <div class="tcol_metrics_value"><span
                                                                    class="clr_gray">—</span>
                                                            </div>
                                                        </td>
                                                        <td class=" tcol_metrics">
                                                            <div class="tcol_metrics_value"><span
                                                                    class="clr_gray">—</span>
                                                            </div>
                                                        </td>
                                                        <td class=" tcol_metrics">
                                                            <div class="tcol_metrics_value"><span
                                                                    class="clr_gray">—</span>
                                                            </div>
                                                        </td>
                                                        <td class=" tcol_icon"></td>
                                                        <td class=" tcol_metrics">
                                                            <div class="tcol_metrics_value"><span
                                                                    title="{{$us->created_at}}">{{$us->created_at}}</span>
                                                            </div>
                                                        </td>
                                                        <td class=" tcol_metrics">
                                                            <div class="tcol_metrics_value"><span
                                                                    title="{{$us->courseDateEnd}}">{{$us->courseDateEnd}}</span>
                                                            </div>
                                                        </td>
                                                        <td class=" tcol_metrics">
                                                            <div class="tcol_metrics_value"><span
                                                                    class="clr_gray">—</span>
                                                            </div>
                                                        </td>
                                                        <td class=" tcol_metrics">
                                                            <div class="tcol_metrics_value"><span
                                                                    class="clr_gray">—</span>
                                                            </div>
                                                        </td>
                                                        <td class=" tcol_menu">
                                                            <div class="flex flex_row"><a style="text-decoration: none;"
                                                                                          href="{{url_custom('/admin/courses/add/collaborator/'.$course->id.'?add='.$us->id)}}"><i
                                                                        style="font-size: 1.5rem;"
                                                                        class="notranslate icn icn-edit "
                                                                        aria-hidden="true"
                                                                        role="presentation"></i></a></div>
                                                        </td>
                                                    </tr>
                                                @endforeach
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @else

                            @php

                                @endphp
                            <form action="{{url_custom('/admin/courses/add/users')}}" method="post"
                                  style="width: 100%;">
                                @csrf
                                <input type="hidden" name="curseid" value="{{$id}}">
                                <input type="hidden" name="useridadd" value="{{$_REQUEST["add"]}}">
                                <div class="basdsa"
                                     style="display: flex;justify-content: flex-end;align-items: center;width: 100%;">
                                    <a href="{{url_custom('/admin/courses/add/collaborator/'.$course->id)}}"
                                       class="btn btn-info">Назад</a>
                                    @if($_REQUEST["add"]>0)
                                        <button type="submit" name="remove" value="remove"
                                                class="btn btn-danger" style="margin-left: 2rem;color:#fff;">Удалить
                                        </button>
                                    @endif
                                    <button type="submit"
                                            class="btn btn-success" style="margin-left: 2rem;color:#fff;">Сохранить
                                    </button>
                                </div>

                                @php
                                    $curseUser=  \App\CourseUser::find($_REQUEST["add"]);
                                @endphp


                                {{maskInput($errors,["name"=>"courseDateEnd_save","type"=>"datetime-local","placeholder"=>"Должен закончить до"],$curseUser)}}

                                @if(is_null($curseUser))
                                    <div class="userboxas" style="width: 100%;">
                                        <div class="userboxas_head" style=" margin-bottom: 1rem; font-weight: 600; ">
                                            Список пользователей
                                        </div>
                                        <table
                                            class="table sortTables  table-bordered table-striped js-table dataTable order-table no-footer"
                                            id="DataTables_Table_0" style="width: 100%;" role="grid"
                                            aria-describedby="DataTables_Table_0_info">
                                            <thead>
                                            <tr>

                                                <th role="row">Имя</th>
                                                <th role="row">Email</th>
                                                <th role="row">Телефон</th>
                                                <th role="row">Тип учётной записи</th>
                                                <th role="row">Должность</th>
                                                <th role="row"></th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            @php
                                                $user_idbox=  array_keys(\App\CourseUser::where("course_id",$id)->get()->groupby("user_id")->toarray());
                                            @endphp
                                            @foreach(\App\User::whereNotIn("id",$user_idbox)->get() as $user)
                                                @php
                                                    $idgrups=  \App\Permission_user::checkId($user->id);

                                                    $nameTYpe="";
                                                    $nameGrups= \App\Permission_grup::find($idgrups);
                                                    if(!is_null($nameGrups)){
                                                        $nameTYpe=$nameGrups->name;
                                                    }
                                                   $dolsnost= App\Official::where("id",$user->jobtitle_id)->first();
                                                @endphp
                                                <tr role="row" class="even  ui-sortable-handle">

                                                    <td class=" sorting_1">
                                                        <div class="img-prof_box">
                                                            <div href="<?=$user->images?>" data-fancybox="gallery" class="img-prof">
                                                                <img src="<?=$user->images?>" alt="">
                                                            </div>
                                                            <?=$user->name?></a>
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <?=$user->email?>
                                                    </td>
                                                    <td>
                                                        <?=$user->tel?>
                                                    </td>
                                                    <td>
                                                        {{$nameTYpe}}
                                                    </td>
                                                    <td>
                                                        {{!is_null($dolsnost)?$dolsnost->name:""}}
                                                    </td>
                                                    <td>
                                                        <div class="interface_assign_item_select">
                                                            <div class="checkcustom"><label
                                                                    class="checkcustom-label noselect"><input required
                                                                        type="radio" name="user"
                                                                        class="checkcustom-checkbox "
                                                                        value="{{$user->id}}"><span
                                                                        class="checkcustom-thumbler"></span></label></div>
                                                        </div>
                                                    </td>
                                                </tr>
                                            @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                @else
                                    <input type="hidden" name="user" value="{{$curseUser->user_id}}">
                                    <div class="tesd">
                                        @php
                                            $users=   \App\User::find($curseUser->user_id);
                                        @endphp
                                        Пользователь: <br>
                                        Ф.И.О: <b>{{$users->name}}</b> <br>
                                        E-mail: <b> {{$users->email}}</b>
                                    </div>
                                @endif
                            </form>
                        @endif

                    </div>

                </div>
            </div>
        </div>


        <style>
            .addMenuData_main, .language_grup {
                width: 100%;
                text-align: left;
            }
        </style>

@endsection
