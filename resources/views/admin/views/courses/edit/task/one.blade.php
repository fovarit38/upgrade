@php
    $codeRoditel=Str::random(5);
    $type="new";

    $valuaPare="";
    if(isset($parents)){
        if(isset($parents->importance)){
            $valuaPare=$parents->importance;
        }
    }
@endphp
<div class="quqis" style=" padding: 1rem; border: 1px solid #ccc;margin-bottom: 2rem; ">
    <a href="javascript:void(0)" class="quqis_remove" >
        <div class="interface_assign_item_select f-boxsl" style="width: auto;padding-right: 0rem;padding-left:0rem;display: flex;justify-content: flex-end;margin-bottom: -1rem;">
            <i class="icn filechoose_clear"></i>
        </div>
    </a>
    <div class="quqis_quis">
        {{maskInput($errors,["name"=>"importance[task][".$codeRoditel."][name]","type"=>"textarea","placeholder"=>"Условие","value"=>$valuaPare])}}

    </div>

</div>
