@extends('views.layouts.app')

@section('content')


    <div class="manager">


        <div class="manager_main">
            <div class="manager_main_head"
                 style="width: 100%; background-color: #ebebeb; border-bottom: 1px solid #ebebeb; height: auto; min-height: auto; padding: 1rem 2rem; ">
                <h1 class="interface_view_title with_toggle" style=" font-size: 1rem; ">
                    <span class="header_title_toggle"></span>
                    Редактор
                </h1>
            </div>
            <div class="manager_main_nav">
                <div class="nav_bbl">
                    <a href="{{url_custom('/admin/courses/add/info/'.$course->id)}}" class="btn btn-curseadd active">
                        Информация
                    </a>
                    <a  href="{{url_custom('/admin/courses/add/school/'.$course->id)}}" class="btn btn-curseadd">
                        Уроки
                    </a>
                    <a @if(!is_null($course)) href="{{url_custom('/admin/courses/add/collaborator/'.$course->id)}}"
                       @endif class="btn btn-curseadd">
                        Сотрудники
                    </a>
                </div>
            </div>
            <div class="menulistin">

                <div class="manager_main_curses addMenuData"
                     style=" align-items: flex-start; justify-content: flex-start; ">

                    <div class="addMenuData_main">
                        {{maskInput($errors,["name"=>"name_save","type"=>"text","placeholder"=>"Название"])}}

                    </div>

                </div>
            </div>
        </div>


        <style>
            .addMenuData_main, .language_grup {
                width: 100%;
                text-align: left;
            }
        </style>

@endsection
