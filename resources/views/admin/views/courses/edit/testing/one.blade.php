@php
    $codeRoditel=Str::random(5);
    $type="new";

    $valuaPare="";
    if(isset($parents)){
        if(isset($parents->importance)){
            $valuaPare=$parents->importance;
        }
    }
@endphp
<div class="quqis" style=" padding: 1rem; border: 1px solid #ccc;margin-bottom: 2rem; ">
    <a href="javascript:void(0)" class="quqis_remove" >
        <div class="interface_assign_item_select f-boxsl" style="width: auto;padding-right: 0rem;padding-left:0rem;display: flex;justify-content: flex-end;margin-bottom: -1rem;">
            <i class="icn filechoose_clear"></i>
        </div>
    </a>
    <div class="quqis_quis">
        {{maskInput($errors,["name"=>"importance[test][".$codeRoditel."][name]","type"=>"textarea","placeholder"=>"Вопрос","value"=>$valuaPare])}}
    </div>
    <div class="quqis_result">
        <p style=" width: 100%; padding: 1rem 0; font-weight: bold; font-size: 1.24rem; ">
            Варианты ответов
        </p>
        <div class="language_grup">
            <div class="language_grup_box " style="width:100%" data-langcount="1">
                <div class="language_grup_hid language_grup_multi language_grup_ru active ">

                    <label for="basic-url" class="label_control"> <b class="text_bigs">Ответы</b></label>

                    <div id="x{{$codeRoditel}}" class="ot_list" style="width: 100%;">
                        @if(isset($brews))
                            @foreach($brews as $bre)
                                @include('views.courses.edit.testing.onde_otvet')
                            @endforeach
                        @endif
                    </div>

                    <a href="javascript:void(0)" data-id="{{$codeRoditel}}" data-type="{{$type}}"
                       data-url="{{url_custom('/admin/test/add-otvet')}}"
                       class="btn btn-light add_test_otvet">добавить ответ</a>

                </div>
            </div>
        </div>
    </div>
</div>
