@extends('views.layouts.app')

@section('content')


<div
    class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
    <h1 class="h2" style=" padding-left: 2rem; padding-bottom: 0.5rem; ">Настройки</h1>
</div>

@if (\App\Permission_date::check("users", "select", 'Db', 0))

<style>
    .bodyMain li {
        width: 25%;
    }

    .bodyMain {
        display: flex;
        flex-wrap: wrap;
    }

    .bodyMain .nav-link {
        font-size: 18px;
        padding: 2rem 0;
    }

    .bodyMain ..nav-item {
        border-right: none !important;
    }

    .bodyMain i {
        font-size: 40px;
    }
</style>
<ul class="bodyMain">
    @if (\App\Permission_date::check("users", "select", 'Db', 0))
    <li class="nav-item ">
        <a href="{{url_custom('/admin/users')}}" class="nav-link  " data-un=""
           data-closest=".nav-item">
            <i class="notranslate icn icn-users "
               aria-hidden="true"
               role="presentation"></i>
            <span> Пользователи</span>
        </a>
    </li>
    @endif

    @if (\App\Permission_date::check("courses_check", "select", 'Db', 0))
    <li class="nav-item  ">
        <a href="{{url_custom('/admin/courses_check')}}" class="nav-link  cli" data-un=""
           data-closest=".nav-item">
            <i class="notranslate icn icn-taskcheck "
               aria-hidden="true" role="presentation"></i>
            <span> Проверка заданий</span>
        </a>
    </li>
    @endif

    @if (\App\Permission_date::check("MeterialType", "select", 'Db', 0))
    <li class="nav-item  ">
        <a href="{{url_custom('/admin/model/CourseType?type=library')}}" class="nav-link  " data-un=""
           data-closest=".nav-item">
            <i class="notranslate icn icn-course "
               aria-hidden="true"
               role="presentation"></i>
            <span>Типы коорпоративная библиотека</span>
        </a>
    </li>
    <li class="nav-item  ">
        <a href="{{url_custom('/admin/model/CourseType?type=video')}}" class="nav-link  " data-un=""
           data-closest=".nav-item">
            <i class="notranslate icn icn-course "
               aria-hidden="true"
               role="presentation"></i>
            <span>Типы видео инструкция о должности</span>
        </a>
    </li>

    <li class="nav-item  ">
        <a href="{{url_custom('/admin/model/CourseType?type=instructions')}}" class="nav-link  " data-un=""
           data-closest=".nav-item">
            <i class="notranslate icn icn-course "
               aria-hidden="true"
               role="presentation"></i>
            <span>Типы должностные инструкции</span>
        </a>
    </li>

    @endif

    @if (\App\Permission_date::check("CourseType", "select", 'Db', 0))
    <li class="nav-item ">
        <a href="{{url_custom('/admin/model/CourseType')}}" class="nav-link  " data-un=""
           data-closest=".nav-item">
            <i class="notranslate icn icn-course "
               aria-hidden="true"
               role="presentation"></i>
            <span>Типы стандарты работы</span>
        </a>
    </li>
    @endif

    @if (\App\Permission_date::check("Official", "select", 'Db', 0))
    <li class="nav-item  ">
        <a href="{{url_custom('/admin/model/Official')}}" class="nav-link  " data-un=""
           data-closest=".nav-item">
            <i class="notranslate icn icn-user_f "
               aria-hidden="true"
               role="presentation"></i>
            <span>Должности</span>
        </a>
    </li>
    @endif

    @if (\App\Permission_date::check("StaticText", "select", 'Db', 0))
    <li class="nav-item ">
        <a href="{{url_custom('/admin/StaticText')}}" class="nav-link  " data-un=""
           data-closest=".nav-item">
            <i class="notranslate icn icn-book "
               aria-hidden="true"
               role="presentation"></i>
            <span> текста сайта</span>
        </a>
    </li>
    @endif

    @if (\App\Permission_date::check("compani", "select", 'Db', 0))
    <li class="nav-item  ">
        <a href="{{url_custom('/admin/compani/list')}}" class="nav-link  " data-un=""
           data-closest=".nav-item">
            <i class="notranslate icn icn-users "
               aria-hidden="true"
               role="presentation"></i>
            <span>Подразделение</span>
        </a>
    </li>
    @endif

    @if (\App\Permission_date::check("Register", "select", 'Db', 0))
    <li class="nav-item ">
        <a href="{{url_custom('/admin/model/Register')}}" class="nav-link  " data-un=""
           data-closest=".nav-item">
            <i class="notranslate icn icn-users "
               aria-hidden="true"
               role="presentation"></i>
            <span> Регистр действий</span>
        </a>
    </li>
    @endif

    @if (\App\Permission_date::check("Permission_grup", "select", 'Db', 0))
    <li class="nav-item ">
        <a href="{{url_custom('/admin/model/Permission_grup')}}" class="nav-link  " data-un=""
           data-closest=".nav-item">
            <i class="notranslate icn icn-users "
               aria-hidden="true"
               role="presentation"></i>
            <span> Права доступа</span>
        </a>
    </li>
    @endif
</ul>
@endif

@endsection
