@extends('views.layouts.app')

@section('content')


    <div class="course_view">
        <div class="course_header">
            <div class="course_header_info">
                <div class="interface_view_header">
                    <div class="interface_view_header_info">
                        <div class="brcr"><a href="">Курсы</a><span>{{$course->name}}</span>
                        </div>
                        <div class="course_title">{{$course->name}}</div>
                    </div>
                    <div class="interface_view_header_actions"></div>
                </div>
            </div>
        </div>
        <div class="course_content">
            <div class="course_content_view">
                <div class="interface_subnav">

                    <a href="javascript:void(0)" class="current cliCurse">Описание</a>

                    <a href="javascript:void(0)" class="cliCurse">Студенты</a>

                    <div style="margin-left: auto;display: flex;align-items: center">
                        @php
                            $check_list=  (\App\CourseUser::where("course_id",$id)->where("user_id",Auth::user()->id)->first());

                        @endphp
                        @if(!is_null($check_list) || $course->shared=="0")

                            <div
                                {{$btn[0]?'onclick=window.location=\''.url_custom('/admin/courses/'.$course->id."/start").'\'':'onclick="return false;"'}}
                                style=" font-size: 0.85rem; " class="btn btn-success">
                                {{$btn[1]}}
                            </div>

                        @else
                            <div
                                style=" font-size: 0.85rem; ">
                                Курс не назначен
                            </div>
                        @endif
                    </div>

                </div>
                <div class="cour_box">
                    <div class="main_surce active">
                        <div class="course_desc">
                            {!! $course->content !!}
                        </div>
                        <div class="bigdesc_bar" style="padding-bottom: 0px;">
                            <div class="bigdesc ">
                                <div class="bigdesc_title ">Теория</div>
                                <div class="bigdesc_item ">
                                    <div class="bigdesc_item_icon"><i class="notranslate icn icn-book "
                                                                      aria-hidden="true"
                                                                      role="presentation"></i></div>
                                    <div
                                        class="bigdesc_item_value">{{\App\CourseMeta::where("course_id",$id)->where("type","text")->count()}}</div>
                                    <div class="bigdesc_item_title">Текст</div>
                                </div>
                                <div class="bigdesc_item ">
                                    <div class="bigdesc_item_icon"><i class="notranslate icn icn-videoplayer "
                                                                      aria-hidden="true" role="presentation"></i></div>
                                    <div
                                        class="bigdesc_item_value">{{\App\CourseMeta::where("course_id",$id)->where("type","video")->count()}}</div>
                                    <div class="bigdesc_item_title">Видео</div>
                                </div>
                            </div>
                            <div class="bigdesc ">
                                <div class="bigdesc_title ">Практика</div>
                                <div class="bigdesc_item ">
                                    <div class="bigdesc_item_icon"><i class="notranslate icn icn-test "
                                                                      aria-hidden="true"
                                                                      role="presentation"></i></div>
                                    <div
                                        class="bigdesc_item_value">{{\App\CourseMeta::where("course_id",$id)->where("type","test")->count()}}</div>
                                    <div class="bigdesc_item_title">Тест</div>
                                </div>
                            </div>
                        </div>
                        {{--                        <div class="form_bar">--}}
                        {{--                            <div class="interface_view_header_filters" style="width: 100%;">--}}
                        {{--                                <input type="search" class="text input_text" value="" style="width: 100%;"--}}
                        {{--                                       placeholder="Поиск курсов">--}}
                        {{--                            </div>--}}
                        {{--                        </div>--}}
                        <div class="row" style="margin-top: 2rem;">
                            <div class="col" style=" padding: 0 1.5rem; ">
                                <div class="lessons_list">
                                    <ul class="lessons_list">
                                        @php
                                            $icon=["video"=>"icn-videoplayer","text"=>"icn-book","pdf"=>"icn-pdf","test"=>"icn-test","task"=>"icn-task"];
                                        @endphp
                                        @foreach(\App\CourseMeta::where("course_id",$id)->orderby("sort")->get() as $coursMeta)
                                            <li class="cards_board_item_material ">
                                                <div style="text-decoration: none;" class="lessons_list_icon"><i
                                                        class="notranslate icn {{$icon[$coursMeta->type]}} "
                                                        aria-hidden="true" role="presentation"></i>
                                                </div>
                                                <div style="text-decoration: none;" class="lessons_list_info">
                                                    <div class="lessons_list_title">{{$coursMeta->name}}</div>
                                                </div>
                                            </li>
                                        @endforeach
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="main_surce">

                        <div class="bigdesc_bar">
                            <div class="radialprogress">
                                <div class="radialprogress_item">
                                    <div class="radialprogress_item_value">
                                        <div class="radialprogress_item_value_text clr_primary">50%</div>
                                        <div class="svgchart_circle clr_primary">
                                            <svg class="svgchart_circle_progress" viewBox="0 0 120 120">
                                                <circle class="svgchart_circle_progress_meter" cx="60" cy="60"
                                                        r="50"></circle>
                                                <circle class="svgchart_circle_progress_value" cx="60" cy="60" r="50"
                                                        style="stroke-dasharray: 314.159; stroke-dashoffset: 157.08;"></circle>
                                            </svg>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            @php
                                $user_list=  (\App\CourseUser::where("course_id",$id)->get());

                            @endphp
                            <div class="bigdesc ">
                                <div class="bigdesc_title ">Общее</div>
                                <div class="bigdesc_item ">
                                    <div class="bigdesc_item_value" style=" margin-left: 0; "><span
                                            class="bigdesc_item_value_sub">0 / {{count($user_list)}}</span>
                                    </div>
                                    <div class="bigdesc_item_title" style=" margin-left: 0; ">Студенты</div>
                                </div>
                            </div>
                            <div class="bigdesc ">
                                <div class="bigdesc_title ">Результаты</div>
                                <div class="bigdesc_item ">
                                    <div class="bigdesc_item_icon"><i class="notranslate icn icn-coin "
                                                                      aria-hidden="true" role="presentation"></i></div>
                                    <div class="bigdesc_item_value">0<span
                                            class="bigdesc_item_value_sub"> / 6.0</span></div>
                                    <div class="bigdesc_item_title">Баллов</div>
                                </div>
                                <div class="bigdesc_item ">
                                    <div class="bigdesc_item_icon"><i class="notranslate icn icn-profits "
                                                                      aria-hidden="true" role="presentation"></i></div>
                                    <div class="bigdesc_item_value">0%</div>
                                    <div class="bigdesc_item_title">Эффективность</div>
                                </div>
                                <div class="bigdesc_item ">
                                    <div class="bigdesc_item_icon"><img
                                            src="/public/media/client/images/certificate_gold.17df3ed6.svg"
                                            role="presentation"></div>
                                    <div class="bigdesc_item_value">0</div>
                                    <div class="bigdesc_item_title">Медалей</div>
                                </div>
                                <div class="bigdesc_item ">
                                    <div class="bigdesc_item_value">0</div>
                                    <div class="bigdesc_item_title">Пересдачи</div>
                                </div>
                            </div>

                            <div class="form_bar deltamaiz" style=" padding: 0; ">
                                                              <div class="form_bl form_bl_expanded"><input type="search" class="text name"
                                                                                                           placeholder="ФИО студента"></div>
                                                              <div class="form_bl">
                                                                  <div class="select"><select>
                                                                            <option value="all" selected="">Все статусы</option>
                                                                          <option value="not_started">Не начали</option>
                                                                            <option value="uncompleted">Обучаются</option>
                                                                          <option value="confirmed">Завершили</option>
                                                                          <option value="failed">Провалили</option>
                                                                      </select></div>
                                                               </div>
                            </div>


                            <div style=" width: 100%; ">
                                <div class="" style="padding-bottom: 6rem;">
                                    <div class="ofl_table">
                                        <div style=" width: 100%; ">
                                            <div class="" style="padding-bottom: 6rem;">
                                                <div class="ofl_table">
                                                    <table class="table td_middle td_highlight">
                                                        <thead>
                                                        <tr>
                                                            <th class=" tcol_status"></th>
                                                            <th class=" tcol_user">ФИО студента</th>
                                                            <th class=" tcol_metrics">Баллов</th>
                                                            <th class=" tcol_metrics">Эффективность</th>
                                                            <th class=" tcol_metrics">Медалей</th>
                                                            <th class=" tcol_metrics">Пересдач</th>
                                                            <th class=" tcol_icon"></th>
                                                            <th class=" tcol_metrics">Назначено</th>
                                                            <th class=" tcol_metrics">Начало</th>
                                                            <th class=" tcol_metrics">Завершение</th>
                                                            @if (\App\Permission_date::check("Official", "select", 'Db', 0))
                                                                <th class=" tcol_menu"></th>
                                                            @endif
                                                        </tr>
                                                        </thead>
                                                        <tbody>
                                                        @php
                                                            $user_list=\App\UserMetaCheck::where("course_id",$course->id)->where("status","!=","progressing")->get();
                                                        @endphp
                                                        @foreach($user_list as $us)
                                                            @php
                                                                $userInd=\App\User::find($us->user_id);
                                                                $curse_user= \App\CourseUser::where("user_id",$userInd->id)->where("course_id",$us->course_id)->first();
                                                                 $bonus_list=  (\App\BonusAdd::where("code",$us->code)->get());
                                                                 $bonusi="—";
                                                                 $medal="—";
                                                                 $efficiency="—";
                                                                 if(count($bonus_list)>0){
                                                                     $bonusi=0;
                                                                     $medal=0;
                                                                     $efficiency=0;
                                                                 }
                                                                 foreach ($bonus_list as $coses){
                                                                    $bonusi+=$coses->bonus;
                                                                    $medal+=$coses->medal;
                                                                    $efficiency+=$coses->efficiency;
                                                                 }
                                                                 if($efficiency!="—"){
                                                                     $efficiency.="%";
                                                                 }

                                                            @endphp
                                                            <tr class="">
                                                                <td class=" tcol_status">
                                                                    <div class="round_status">
                                                                        <div class="round_status_icon "
                                                                             data-tooltipped=""
                                                                             aria-describedby="tippy-tooltip-2"
                                                                             data-original-title="Курс еще не начат">
                                                                            <i
                                                                                class="notranslate icn icn-hourglass "
                                                                                aria-hidden="true"
                                                                                role="presentation"></i></div>
                                                                    </div>
                                                                </td>
                                                                <td class=" tcol_user">

                                                                    @php
                                                                        $linksa="javascript:void(0);";
                                                                    @endphp
                                                                    @if (\App\Permission_date::check("Official", "select", 'Db', 0))
                                                                        @php
                                                                            $linksa=url_custom('/admin/courses/check/'.$us->code);
                                                                        @endphp
                                                                    @endif

                                                                    <a href="{{$linksa}}" style="text-decoration: none;" title="mr.chacov@gmail.com"
                                                                       class="userinfo_block  undefined">
                                                                        <div class="userinfo_avatar ">
                                                                            <div
                                                                                class="userinfo_avatar_status offline">
                                                                            </div>
                                                                            <img src="{{$userInd->images}}" alt="">
                                                                        </div>
                                                                        <div class="userinfo_details">
                                                                            <div
                                                                                class="userinfo_name">{{$userInd->name}}
                                                                            </div>
                                                                            <div class="userinfo_desc"><span
                                                                                    class="clr_gray">{{$userInd->email}}</span>
                                                                            </div>
                                                                            <div class="userinfo_desc">Курс</div>
                                                                            <div
                                                                                class="userinfo_desc">{{$us->name}}</div>
                                                                        </div>
                                                                    </a>

                                                                </td>
                                                                <td class=" tcol_metrics">
                                                                    <div class="tcol_metrics_value"><span
                                                                            class="clr_gray">{{$bonusi}}</span>
                                                                    </div>
                                                                </td>
                                                                <td class=" tcol_metrics">
                                                                    <div class="tcol_metrics_value"><span
                                                                            class="clr_gray">{{$efficiency}}</span>
                                                                    </div>
                                                                </td>
                                                                <td class=" tcol_metrics">
                                                                    <div class="tcol_metrics_value"><span
                                                                            class="clr_gray">{{$medal}}</span>
                                                                    </div>
                                                                </td>
                                                                <td class=" tcol_metrics">
                                                                    <div class="tcol_metrics_value"><span
                                                                            class="clr_gray">—</span>
                                                                    </div>
                                                                </td>
                                                                <td class=" tcol_icon"></td>
                                                                <td class=" tcol_metrics">
                                                                    @if(!is_null($curse_user))
                                                                        <div class="tcol_metrics_value"><span
                                                                                title="{{Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $curse_user->created_at)->format('d-m-Y')}}">{{Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $us->created_at)->format('d-m-Y')}}</span>
                                                                        </div>
                                                                    @endif
                                                                </td>
                                                                <td class=" tcol_metrics">
                                                                    @if(!is_null($us))
                                                                        <div class="tcol_metrics_value"><span
                                                                                class="clr_gray">{{Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $us->created_at)->format('d-m-Y H:i:s')}}</span>
                                                                        </div>
                                                                    @endif
                                                                </td>
                                                                <td class=" tcol_metrics">
                                                                    @if(!is_null($us))
                                                                        <div class="tcol_metrics_value"><span
                                                                                class="clr_gray">{{Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $us->done_time)->format('d-m-Y H:i:s')}}</span>
                                                                        </div>
                                                                    @endif
                                                                </td>
                                                                @if (\App\Permission_date::check("Official", "select", 'Db', 0))
                                                                    <td class=" tcol_menu">
                                                                        <a style="text-decoration: none"
                                                                           href="{{url_custom('/admin/courses/check/'.$us->code)}}"
                                                                           class="flex flex_row"><i
                                                                                class="notranslate icn icn-more_v "
                                                                                aria-hidden="true"
                                                                                role="presentation"></i></a>
                                                                    </td>
                                                                @endif
                                                            </tr>
                                                        @endforeach
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>

    <style>

    </style>

@endsection
