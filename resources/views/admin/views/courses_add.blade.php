@extends('views.layouts.app')

@section('content')


    <div class="manager">


        <div class="manager_main">
            <div class="manager_main_head"
                 style="width: 100%; background-color: #ebebeb; border-bottom: 1px solid #ebebeb; height: auto; min-height: auto; padding: 1rem 2rem; ">
                <h1 class="interface_view_title with_toggle" style=" font-size: 1rem; ">
                    <span class="header_title_toggle"></span>
                    Информация о пользователе
                </h1>

            </div>

            <div class="menulistin">
                <div class="sitebarleft">
                    <div class="lessons_list">
                        <ul class="lessons_list">
                            <li class="cards_board_item_material undefined"><a class="item_link"></a>
                                <div class="lessons_list_icon"><i class="notranslate icn icn-videoplayer "
                                                                  aria-hidden="true" role="presentation"></i>
                                </div>
                                <div class="lessons_list_info">
                                    <div class="lessons_list_title">Приветствие</div>
                                </div>
                            </li>

                        </ul>
                    </div>
                </div>
                <div class="manager_main_curses addMenuData">

                    <div class="addMenuData_main">
                        <div class="addMenuData-title">
                            <h2>Добавление урока</h2>
                        </div>
                        <div class="addMenuData-after">
                            <p>
                                Выберите, какой урок вы хотите выбрать в данный курс
                            </p>
                        </div>
                        <div class="addMenuData-add">
                            <div class="item-add">
                                <i class="icn icn-test"></i>
                                <p>
                                    Тест
                                </p>
                            </div>
                            <div class="item-add">
                                <i class="icn icn-videoplayer"></i>
                                <p>
                                    Видео
                                </p>
                            </div>
                            <div class="item-add">
                                <i class="icn icn-pdf"></i>
                                <p>
                                    PDF
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>


@endsection
