<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>{{ config('app.name', 'Laravel') }} :: Dashboard</title>
    <link rel="stylesheet" href="https://unicons.iconscout.com/release/v2.1.11/css/unicons.css">
    <link href="https://fonts.googleapis.com/css2?family=Open+Sans:wght@300;400;600;700&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="/public/media/admin/css/style.css?v=9">
</head>
<body>


<form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
    @csrf
</form>

<div class="container-fluid">
    <section class="mainsection row">


        <nav style="display: none;" class="navbar navbar-dark fixed-top bg-dark flex-md-nowrap p-0 shadow">
            <a class="navbar-brand  mr-0" href="#">{{ config('app.name', 'Laravel') }}</a>
            <ul class="navbar-nav px-4">
                <li class="nav-item text-nowrap">
                    <a class="userExit" href="javascript:void(0);" style="color:#fff;" title="Выйти"
                       onclick="event.preventDefault();document.getElementById('logout-form').submit();"> Выйти</a>
                </li>
            </ul>
        </nav>

        <nav class=" d-none d-md-block bg-light sidebar">

            <div class="sidebar-sticky">
                @php
                $model_urlink="";
                if(isset($model_name)){
                $model_urlink= $model_name;
                }
                $id_saving=-1;

                if(isset($id)){
                $id_saving=$id;
                }

                $dev_status=false;

                if (Cache::has('dev')){
                $dev_status=Cache::get('dev');
                }
                $navHas=[];
                if(isset($nav)){
                $navHas=$nav;
                }
                @endphp

                <ul class="nav ">
                    <li class="nav-item   " style="margin-bottom: 2.77rem; ">
                        <div class="prop"
                             style="background-color: #A6C437; display: flex; justify-content: center; align-items: center;">
                            <div class="s" style="width: 100%;">
                                <div class="prop_img" style="    border: none;padding-top: 45%;">
                                    <div class="prop_img_src"
                                         style="background-image: url('/public/media/admin/img/logo.png'); background-position: center center; background-repeat: no-repeat;background-size: 9.77rem;">

                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="info_modul">
                            <?php
                            if (isset($_REQUEST['type'])) {
                            if (isset($nav)) {
                                array_push($nav, $_REQUEST['type']);
                            }
                            }
                            if (isset($nav)) {
                                if (in_array('book', $nav)) {
                                    $_REQUEST['ti'] = 2;
                                }
                                if (in_array('video', $nav)) {
                                    $_REQUEST['ti'] = 2;
                                }
                                if (in_array('personal', $nav)) {
                                    $_REQUEST['ti'] = 2;
                                }
                                if (in_array('regulations', $nav)) {
                                    $_REQUEST['ti'] = 2;
                                }
                            }

                            if (isset($_REQUEST['ti'])) {
                                echo 'Список материалов ';
                            } else {
                                echo 'Обучение и развитие';
                            }
                            ?>
                        </div>
                    </li>
                    <?php


                    $nav = [
                        ['Стандарты <br> работы', '/public/media/admin/img/boxicon/podbpt.svg', ' ', url_custom('/admin/courses')],
                        ['Коорпоративная <br> библиотека', '/public/media/admin/img/boxicon/podbpt.svg', ' ', url_custom('/admin/library')],
                        ['Должностные <br> инструкции', '/public/media/admin/img/boxicon/podbpt.svg', ' ', url_custom('/admin/instructions?type=instructions')],
                    ];
                    if (isset($_REQUEST['ti'])) {
                        $nav = [
                            ['Книга <br> новичка', '/public/media/admin/img/boxicon/podbpt.svg', ' ', url_custom('/admin/book')],
                            ['Видео инструкция <br> о должности', '/public/media/admin/img/boxicon/podbpt.svg', ' ', url_custom('/admin/video?type=video')],
                            ['Внутренний <br> трудовой распорядок', '/public/media/admin/img/boxicon/podbpt.svg', ' ', url_custom('/admin/personal')],
                            ['Регламенты <br> компании', '/public/media/admin/img/boxicon/podbpt.svg', ' ', url_custom('/admin/regulations')],
                        ];
                    }
                    foreach ($nav as $n) {
                        ?>
                        <li class="nav-item   ">
                            <a href="{!!$n[3]!!}"
                               class="nav-link {!!$n[2]!!} {{$_SERVER['REQUEST_URI'] == $n[3] ? 'active' : ''}}"
                               data-un=""
                               data-closest=".nav-item">
                                <span class="icons_text text">{!!$n[0]!!}</span>
                                <span class="icons_text icons_text-mini text">Прочитано 0/0</span>
                            </a>
                        </li>
                        <?php
                    }
                    ?>


                    <style>
                        .icons_menu {
                            width: 2rem;
                            height: 2rem;
                            display: flex;
                            background-size: contain;
                            background-repeat: no-repeat;
                            background-position: center center;
                            align-items: center;
                            margin-left: 1rem;

                        }

                        .sidebar-sticky {
                            height: 100vh !important;
                        }

                        .opacity {
                            opacity: 0.3;
                        }

                        .sidebar .nav-link {
                            display: flex;
                            flex-direction: column;
                            justify-content: flex-start;
                            text-align: left !important;
                            padding-right: 15px;
                        }

                        .icons_text-mini {
                            font-size: 0.777rem !important;
                            text-transform: capitalize !important;
                            margin-top: 10px;
                        }

                        .icons_text {
                            margin-left: 1rem;
                            font-size: 0.8rem;
                            color: #C4C4C4;
                            width: 100%;
                            font-size: 1.3333rem;
                            flex-grow: 2;
                        }

                        .btn_s {
                            text-decoration: none !important;
                        }

                        .btn_s:hover {
                            text-decoration: none !important;
                        }
                    </style>


                </ul>


            </div>
        </nav>
        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
            @csrf
        </form>
        <main class="main" role="main">
            <div class="main_head">
                <img style=" width: 2.5rem; " src="/public/media/admin/img/icons/notif.png" alt="">
                <img style=" width: 2rem; " src="/public/media/admin/img/icons/mess.png" alt="">

                <a href="{{url_custom('/admin/users/'.Auth::user()->id.'')}}" data-un=""
                   data-closest=".nav-item">
                    <img style=" width: 4.5rem; " src="/public/media/admin/img/icons/use.png" alt="">
                </a>
            </div>
            <div class="main_box">
                @yield('content')
            </div>

        </main>
    </section>
</div>
<script src="/public/media/admin/js/app.js?v=10"></script>

<script>

    $(function () {
        <?php
        if(\Session::has('alert')){
        ?>
        $.fancybox.open('<?=Session::get('alert')?>');
        <?
        }
        ?>
    });

</script>

<style>
    .app__table[data-v-3551e161] {
        width: 100%;
        border-collapse: collapse;
        margin-top: 5rem;
        position: absolute;
        top: 7rem;
    }

    .app__table thead[data-v-3551e161] {
        background: #f5f5f7;
    }

    .app__table[data-v-3551e161] {
        width: 100%;
        border-collapse: collapse;
    }

    .app__table td[data-v-3551e161], .app__table th[data-v-3551e161] {
        padding: 1rem;
        text-align: left;
    }

    .app__table td[data-v-3551e161], .app__table th[data-v-3551e161] {
        padding: 1rem;
        text-align: left;
    }

    .app__table tbody[data-v-3551e161] {
        background: #fff;
    }

    .btnsBox {
        width: calc(100% - 5rem);
        padding-right: 5rem;
        box-sizing: border-box;
    }
</style>
@yield('script')

</body>
</html>
