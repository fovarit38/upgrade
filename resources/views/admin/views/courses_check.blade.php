@extends('views.layouts.app')

@section('content')


    <div class="manager">

        <div class="manager_main">
            <div class="form_bar deltamaiz">
                <div class="form_bl form_bl_expanded"><input type="search" class="text name"
                                                             placeholder="Поиск"></div>
                <div class="form_bl">
                    <div class="select"><select>
                            <option value="all" selected="">Все курсы</option>
                        </select></div>
                </div>
                <div class="form_bl">
                    <div class="select"><select>
                            <option value="all" selected="">Все статусы</option>
                            <option value="not_started">Не начали</option>
                            <option value="uncompleted">Обучаются</option>
                            <option value="confirmed">Завершили</option>
                            <option value="failed">Провалили</option>
                        </select></div>
                </div>
            </div>


            <div style=" width: 100%; ">
                <div class="col" style="padding-bottom: 6rem;">
                    <div class="ofl_table">
                        <table class="table td_middle td_highlight">
                            <thead>
                            <tr>
                                <th class=" tcol_status"></th>
                                <th class=" tcol_user">ФИО студента</th>
                                <th class=" tcol_metrics">Баллов</th>
                                <th class=" tcol_metrics">Эффективность</th>
                                <th class=" tcol_metrics">Медалей</th>
                                <th class=" tcol_metrics">Пересдач</th>
                                <th class=" tcol_icon"></th>
                                <th class=" tcol_metrics">Назначено</th>
                                <th class=" tcol_metrics">Начало</th>
                                <th class=" tcol_metrics">Завершение</th>
                                <th class=" tcol_menu"></th>
                            </tr>
                            </thead>
                            <tbody>
                            @php
                                $user_list=\App\UserMetaCheck::where("status","!=","progressing")->get();
                            @endphp
                            @foreach($user_list as $us)
                                @php
                                    $userInd=\App\User::find($us->user_id);
                            if(!is_null($userInd)){
                                    $curse_user= \App\CourseUser::where("user_id",$userInd->id)->where("course_id",$us->course_id)->first();
                                     $bonus_list=  (\App\BonusAdd::where("code",$us->code)->get());
                                     $bonusi="—";
                                     $medal="—";
                                     $efficiency="—";
                                     if(count($bonus_list)>0){
                                         $bonusi=0;
                                         $medal=0;
                                         $efficiency=0;
                                     }
                                     foreach ($bonus_list as $coses){
                                        $bonusi+=$coses->bonus;
                                        $medal+=$coses->medal;
                                        $efficiency+=$coses->efficiency;
                                     }
                                     if($efficiency!="—"){
                                         $efficiency.="%";
                                     }

                                @endphp
                                <tr class="">
                                    <td class=" tcol_status">
                                        <div class="round_status">
                                            <div class="round_status_icon " data-tooltipped=""
                                                 aria-describedby="tippy-tooltip-2"
                                                 data-original-title="Курс еще не начат"><i
                                                    class="notranslate icn icn-hourglass "
                                                    aria-hidden="true" role="presentation"></i></div>
                                        </div>
                                    </td>
                                    <td class=" tcol_user">

                                        @php
                                            $linksa="javascript:void(0);";
                                        @endphp
                                        @if (\App\Permission_date::check("Official", "select", 'Db', 0))
                                            @php
                                                $linksa=url_custom('/admin/courses/check/'.$us->code);
                                            @endphp
                                        @endif
                                        <a href="{{$linksa}}" style="text-decoration: none;" title="mr.chacov@gmail.com"
                                             class="userinfo_block  undefined">
                                            <div class="userinfo_avatar ">
                                                <div class="userinfo_avatar_status offline">
                                                </div>
                                                <img src="{{$userInd->images}}" alt="">
                                            </div>
                                            <div class="userinfo_details">
                                                <div class="userinfo_name">{{$userInd->name}}
                                                </div>
                                                <div class="userinfo_desc"><span
                                                        class="clr_gray">{{$userInd->email}}</span>
                                                </div>
                                                <div class="userinfo_desc">Курс</div>
                                                <div class="userinfo_desc">{{$us->name}}</div>
                                            </div>
                                        </a>
                                    </td>
                                    <td class=" tcol_metrics">
                                        <div class="tcol_metrics_value"><span class="clr_gray">{{$bonusi}}</span>
                                        </div>
                                    </td>
                                    <td class=" tcol_metrics">
                                        <div class="tcol_metrics_value"><span class="clr_gray">{{$efficiency}}</span>
                                        </div>
                                    </td>
                                    <td class=" tcol_metrics">
                                        <div class="tcol_metrics_value"><span class="clr_gray">{{$medal}}</span>
                                        </div>
                                    </td>
                                    <td class=" tcol_metrics">
                                        <div class="tcol_metrics_value"><span class="clr_gray">—</span>
                                        </div>
                                    </td>
                                    <td class=" tcol_icon"></td>
                                    <td class=" tcol_metrics">
                                        @if(!is_null($curse_user))
                                        <div class="tcol_metrics_value"><span
                                                title="{{Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $curse_user->created_at)->format('d-m-Y')}}">{{Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $us->created_at)->format('d-m-Y')}}</span>
                                        </div>
                                        @endif
                                    </td>
                                    <td class=" tcol_metrics">
                                        @if(!is_null($us))
                                        <div class="tcol_metrics_value"><span
                                                class="clr_gray">{{Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $us->created_at)->format('d-m-Y H:i:s')}}</span>
                                        </div>
                                        @endif
                                    </td>
                                    <td class=" tcol_metrics">
                                        <div class="tcol_metrics_value"><span
                                                class="clr_gray">{{Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $us->done_time)->format('d-m-Y H:i:s')}}</span>
                                        </div>
                                    </td>
                                    <td class=" tcol_menu">
                                        <a style="text-decoration: none"
                                           href="{{url_custom('/admin/courses/check/'.$us->code)}}"
                                           class="flex flex_row"><i
                                                class="notranslate icn icn-more_v "
                                                aria-hidden="true"
                                                role="presentation"></i></a>
                                    </td>
                                </tr>
                            @php
                            }
                            @endphp
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>

        </div>

    </div>


@endsection
