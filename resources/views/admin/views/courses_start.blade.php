@extends('views.layouts.app')

@section('content')


    <form action="{{url_custom('/admin/courses/'.$course->id."/start/".$chec_is->code."/next")}}" method="post"
          class="manager">
        @csrf

        <div class="manager_main">
            <div class="manager_main_head"
                 style="width: 100%; background-color: #ebebeb; border-bottom: 1px solid #ebebeb; height: auto; min-height: auto; padding: 1rem 2rem; ">
                <h1 class="interface_view_title with_toggle" style=" font-size: 1rem; ">
                    <span class="header_title_toggle"></span>
                    Прохождение курса: {{$course->name}}
                </h1>
            </div>

            @php

                $include_type=['test','video','pdf','text','task'];
              $navInclude=isset($_REQUEST["add"])?$_REQUEST["add"]:false;
              $navEdit=isset($_REQUEST["edit"])?$_REQUEST["edit"]:0;

              if($navInclude){
                if(!in_array($navInclude,$include_type)){
                  $navInclude=false;
                }
              }

            @endphp
            <div class="menulistin">
                <div class="sitebarleft">
                    <div class="lessons_list">
                        <ul class="lessons_list_ul">
                            @php
                                $currentData=null;
                                  $icon=["video"=>"icn-videoplayer","text"=>"icn-book","pdf"=>"icn-pdf","test"=>"icn-test","task"=>"icn-task"];
                                $currentTest=0;
                                $nextBox=null;
                                $currentadd=isset($_REQUEST["add"])?$_REQUEST["add"]:null;

                            $chec_is = \App\UserMetaCheck::where("re-publish", "0")->where("course_id", $course->id)->first();
$end=false;
                            @endphp
                            @foreach($courseMeta as $keyxs=>$coursMeta)

                                @php

                                    if(is_null($currentData)){
                                        if(!in_array($coursMeta->id,$pre_ingnor)){
                                                                            $currentData=$coursMeta;
                                             if(count($courseMeta)-1==($keyxs)){
$end=true;
                                             }
                                        }
                                    }

                                @endphp
                                <li class="cards_board_item_material {{!is_null($currentData)?($currentData->id==$coursMeta->id?'active':''):''}}">
                                    <div style="text-decoration: none;" class="lessons_list_icon"><i
                                            class="notranslate icn {{$icon[$coursMeta->type]}} "
                                            aria-hidden="true" role="presentation"></i>
                                    </div>
                                    <div style="text-decoration: none;" class="lessons_list_info">
                                        <div class="lessons_list_title">{{$coursMeta->name}}</div>
                                    </div>
                                </li>


                            @endforeach
                        </ul>

                    </div>
                </div>
                <div class="manager_main_curses addMenuData">

                    @if(!is_null($currentData))
                        @php
                            $children=\App\CourseMetaData::where("CourseMeta_id",!is_null($currentData)?$currentData->id:0);
                            $navInclude=$currentData->type;
                        @endphp
                        <input type="hidden" name="type[id]" value="{{$currentData->id}}">
                        <input type="hidden" name="type[name]" value="course_metas">
                        <input type="hidden" name="importance[name]" value="{{$currentData->name}}">
                        <input type="hidden" name="icon_type" value="{{$navInclude}}">



                        @include('views.courses.start.'.$navInclude)
                    @endif
                    <button type="submit" href="javascript:void(0)"
                            id="next_btn" data-url="" class="btn btn-add"
                            style="background-color: #5298e8;margin-top: 4rem;">
                        @if($end)
                            Завершить курс
                        @else
                            перейти к следующему уроку
                        @endif
                    </button>

                </div>
            </div>
        </div>

        <style>
            .manager_main_curses {
                width: 50%;
                flex-grow: 2;
            }
        </style>

@endsection
