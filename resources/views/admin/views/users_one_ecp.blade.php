@extends('views.layouts.app')

@section('content')



    <div class="manager">
        <div class="manager_main">
            <div class="manager_main_head"
                 style=" background-color: #ebebeb; border-bottom: 1px solid #ebebeb; height: auto; min-height: auto; padding: 1rem 2rem; ">
                <h1 class="interface_view_title with_toggle" style=" font-size: 1rem; ">
                    <span class="header_title_toggle"></span>
                    Информация о пользователе
                </h1>

            </div>
            <div class="manager_main_curses">
                <div class="interface_view">
                    <div class="row">
                        <div class="col">
                            <div class="ofl_table">
                                <form method="post" action="{{url_custom('/admin/ecp/post')}}"
                                      enctype="multipart/form-data" class="files">
                                    <div class="language_grup">
                                        <div class="box_lsd" style="display: flex;flex-wrap: wrap;">
                                            <div class="language_grup_hid language_grup_multi language_grup_ru active ">
                                                <label class="label_control" for="v5TD66sa"><b class="text_bigs">
                                                        Файл для подписания</b> </label>
                                                <div class="form-group form-link filbox" style="">
                                                    <div class="boxsinvs"
                                                         style=" position: relative; right: 0; top: 0; transform: none; ">
                                                        <input type="file" name="file" required
                                                               class="form-control filecontrols" id="v5TD66sa"
                                                               placeholder="">
                                                        <label for="v5TD66sa" class="boxLImaege">
                                                            <svg xmlns="http://www.w3.org/2000/svg" width="24"
                                                                 height="24"
                                                                 viewBox="0 0 24 24" fill="none" stroke="currentColor"
                                                                 stroke-width="2" stroke-linecap="round"
                                                                 stroke-linejoin="round"
                                                                 class="feather feather-file-text">
                                                                <path
                                                                    d="M14 2H6a2 2 0 0 0-2 2v16a2 2 0 0 0 2 2h12a2 2 0 0 0 2-2V8z"></path>
                                                                <polyline points="14 2 14 8 20 8"></polyline>
                                                                <line x1="16" y1="13" x2="8" y2="13"></line>
                                                                <line x1="16" y1="17" x2="8" y2="17"></line>
                                                                <polyline points="10 9 9 9 8 9"></polyline>
                                                            </svg>
                                                        </label>
                                                    </div>
                                                    <div class="input-line"></div>
                                                </div>
                                            </div>
                                            @csrf
                                            <div class="language_grup_hid language_grup_multi language_grup_ru active "
                                                 style="margin-left: 50px;">
                                                <label class="label_control" for="v5TD66sa"><b class="text_bigs">
                                                        ЭЦП Ключ (RSA256)</b> </label>
                                                <div class="form-group form-link filbox" style="">
                                                    <div class="boxsinvs"
                                                         style=" position: relative; right: 0; top: 0; transform: none; ">
                                                        <input type="file" name="ecp" required
                                                               class="form-control filecontrols" id="v5TD66sa21"
                                                               placeholder="">
                                                        <label for="v5TD66sa21" class="boxLImaege">
                                                            <svg xmlns="http://www.w3.org/2000/svg" width="24"
                                                                 height="24"
                                                                 viewBox="0 0 24 24" fill="none" stroke="currentColor"
                                                                 stroke-width="2" stroke-linecap="round"
                                                                 stroke-linejoin="round"
                                                                 class="feather feather-file-text">
                                                                <path
                                                                    d="M14 2H6a2 2 0 0 0-2 2v16a2 2 0 0 0 2 2h12a2 2 0 0 0 2-2V8z"></path>
                                                                <polyline points="14 2 14 8 20 8"></polyline>
                                                                <line x1="16" y1="13" x2="8" y2="13"></line>
                                                                <line x1="16" y1="17" x2="8" y2="17"></line>
                                                                <polyline points="10 9 9 9 8 9"></polyline>
                                                            </svg>
                                                        </label>
                                                    </div>
                                                    <div class="input-line"></div>
                                                </div>
                                            </div>
                                        </div>
                                        {{maskInput($errors,["name"=>"pass","type"=>"text","placeholder"=>"Пароль ключа","required"=>true])}}
                                    </div>
                                    <button type="submit" class="btn btn-success"
                                            style="width: 200px;margin-bottom: 20px;">Подписать
                                    </button>
                                </form>
                                <table class="table td_middle td_highlight">
                                    <thead>
                                    <tr>
                                        <th class=" tcol_metrics" style="text-align: left;">Оригинальный Файл</th>
                                        <th class=" tcol_metrics" style="text-align: left;">Подписанный файл ЭЦП</th>
                                        <th class=" tcol_metrics" style="text-align: left;">Информация с ЭЦП</th>
                                        <th class=" tcol_metrics" style="text-align: left;">Подписан</th>
                                    </tr>
                                    </thead>
                                    <tbody>


                                    @foreach(\App\EdsItem::where("user_id",\Illuminate\Support\Facades\Auth::id())->orderby("id","desc")->get() as $ussers)

                                        <tr class="">
                                            <td>
                                                <a download="" href="{{$ussers->file}}"> {{$ussers->file}}</a>
                                            </td>
                                            <td>
                                                <a download="" href="{{$ussers->file_out}}"> {{$ussers->file_out}}</a>
                                            </td>
                                            <td>
                                                @php
                                                    if(empty($ussers->user_name)){
                                                        echo 'Нет данных';
                                                    }else{
                                                     $info=   json_decode($ussers->user_name,JSON_UNESCAPED_UNICODE);
                                                     if(is_array($info)){
                                                        echo $info["commonName"]."  ".$info["lastName"]." <br> ".$info["birthDate"]." Рождения <br> ".$info["state"];
                                                     }else {
                                                         echo 'Нет данных';
                                                     }
                                                    }
                                                @endphp
                                            </td>
                                            <td>
                                                @if(isset($ussers->created_at))
                                                    @php
                                                        $time_now = strtotime(date('Y-m-d H:i'));
                                                         $time_need = strtotime($ussers->created_at);

                                                    @endphp
                                                    {{Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $ussers->created_at)->format('d-m-Y H:i:s')}} <br> ({{ceil(($time_now-$time_need)/60)}}) минут назад
                                                @endif
                                            </td>
                                        </tr>
                                    @endforeach

                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
