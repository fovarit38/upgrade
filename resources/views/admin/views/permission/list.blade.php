@extends('views.layouts.app')

@section('content')


    <div class="manager">
        <div class="manager_main">
            <div class="manager_main_head">
                <h1 class="interface_view_title with_toggle">
                    <span class="header_title_toggle"></span>
                    Пользователи
                </h1>
                <form method="get" class="interface_view_header_filters"
                      style="margin-left: auto;margin-right: 2rem;position: relative;">
                    <input type="search" name="search" class="text sear_c input_text"
                           value="{{isset($_REQUEST["search"])?$_REQUEST["search"]:''}}" placeholder="Поиск ">
                </form>
                <div class="form_bl">
                    <a href="{{url_custom('/admin/users/0/edit')}}" title="Добавить пользователя" type="button"
                       class="button button_icon_only" data-tooltipped=""
                       aria-describedby="tippy-tooltip-1"
                       data-original-title="Управление студентами"><i
                            class="notranslate icn icn-course_add " aria-hidden="true"
                            role="presentation"></i></a>
                </div>
            </div>

            <div class="manager_main_curses" style="padding: 0 2rem;">



                <style>
                    .select-sa {
                        width: 30%;
                        flex-grow: 2;
                    }
                </style>


                <table
                    class="table sortTables  table-bordered table-striped js-table dataTable order-table no-footer"
                    id="DataTables_Table_0" style="width: 100%;" role="grid"
                    aria-describedby="DataTables_Table_0_info">
                    <thead>
                    <tr>

                        <th role="row">Имя</th>
                        <th role="row">Email</th>
                        <th role="row">Телефон</th>
                        <th role="row">Тип учётной записи</th>
                        <th role="row">Должность</th>
                        <th role="row">Руководитель</th>
                        <th role="row">Подразделение</th>
                        <th role="row"></th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($users as $user)
                        @php
                            $idgrups=  \App\Permission_user::checkId($user->id);

                            $nameTYpe="";
                            $nameGrups= \App\Permission_grup::find($idgrups);
                            if(!is_null($nameGrups)){
                                $nameTYpe=$nameGrups->name;
                            }
                           $dolsnost= App\Official::where("id",$user->jobtitle_id)->first();
                           $rukosado= App\User::where("id",$user->rukovod_id)->first();

                           $compani_list= array_keys(App\CompaniUser::where("user_id",$user->id)->get()->groupby("compani_id")->toarray());

                           $componiname= \App\Compani::wherein("id",$compani_list)->get();

                        @endphp
                        <tr role="row" class="even  ui-sortable-handle">

                            <td class=" sorting_1">
                                <div class="img-prof_box">
                                    <div href="<?=$user->images?>" data-fancybox="gallery" class="img-prof">
                                        <img src="<?=$user->images?>" alt="">
                                    </div>
                                    <?=$user->name?></a>
                                </div>
                            </td>
                            <td>
                                <?=$user->email?>
                            </td>
                            <td>
                                <?=$user->tel?>
                            </td>
                            <td>
                                {{$nameTYpe}}
                            </td>
                            <td>
                                {{!is_null($dolsnost)?$dolsnost->name:""}}
                            </td>
                            <td>{{!is_null($rukosado)?$rukosado->name:''}}</td>
                            <td>
                                @foreach($componiname as $coms)
                                    <p>
                                        {{$coms->name}}
                                    </p>

                                @endforeach
                            </td>

                            <td>
                                <div class="options-edit">
                                    @if($user->ban=="1")
                                        <a href="{{url_custom('/admin/user/ban/'.$user->id.'/0')}}" style="color:green;margin-right: 1rem;">Разблокировать</a>
                                    @else

                                        <a href="{{url_custom('/admin/user/ban/'.$user->id.'/1')}}" style="color:red;margin-right: 1rem;">Заблокировать</a>
                                    @endif
                                    <a href="{{url_custom('/admin/users/'.$user->id)}}"
                                       style="text-decoration: none;">
                                        <span class="icn icn-view" style="font-size:32px;"></span>
                                    </a>
                                    <a href="{{url_custom('/admin/users/'.$user->id.'/edit')}}"
                                       style="text-decoration: none;">
                                            <span class="icn icn-edit"
                                                  style="font-size: 22px;position: relative;margin-left: 1rem;"></span>
                                    </a>
                                </div>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>

            </div>
        </div>
    </div>

@endsection
