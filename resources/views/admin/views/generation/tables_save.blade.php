@extends('views.layouts.app')

@section('content')

    <div style=" padding: 2rem;padding-top:0;display: flex;flex-direction: column ">
        <div
            class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-4 border-bottom">
            <h1 class="h2" style="font-size: 2rem; font-weight: 600;">{{GMN($model_name)}}
                : {{$id==0?"Добавление записи":"Обновление записи"}}</h1>
            <div class="btn-toolbar mb-2 mb-md-0">

                <div class="langbtn"
                     style="margin-right: 2rem;padding-right: 2rem;border-right: 1px solid #000;display: flex;align-items: center;">
                    <p>
                        Выберите язык:
                    </p>
                    <?php
                    foreach (\App\Language::get() as $keyxs => $langs) {
                    ?>
                    <a href="javascript:void(0)"
                       data-lang="<?= $langs->name_key ?>"
                       class="btn btn-sm lang_control  btn-outline-success <?= $keyxs == 0 ? 'active' : ''?> waves-effect"
                       style="margin-left: 1rem;"><?= $langs->name ?></a>
                    <?php
                    }
                    ?>
                </div>

                <div class="btn-group ">
                    <a href="javascript:void(0);" onclick="$('#form_submit').submit()" type="submit"
                       class="btn btn-sm  btn-success waves-effect  px-4">сохранить</a>
                </div>

            </div>
        </div>

        @if(View::exists('views.generation.input.'.$model_name.'._pre'))
            @include('views.generation.input.'.$model_name.'._pre')
        @endif

        <form id="form_submit" enctype="multipart/form-data" method="post" action="{{url_custom("/admin/update")}}"
              class="bodyMain">
            @csrf

            <input type="hidden" name="model_name" value="{{$model_name}}">
            <input type="hidden" name="id" value="{{$id}}">
            <input type="hidden" name="path" value="{{url_custom("/admin/model/".$model_name."/{id}")}}">
            @if(!is_null($model_db))
                @foreach($model_db->meta("table_save")->where("attachment",$model_name)->orderby("sort","desc")->get() as $modelSing)
                    @php
                        $typins=column_rename($modelSing->name_key);
                        if($model_name=="Compani"){
                            if($modelSing->name_key=="name"){
                                $typins="Название подразделения";
                            }
                        }

                        if($model_name=="Official"){
                            if($modelSing->name_key=="name"){
                                $typins="Название Должности";
                            }
                            if($modelSing->name_key=="content"){
                                $typins="Обязанности";
                            }
                             if($modelSing->name_key=="price"){
                                $typins="Заработная плата";
                            }
                        }

                       if($model_name=="Register"){
                            if($modelSing->name_key=="name"){
                                $typins="Действие";
                            }
                            if($modelSing->name_key=="name_key"){
                                $typins="Ключ действия";
                            }

                            if($modelSing->name_key=="index-inv"){
                                $typins="Затронуто";
                            }

                        }

                    @endphp
                    @if($modelSing->name_key=="time")
                        {{maskInput($errors,["name"=>$modelSing->name_key."_save","type"=>"time","attr"=>"select","placeholder"=>$typins,"lang"=>$modelSing->languages],$model)}}
                    @elseif($modelSing->name_key=="date")
                        {{maskInput($errors,["name"=>$modelSing->name_key."_save","type"=>"date","attr"=>"select","placeholder"=>$typins,"lang"=>$modelSing->languages],$model)}}
                    @elseif($modelSing->name_key=="images")
                        {{maskInput($errors,["name"=>$modelSing->name_key."_save","type"=>"file","attr"=>"select","placeholder"=>$typins,"lang"=>$modelSing->languages],$model)}}
                    @elseif($modelSing->name_key=="sites_name_key")
                        {{maskInput($errors,["name"=>$modelSing->name_key."_save","type"=>"text","attr"=>"select","placeholder"=>$typins,"lang"=>$modelSing->languages],$model)}}
                    @else
                        @if($model_name=="Product" && $modelSing->name_key=="title")
                            {{maskInput($errors,["name"=>$modelSing->name_key."_save","type"=>"textarea","placeholder"=>$typins,"lang"=>$modelSing->languages],$model)}}
                        @else
                            {{maskInput($errors,["name"=>$modelSing->name_key."_save","type"=>"text","placeholder"=>$typins,"lang"=>$modelSing->languages],$model)}}

                        @endif
                    @endif
                @endforeach
            @endif

            @if(isset($_REQUEST["type"]))
                <input type="hidden" name="type_save" value="{{$_REQUEST["type"]}}">
            @endif

            @if(View::exists('views.generation.input.'.$model_name.'._next'))
                @include('views.generation.input.'.$model_name.'._next')
            @endif

        </form>
    </div>

@endsection
