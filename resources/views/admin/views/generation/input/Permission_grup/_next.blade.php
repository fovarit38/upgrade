<?php

$typings = [
    'course' => ['Курсы', ['s', 'u', 'i', 'd']],
    'library' => ['Библиотека', ['s', 'u', 'i', 'd']],
    'Gallery' => ['Новости', ['s', 'u', 'i', 'd']],
    'users' => ['Пользователи', ['s', 'u', 'i', 'd']],
    'CourseType' => ['Типы курса', ['s', 'u', 'i', 'd']],
    'MeterialType' => ['Типы материалов', ['s', 'u', 'i', 'd']],
    'Official' => ['Должности', ['s', 'u', 'i', 'd']],
    'StaticText' => ['Текста сайта', ['s', 'u', 'i', 'd']],
    'compani' => ['Подразделения', ['s', 'u', 'i', 'd']],
    'Permission_grup' => ['Права доступа', ['s', 'u', 'i', 'd']],
    'Register' => ['Регистр действий', ['s']],
    'courses_check' => ['Проверка заданий', ['s']],
];

?>
<style>
    .inputchcon {
        display: flex;
        align-items: center;
    }

    .inputchcon + .inputchcon {
        margin-top: 0.5rem;
    }

    .typing_b {
        display: flex;
        flex-wrap: wrap

    }


    .selection {
        width: 250px;
        padding: 1rem;
    }

    .t-bs {
        margin-bottom: 0.75rem;
    }
</style>
<h2 style="margin-top: 2rem;margin-bottom: 1rem;font-size: 1.5rem;font-weight: 600;">
    Доступы
</h2>

<div class="typing_b">
    @foreach($typings as $keyxs=>$typs)
        <div class="selection">
            <p class="t-bs">
                {{$typs[0]}}
            </p>
            @if(in_array('s',$typs[1]))
            <div class="inputchcon">
                <input type="checkbox" id="select{{$keyxs}}"  @if(\App\Permission_date::check_sing($id,$keyxs,"select","")) checked @endif name="permission[select][]"
                       value="{{$keyxs}}"
                       class="chbox"
                       style="display:none"/>
                <label for="select{{$keyxs}}" class="toggle"><span></span>
                </label>
                Читать
            </div>
            @endif
            @if(in_array('u',$typs[1]))
            <div class="inputchcon">
                <input type="checkbox" id="update{{$keyxs}}"  @if(\App\Permission_date::check_sing($id,$keyxs,"update","")) checked @endif name="permission[update][]"
                       value="{{$keyxs}}"
                       class="chbox"
                       style="display:none"/>
                <label for="update{{$keyxs}}" class="toggle"><span></span>
                </label>
                Изменять
            </div>
            @endif

            @if(in_array('i',$typs[1]))
            <div class="inputchcon">
                <input type="checkbox" id="insert{{$keyxs}}"  @if(\App\Permission_date::check_sing($id,$keyxs,"insert","")) checked @endif name="permission[insert][]"
                       value="{{$keyxs}}"
                       class="chbox"
                       style="display:none"/>
                <label for="insert{{$keyxs}}" class="toggle"><span></span>
                </label>
                Добавлять
            </div>
            @endif

            @if(in_array('d',$typs[1]))
            <div class="inputchcon">
                <input type="checkbox" id="delete{{$keyxs}}"  @if(\App\Permission_date::check_sing($id,$keyxs,"delete","")) checked @endif name="permission[delete][]"
                       value="{{$keyxs}}"
                       class="chbox"
                       style="display:none"/>
                <label for="delete{{$keyxs}}" class="toggle"><span></span>
                </label>
                Удалять
            </div>
            @endif
        </div>

    @endforeach
</div>
