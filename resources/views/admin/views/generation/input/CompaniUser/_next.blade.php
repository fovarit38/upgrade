@php
    $product_id  = isset($_REQUEST["compani_id"])? $_REQUEST["compani_id"]:'';
    $product_id  = is_null($model)? $product_id: $model->compani_id;

    $users_list=[];
    if($id==0){
            $users_list=  array_keys(\App\CompaniUser::get()->groupby("user_id")->toarray());
    }else{
    $users_list=  array_keys(\App\CompaniUser::where("compani_id","!=",$product_id)->get()->groupby("user_id")->toarray());
}
@endphp

<div class="btn-group" style="margin-right: 1rem;">
    <div class="row-fluid">
        <select class="selectpicker" name="user_id_save" data-show-subtext="true"
                data-live-search="true" required>
            <option value="">Выберите пользователя</option>
            @foreach(\App\User::whereNotIn("id",$users_list)->get() as $product)
                <option value="{{$product->id}}"
                        {{ !is_null($model)?($model->user_id==$product->id?'selected':'' ): ''}} data-subtext="">{{strip_tags(LC($product->name))}}
                </option>
            @endforeach
        </select>
    </div>
</div>


<input type="hidden" name="compani_id_save" value="{{$product_id}}">

<input type="hidden" name="path" value="{{url_custom("/admin/model/Compani/".$product_id)}}">
