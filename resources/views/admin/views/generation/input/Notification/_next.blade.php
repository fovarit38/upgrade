@php

    $product_id  = isset($_REQUEST["id_1c"])? $_REQUEST["id_1c"]:'';


    $product_id  = is_null($model)? $product_id: $model->id_1c;

    $_1c=\App\_1c_User::where("id_1c",$product_id)->first();


    $user_email = \App\User::where("email", $_1c->email)->get();
    $user_tel = [];
    if (validate_phone_number($_1c->personal_phone)) {
        $user_tel = \App\User::where("tel", validate_phone_number($_1c->personal_phone))->get();
    }

    $emailList=[];
    $telList=[];
    if($_1c->email!=""){
        array_push($emailList,$_1c->email);
    }

    foreach($user_email as $eme){
     if($eme->email!=""){
        array_push($emailList,$eme->email);
     }
       if($eme->tel!=""){
        array_push($telList,validate_phone_number($eme->tel));
     }
    }

     foreach($user_tel as $emes){
         if($emes->email!=""){
            array_push($emailList,$emes->email);
         }
         if($emes->tel!=""){
            array_push($telList,validate_phone_number($emes->tel));
         }
    }
  $emailList=  array_unique($emailList);
  $telList=  array_unique($telList);

@endphp

<div class="btn_list" style="margin-bottom: 2rem;display: flex;flex-direction: column;">
    ссылка для прямой авторизации
    <div
        style="background-color: #ccc;padding: 0.5rem;">{{'https://'.$_SERVER['SERVER_NAME'].url_custom('/authorization/'.$_1c->_token)}}</div>
</div>

{{maskInput($errors,["name"=>"message_save","type"=>"textarea","placeholder"=>"Сообщение","lang"=>0],$model)}}

<div class="typing_b">

    <div class="selection" style="margin-top: 1rem;">
        <p class="t-bs">
            Отправить уведомление в
        </p>

        @if(count($telList)>0)
            <div class="inputchcon">
                <input type="checkbox" id="message_sms" checked name="message[sms]"
                       value="+7{{$telList[0]}}"
                       class="chbox"
                       style="display:none"/>
                <label for="message_sms" class="toggle"><span></span>
                </label>
                SMS
            </div>
        @endif
        @if(count($emailList)>0)
            <div class="inputchcon">
                <input type="checkbox" id="message_email" checked name="message[email]"
                       value="{{$emailList[0]}}"
                       class="chbox"
                       style="display:none"/>
                <label for="message_sms" class="toggle"><span></span>
                </label>
                Email на
            </div>
        @else
            <p>
                Нет доступного email
            </p>
        @endif

        @if(isset($_1c["id_1c"]))
            <div class="inputchcon">
                <input type="checkbox" id="message_1с" checked name="message[bitrix]"
                       value="{{$_1c["id_1c"]}}"
                       class="chbox"
                       style="display:none"/>
                <label for="message_1с" class="toggle"><span></span>
                </label>
                Битрикс
            </div>
        @endif

    </div>
</div>

<input type="hidden" name="id_1c_save" value="{{$product_id}}">
<input type="hidden" name="path" value="{{url_custom("/admin/model/_1c_User/".$_1c->id)}}">


<style>
    p + p {
        margin-top: 1rem;
    }
</style>


<style>
    .inputchcon {
        display: flex;
        align-items: center;
    }

    .toggle {
        margin-right: 1rem;
    }

    .inputchcon + .inputchcon {
        margin-top: 0.5rem;
    }

    .typing_b {
        display: flex;
        flex-wrap: wrap

    }


    .selection {
        width: 250px;

    }

    .t-bs {
        margin-bottom: 0.75rem;
    }
</style>
