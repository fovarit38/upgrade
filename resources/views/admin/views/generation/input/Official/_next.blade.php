<div class="btn-group" style="margin-right: 1rem;flex-wrap: wrap;">
    <p style="width: 100%;">
        Вакансия
    </p>
    <div class="row-fluid">
        <select class="selectpicker" name="set_kit_save" data-show-subtext="true"
                data-live-search="true" required>
            <option value="0" {{ !is_null($model)?($model->set_kit=="0"?'selected':'' ): ''}}> не активна</option>
            <option value="1" {{ !is_null($model)?($model->set_kit=="1"?'selected':'' ): ''}}> активна</option>

        </select>
    </div>
</div>
