@php
    $model=$model->toarray();
$id_1c=$model["id_1c"];
unset($model["_token"]);
unset($model["id"]);
unset($model["id_1c"]);

@endphp
@foreach($model as $keyxs=>$kke)
    @if($keyxs=="active")
        @if($kke=="1")
            <p>
                <b> Статус</b> : Работает <br>
            </p>
        @else
            <p>
                <b> Статус</b> : Уволен <br>
            </p>
        @endif
    @else
        @if($kke!="")
            <p>
                <b> {{$keyxs}}</b> : {{$kke}}
            </p>
        @endif
    @endif

@endforeach



@php
    $model_old=$model;

    $model_name="Notification";

    $model = app("\App\\" . $model_name);
    $sql_name = $model->getTable();
    $colum_list = Schema::getColumnListing($sql_name);

    $tbody = $model;
    if (in_array('sort', $colum_list) == true) {
        $tbody = $tbody->orderby("sort");
    }
    $tbody = $tbody->get();
    $thead = [];
    if(!is_null($model_old)){
      $tbody= $tbody->where("id_1c",$id_1c) ;
    }



    $meta_colum = \App\Model_meta::where("type", "table_catalog")->where("attachment", $model_name)->get();

    foreach ($meta_colum as $rows) {
        $colums = \App\Column_name::where("name_key", $rows->name_key)->first();
        $thead[$rows->name_key] = isset($colums->name) ? $colums->name : $rows->name_key;
    }
    $table_link = [];

@endphp

@if(!is_null($model_old))
    <div class="bodyMain">
        <div class="header">
            <div
                class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-2 border-bottom">
                <h1 class="h2">Уведомления</h1>
                <div class="btn-toolbar mb-2 mb-md-0">
                    <div class="btn-group">
                        <a href="{{url_custom("/admin/model/".$model_name."/0".(!is_null($model_old)?'?id_1c='.$id_1c:''))}}"
                           class="btn btn-sm btn-outline-secondary waves-effect  ">Отправить уведомление</a>
                    </div>
                </div>
            </div>
        </div>

        @include('constituent_element.table.main')

    </div>
@endif



<style>
    p + p {
        margin-top: 1rem;
    }
</style>
