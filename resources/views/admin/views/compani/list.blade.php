@extends('views.layouts.app')

@section('content')


    <div class="manager">
        <div class="manager_main">
            <div class="manager_main_head">
                <h1 class="interface_view_title with_toggle">
                    <span class="header_title_toggle"></span>
                    Подразделения
                </h1>
                <form method="get" class="interface_view_header_filters"
                      style="margin-left: auto;margin-right: 2rem;position: relative;">
                    <input type="search" name="search" class="text input_text"
                           value="{{isset($_REQUEST["search"])?$_REQUEST["search"]:''}}" placeholder="Поиск ">
                </form>
                <div class="form_bl">
                    <a href="{{url_custom('/admin/model/Compani/0')}}" title="Добавить подразделение" type="button"
                       class="button button_icon_only" data-tooltipped=""
                       aria-describedby="tippy-tooltip-1"><i
                            class="notranslate icn icn-course_add " aria-hidden="true"
                            role="presentation"></i></a>
                </div>
            </div>
            <div class="manager_main_curses">
                    @php
                        if(isset($_REQUEST["search"])){
                          $table_link=\App\Compani::where("name","LIKE",'%'.$_REQUEST["search"].'%')->get();
                         }
                    @endphp
                    @if(count($table_link)>0)
                            <div style=" width: 100%; ">
                                <div class="col" style="padding-bottom: 6rem;">
                                    <div class="ofl_table">
                                        <table class="table td_middle td_highlight">
                                            <thead>
                                            <tr>
                                                <th class=" tcol_user" style=" width: 100%; text-align: left; ">Название подразделения</th>
                                                <th class=" tcol_menu"></th>
                                            </tr>
                                            </thead>
                                            <tbody>

                                            @foreach($table_link as $user)

                                                <tr class="">

                                                    <td class=" tcol_metrics" style=" width: 100%; text-align: left; ">
                                                        <div class="tcol_metrics_value"><span
                                                                class="clr_gray">{{$user->name}}</span>
                                                        </div>
                                                    </td>
                                                    <td style="    width: 10px;" class=" tcol_menu">
                                                        <a style="text-decoration: none"
                                                           href="{{url_custom('/admin/model/Compani/'.$user->id)}}"
                                                           class="flex flex_row"><i style="font-size: 2rem;"
                                                                class="notranslate icn icn-edit "
                                                                aria-hidden="true"
                                                                role="presentation"></i></a>
                                                    </td>
                                                </tr>
                                            @endforeach
                                            </tbody>
                                        </table>
                                </div>
                            </div>
                    @else
                        <p style="padding:1rem;display: flex;align-items: center;justify-content: center;color: red;border-radius: 4px;border: 1px solid #e6e6e6;">
                            Компания не найдена
                        </p>
                    @endif
                </div>
            </div>
        </div>
    </div>

@endsection
