@extends('views.layouts.app')

@section('content')

    @php
        $user_list=  (\App\CourseUser::where("user_id",$user->id)->get());
    @endphp

    <div class="manager">
        <div class="manager_main">
            <div class="manager_main_head"
                 style=" background-color: #ebebeb; border-bottom: 1px solid #ebebeb; height: auto; min-height: auto; padding: 1rem 2rem; ">
                <h1 class="interface_view_title with_toggle" style=" font-size: 1rem; ">
                    <span class="header_title_toggle"></span>
                    Информация о пользователе
                </h1>

            </div>
            <div class="manager_main_curses">
                <div class="interface_view">
                    <div class="interface_profile_view">
                        <div class="interface_profile_view_user">
                            <div class="userinfo_block">
                                <div class="userinfo_avatar "><img
                                        src="{{$user->images}}"
                                        role="presentation">
                                    <div class="userinfo_avatar_status offline"></div>
                                </div>
                                <div class="userinfo_details">
                                    <div class="userinfo_role">
                                        @php
                                            $idgrups=  \App\Permission_user::checkId($user->id);

                                            $nameTYpe="";
                                            $nameGrups= \App\Permission_grup::find($idgrups);
                                            if(!is_null($nameGrups)){
                                                $nameTYpe=$nameGrups->name;
                                            }
                                        @endphp
                                        {{$nameTYpe}}
                                    </div>
                                    <div class="userinfo_name">{{$user->name}}</div>
                                    <div class="userinfo_desc"><span class="clr_gray"><a
                                                href="mailto:{{$user->email}}">{{$user->email}}</a></span>
                                    </div>
                                    {{--                                    <div class="userinfo_desc" style=" padding-top: 0.75rem; ">Группы: Демостенд</div>--}}
                                    <a href="{{url_custom('/admin/users/'.$user->id.'/edit')}}"
                                       style="display: flex;justify-content: center;"
                                       class="form_bl">
                                        <span class="dottedlink smalltext">Редактировать профиль</span>
                                    </a>
                                </div>
                                <div class="userinfo_systat"><span
                                        class="notificator notificator_warning"
                                        style=" background-color: green !important; color: #fff !important; ">в сети</span>
                                </div>
                            </div>
                        </div>
                        <div class="interface_profile_view_info">
                            <div class="interface_view" style="padding: 2rem;box-sizing: border-box">
                                <div class="bigdesc_bar">
                                    <div class="radialprogress">
                                        <div class="radialprogress_item">
                                            <div class="radialprogress_item_value">
                                                <div class="radialprogress_item_value_text clr_primary">0%</div>
                                                <div class="svgchart_circle clr_primary">
                                                    <svg class="svgchart_circle_progress" viewBox="0 0 120 120">
                                                        <circle class="svgchart_circle_progress_meter" cx="60" cy="60"
                                                                r="50"></circle>
                                                        <circle class="svgchart_circle_progress_value" cx="60" cy="60"
                                                                r="50"
                                                                style="stroke-dasharray: 314.159; stroke-dashoffset: 314.159;"></circle>
                                                    </svg>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="bigdesc ">
                                        <div class="bigdesc_title ">Обучение</div>
                                        <div class="bigdesc_item " data-tooltipped="" aria-describedby="tippy-tooltip-2"
                                             data-original-title="0%">
                                            <div class="bigdesc_item_value" style="    margin-left: 0;">0<span
                                                    class="bigdesc_item_value_sub">/ {{count($user_list)}}</span></div>
                                            <div class="bigdesc_item_title" style="    margin-left: 0;">Курсов</div>
                                        </div>

                                    </div>
                                    <div class="bigdesc ">
                                        <div class="bigdesc_title ">Результаты</div>
                                        <div class="bigdesc_item ">
                                            <div class="bigdesc_item_icon"><i class="notranslate icn icn-coin "
                                                                              aria-hidden="true"
                                                                              role="presentation"></i></div>
                                            <div class="bigdesc_item_value">0.0</div>
                                            <div class="bigdesc_item_title">Баллов</div>
                                        </div>
                                        <div class="bigdesc_item ">
                                            <div class="bigdesc_item_icon"><i class="notranslate icn icn-profits "
                                                                              aria-hidden="true"
                                                                              role="presentation"></i></div>
                                            <div class="bigdesc_item_value">0%</div>
                                            <div class="bigdesc_item_title">Эффективность</div>
                                        </div>
                                        <div class="bigdesc_item ">
                                            <div class="bigdesc_item_icon"><img
                                                    src="/public/media/client/images/certificate_gold.17df3ed6.svg"
                                                    role="presentation"></div>
                                            <div class="bigdesc_item_value">0</div>
                                            <div class="bigdesc_item_title">Медалей</div>
                                        </div>
                                        <div class="bigdesc_item ">
                                            <div class="bigdesc_item_value">0</div>
                                            <div class="bigdesc_item_title">Пересдач</div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col">
                                        <div class="ofl_table">
                                            <table class="table td_middle td_highlight">
                                                <thead>
                                                <tr>
                                                    <th class=" tcol_status"></th>
                                                    <th class=" tcol_info">Название курса</th>
                                                    <th class=" tcol_metrics">Баллов</th>
                                                    <th class=" tcol_metrics">Эффективность</th>
                                                    <th class=" tcol_metrics">Медалей</th>
                                                    <th class=" tcol_metrics">Пересдач</th>
                                                    <th class=" tcol_metrics">Назначено</th>
                                                    <th class=" tcol_metrics">Начало</th>
                                                    <th class=" tcol_metrics">Завершение</th>
                                                </tr>
                                                </thead>
                                                <tbody>


                                                @foreach($user_list as $us)
                                                    @php
                                                        $curse=\App\Course::find($us->course_id);
                                                    @endphp
                                                    <tr class="">
                                                        <td class=" tcol_status">
                                                            <div class="round_status">
                                                                <div class="round_status_icon " data-tooltipped=""
                                                                     aria-describedby="tippy-tooltip-3"
                                                                     data-original-title="Курс еще не начат"><i
                                                                        class="notranslate icn icn-hourglass "
                                                                        aria-hidden="true" role="presentation"></i>
                                                                </div>
                                                            </div>
                                                        </td>
                                                        <td class=" tcol_info">
                                                            @if(isset($curse->id))
                                                                <a style="text-decoration: none;"
                                                                   href="{{url_custom('/admin/courses/'.$curse->id)}}"
                                                                   class="item_title">{{$curse->name}}</a>
                                                            @endif
                                                        </td>
                                                        <td class=" tcol_metrics">
                                                            <div class="tcol_metrics_value"><span
                                                                    class="clr_gray">—</span>
                                                            </div>
                                                        </td>
                                                        <td class=" tcol_metrics">
                                                            <div class="tcol_metrics_value"><span
                                                                    class="clr_gray">—</span>
                                                            </div>
                                                        </td>
                                                        <td class=" tcol_metrics">
                                                            <div class="tcol_metrics_value"><span
                                                                    class="clr_gray">—</span>
                                                            </div>
                                                        </td>
                                                        <td class=" tcol_metrics">
                                                            <div class="tcol_metrics_value"><span
                                                                    class="clr_gray">—</span>
                                                            </div>
                                                        </td>
                                                        <td class=" tcol_metrics">
                                                            @if(isset($curse->created_at))
                                                                <div class="tcol_metrics_value">
                                                                    <span>{{Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $curse->created_at)->format('d-m-Y')}}</span>
                                                                </div>
                                                            @endif
                                                        </td>
                                                        <td class=" tcol_metrics">
                                                            <div class="tcol_metrics_value"><span
                                                                    class="clr_gray">—</span>
                                                            </div>
                                                        </td>
                                                        <td class=" tcol_metrics">
                                                            <div class="tcol_metrics_value"><span
                                                                    class="clr_gray">—</span>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                @endforeach
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
