@extends('views.layouts.app')

@section('content')

@php
$typeControl=null;
if(isset($_REQUEST["coursetype"])){
$typeControl=  ($_REQUEST["coursetype"]);
}
$type_info='library';
if(isset($_REQUEST["type"])){
$type_info=  ($_REQUEST["type"]);
}

@endphp
<div class="manager">
    <div class="manager_main">
        <div class="manager_main_head">
            <h1 class="interface_view_title with_toggle">
                <span class="header_title_toggle"></span>
                <?php
                $returen_text = 'Все материалы';
                if (in_array('video', $nav)) {
                    $returen_text = 'Видео инструкция о должности';
                }
                echo $returen_text;
                ?>
            </h1>
            {{--
            <div class="interface_view_header_filters" style="--}}
{{--    margin-lefT: auto;--}}
{{--    margin-right: 2rem;--}}
{{--">--}}
                {{-- <input type="search" class="text input_text" value="" placeholder="Поиск материалов">--}}
                {{--                </div>
            --}}

            @if (\App\Permission_date::check("courses_check", "select", 'Db', 0))
            <div class="form_bl">
                <a href="{{url_custom('/admin/courses/add/info/0?type='.$type_info)}}" title="Добавить материал"
                   type="button"
                   class="button button_icon_only" data-tooltipped=""
                   aria-describedby="tippy-tooltip-1"
                   data-original-title="Управление студентами"><i
                        class="notranslate icn icn-course_add " aria-hidden="true"
                        role="presentation"></i></a>
            </div>
            @endif
        </div>
        <div class="manager_main_curses">
            @php
            $caCurst=\App\CourseType::where("type",$type_info)->orderby("sort")->get();
            if(!is_null($typeControl)){
            $caCurst=$caCurst->where("id",$typeControl);
            }
            @endphp
            @foreach($caCurst as $keyxs=>$catalog)
            <div class="interface">
                <div class="interface_head">
                    <h2>
                        {{$keyxs + 1}}. {{$catalog->name}}
                    </h2>
                </div>
                <div class="interface_main">

                    @foreach(\App\Course::where("courseType_id",$catalog->id)->where("type",$type_info)->where("delete","0")->get()
                    as $cours)
                    <div class="curs">
                        <a href="{{url_custom('/admin/library/'.$cours->id)}}" class="a-clock"></a>
                        @if (\App\Permission_date::check("courses_check", "select", 'Db', 0))
                        <a href="{{url_custom('/admin/courses/add/info/'.$cours->id)}}"
                           class="setting_edit">
                            <i class="icn icn-edit"></i>
                        </a>
                        @endif
                        <div class="curs_img">
                            <div class="prop">
                                <div class="prop_img">
                                    <div class="prop_img_src"
                                         style="background-image: url('{{$cours->images}}');background-size: contain; background-position: center center; background-repeat: no-repeat;">

                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="curs_main">
                            <div class="title">
                                <div>
                                    {{$cours->name}}
                                </div>
                            </div>
                            <div class="content">
                                <p>
                                    {{$cours->mini}}
                                </p>
                            </div>

                        </div>

                    </div>
                    @endforeach

                </div>
            </div>
            @endforeach
        </div>
    </div>


    @endsection
