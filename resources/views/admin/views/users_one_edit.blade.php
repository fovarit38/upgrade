@extends('views.layouts.app')

@section('content')


    <div class="manager">
        <div class="manager_main">
            <div class="manager_main_head"
                 style=" background-color: #ebebeb; border-bottom: 1px solid #ebebeb; height: auto; min-height: auto; padding: 1rem 2rem; ">
                <h1 class="interface_view_title with_toggle" style=" font-size: 1rem; ">
                    <span class="header_title_toggle"></span>
                    Информация о пользователе
                </h1>

            </div>
            <form enctype="multipart/form-data" action="{{url_custom("/admin/update")}}" method="post"
                  class="manager_main_curses">
                @csrf
                <input type="hidden" name="model_name" value="User">
                <input type="hidden" name="id" value="{{isset($user->id)?$user->id:0}}">
                <input type="hidden" name="path" value="{{url_custom("/admin/users/{id}/edit")}}">

                <div class="interface_view">
                    <div class="interface_view_header">
                        <div class="interface_view_header_info"><h1 class="interface_view_title">Редактирование
                                профиля</h1></div>
                        <div class="interface_view_header_actions">
                            <button class="button" onclick="history.go(-1)">Вернуться</button>
                            <button class="button button_accent">Сохранить</button>
                        </div>
                    </div>
                    <div class="form">
                        <div class="flex">
                            <div class="form_col" style="flex-basis: 0px;">
                                <div class="form_fieldset">
                                    <legend class="form_legend">Фотография</legend>
                                    <div class="form_bl">
                                        <div class="filechoose filechoose_avatar">

                                            @if(isset($user->images)?$user->images:'')
                                                <img class="uploadpreview" src="{{$user->images}}"
                                                     role="presentation">
                                            @endif

                                            <input type="file" name="images_save" accept="image/jpeg, image/png">
                                            <div class="filechoose_desc">
                                                <div class="filechoose_desc_filename">

                                                </div>
                                                <div>Нажмите для замены изображения или перетащите ее в этот блок</div>
                                            </div>
                                            {{--                                            <a class="filechoose_clear" data-tooltipped=""--}}
                                            {{--                                               aria-describedby="tippy-tooltip-4"--}}
                                            {{--                                               data-original-title="Удалить изображение"></a>--}}
                                        </div>
                                    </div>
                                    <div class="infoblock infoblock_requirements"><p>Минимальный размер фотографии: <b>300x300
                                                пикселей.</b></p>
                                        <p>Максимальный размер фотографии: <b>1000x1000 пикселей.</b></p>
                                        <p>Максимальный размер файла: <b>500KB</b></p>
                                        <p>Поддерживаемые форматы файлов:: <b>png</b><b>, jpeg</b></p></div>
                                </div>
                            </div>
                            <div class="flex flex_grow_huge">
                                <div class="form_col">
                                    <div class="form_fieldset">
                                        <legend class="form_legend">Общая информация</legend>
                                        @php

                                            $fio_save=['','',''];
                                            $info_name=isset($user->name)?$user->name: '';
                                            $info_name=explode(" ",$info_name);

                                        foreach ($info_name as $inde=>$inso){
                                            $fio_save[$inde]=$inso;
                                        }

                                        @endphp
                                        <div class="form_bl"><label for="user_name">Фамилия</label><input type="text"
                                                                                                          class="text"
                                                                                                          name="name_date[]"
                                                                                                          value="{{$fio_save[0]}}"
                                                                                                          placeholder="Фамилия"
                                                                                                          pattern="[A-Za-zА-Яа-яЁё]*"
                                                                                                          autocomplete="off"
                                                                                                          >
                                        </div>

                                        <div class="form_bl"><label for="user_name">Имя</label><input type="text"
                                                                                                      class="text"
                                                                                                      name="name_date[]"
                                                                                                      value="{{$fio_save[1]}}"
                                                                                                      pattern="[A-Za-zА-Яа-яЁё]*"
                                                                                                      placeholder="Имя"
                                                                                                      autocomplete="off"
                                                                                                      ></div>

                                        <div class="form_bl"><label for="user_name">Отчество</label><input type="text"
                                                                                                           class="text"
                                                                                                           name="name_date[]"
                                                                                                           pattern="[A-Za-zА-Яа-яЁё]*"
                                                                                                           value="{{$fio_save[2]}}"
                                                                                                           placeholder="Отчество"
                                                                                                           autocomplete="off"
                                                                                                           >
                                        </div>

                                        <div class="form_bl"><label for="user_pol">Пол</label>
                                            <div class="select"><select id="user_pol" name="pol_save" required>
                                                    <option value="">Выберите пол</option>
                                                    <option
                                                        value="Муж" {{isset($user->pol)?($user->pol=="Муж"?'selected':''): ''}}>
                                                        Муж
                                                    </option>
                                                    <option
                                                        value="Жен" {{isset($user->pol)?($user->pol=="Жен"?'selected':''): ''}}>
                                                        Жен
                                                    </option>
                                                </select>
                                            </div>
                                        </div>

                                        <div class="form_bl">
                                            <label for="user_iin"> ИИН</label><input type="text" id="user_iin"
                                                                                     class="text"
                                                                                     name="iin_save"
                                                                                     autocomplete="off"
                                                                                     value="{{isset($user->iin)?$user->iin: ''}}"
                                                                                     placeholder="ИИН">
                                        </div>

                                        <div class="form_bl">
                                            <label for="user_iin"> Email</label><input type="text" id="user_iin"
                                                                                       class="text"
                                                                                       name="email_save"
                                                                                       autocomplete="off"
                                                                                       value="{{isset($user->email)?$user->email: ''}}"
                                                                                       placeholder="Email">
                                        </div>

                                        <div class="form_bl">
                                            <label for="user_adres"> Cтрана город улица дом</label><input type="text"
                                                                                                          id="user_adres"
                                                                                                          class="text"
                                                                                                          name="adres_save"
                                                                                                          autocomplete="off"
                                                                                                          value="{{isset($user->adres)?$user->adres: ''}}"
                                                                                                          placeholder="Cтрана город улица дом"
                                                                                                          >
                                        </div>
                                        <div class="form_bl">
                                            <label for="user_tel"> Телефон</label><input type="text" id="user_tel"
                                                                                         class="text mask-tel"
                                                                                         autocomplete="off"
                                                                                         name="tel_save"
                                                                                         @if((!\App\Permission_date::check("users", "select", 'Db', 0))) disabled
                                                                                         @endif
                                                                                         value="+7{{isset($user->tel)?$user->tel: ''}}"
                                                                                         placeholder="Телефон"
                                                                                         >
                                        </div>
                                        <div class="form_bl">
                                            <label for="user_dob"> Дата рождения</label><input type="date"
                                                                                               id="user_dob"
                                                                                               name="dob_save"
                                                                                               class="text"
                                                                                               autocomplete="off"
                                                                                               value="{{isset($user->dob)?$user->dob: ''}}"
                                                                                               placeholder="Телефон"
                                                                                               >
                                        </div>

                                        <div class="form_bl"><label for="user_role">Тип учётной записи</label>
                                            <div class="select">
                                                @php

                                                    $curentPermision=\App\Permission_user::checkId(Auth::user()->id);


                                                 $valus=-1;
                                                                        if(isset($user->id)){
                                                                                $valus=\App\Permission_user::checkId($user->id);
                                                                        }

                                                                        $controlsa=false;
                                                    if($valus==4||$valus==1 ||$valus==5){
                                                        if($curentPermision==4){

                                                        }else{
                                                            $controlsa=true;
                                                        }
                                                    }
                                                @endphp
                                                <select id="user_role" name="user_role"
                                                        @if((!\App\Permission_date::check("users", "select", 'Db', 0)) || $controlsa) disabled @endif>
                                                    <option value="">Выберите тип</option>
                                                    @foreach(\App\Permission_grup::get() as $userGrup)
                                                        @php




                                                            if($userGrup->id==4||$userGrup->id==1 ||$userGrup->id==5){

                                                                if($curentPermision=="3"){
                                                                    $valus=$userGrup->id;
                                                                }else{
                                                                      $valus="";
                                                                }

                                                            }else{
                                                              $valus=$userGrup->id;
                                                            }

                                                        @endphp
                                                        @if($valus!="")
                                                            <option
                                                                value="{{$valus}}" {{isset($user->id)?(\App\Permission_user::checkId($user->id)?(\App\Permission_user::checkId($user->id)==$userGrup->id?'selected':''):'') : ''}}>{{$userGrup->name}}</option>
                                                        @endif
                                                    @endforeach
                                                </select>
                                            </div>
                                            <div class="infoblock infoblock_info" style="display: none;"><p><b>Админ</b> - управление
                                                    назначением на курсы, пользователями </p>
                                                <p><b>Наставник</b> - проверка заданий </p>
                                                <p><b>Сотрудник</b> - прохождение курсов</p></div>
                                        </div>
                                    </div>
                                    <div class="form_fieldset">
                                        <legend class="form_legend">Пароль</legend>
                                        <div class="form_bl">
                                            <div class="form_desc">В случае необходимости установите новый пароль для
                                                пользователя
                                            </div>
                                            <input type="password" name="password_pass" id="user_password" class="text"
                                                   value="" placeholder="Пароль"
                                                   autocomplete="new-password" {{is_null($user)?'required':''}} >
                                            <br>
                                            <input type="password" class="text" value=""
                                                   {{is_null($user)?'required':''}} placeholder="Повторите пароль"
                                                   autocomplete="new-password"></div>
                                    </div>
                                </div>
                                <div class="form_col">
                                    <div class="form_fieldset">
                                        <legend class="form_legend">Дополнительная информация</legend>


                                        <div class="form_bl"><label for="user_work">История работы в
                                                компании</label><textarea
                                                id="user_work" name="user_work_save" class="text"
                                                placeholder="Добавьте историю"
                                                rows="10">{{isset($user->user_work)?$user->user_work: ''}}</textarea>
                                        </div>


                                        <div class="form_bl"><label for="user_setting">Должностная инструкция</label>
                                            <textarea id="user_setting" name="user_setting_save" class="text"
                                                      placeholder="Добавьте инструкция"
                                                      rows="10">{{isset($user->user_setting)?$user->user_setting: ''}}</textarea>
                                        </div>


                                        <div class="form_bl"><label for="user_pol">Руководитель</label>
                                            <div class="select"><select id="user_pol" class="selectpicker"
                                                                        data-show-subtext="true"
                                                                        @if((!\App\Permission_date::check("users", "select", 'Db', 0))) disabled
                                                                        @endif
                                                                        data-live-search="true" name="rukovod_id_save">
                                                    <option value="">Выберите руководителя</option>
                                                    @foreach(\App\User::where("id","!=",isset($user->id)?$user->id:0)->get() as $usr)
                                                        <option
                                                            value="{{$usr->id}}" {{isset($user->rukovod_id)?($user->rukovod_id==$usr->id?'selected':''): ''}}>{{$usr->name}}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>

                                        <div class="form_bl"><label for="jobtitle">Должность</label>
                                            <div class="select"><select id="jobtitle" class="selectpicker"
                                                                        data-show-subtext="true"
                                                                        @if((!\App\Permission_date::check("users", "select", 'Db', 0))) disabled
                                                                        @endif
                                                                        data-live-search="true" name="jobtitle_id_save">
                                                    <option value="">Выберите должность</option>
                                                    @foreach(\App\Official::get() as $official)
                                                        <option
                                                            value="{{$official->id}}" {{isset($user->jobtitle_id)?($user->jobtitle_id==$official->id?'selected':''): ''}}>
                                                            {{$official->name}}
                                                        </option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="" style="display: none;justify-content: flex-end;">
                        <div class="col flex_grow_0" style="display: flex;justify-content: flex-end;">
                            <button role="button" class="button button_danger">Удалить</button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>

@endsection
