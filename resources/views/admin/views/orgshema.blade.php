@extends('views.layouts.app')

@section('content')


    <div class="manager">
        <div class="manager_main">
            <div class="manager_main_head">
                <h1 class="interface_view_title with_toggle">
                    <span class="header_title_toggle"></span>
                    Оргсхема
                </h1>
            </div>

            <div class="manager_main_curses" style="padding: 0 2rem;">

                <div class="org">
                    <div class="org_main">

                        @php
                            $dolsh= \App\Official::wherein("id",[4])->get();


                        function getinfo($user){
                             $userPosition= \App\Official::find($user->jobtitle_id);
                             if(!is_null($userPosition)){
                                    ?>
                                    <div class="org_list">
                                                    <div class="org_list_head">
                                                        <div class="org_list_head_style">
                                                            <p>
                                                                Должность <br> <b> <?=$userPosition->name?></b> <br><br>
                                                                ФИО <br> <b> <?=$user->name?></b>
                                                            </p>
                                                            </div>
                                                    </div>
                                                    <div class="org_list_drop">
                                                        <?php
                                                       $userChList= \App\User::where("rukovod_id",$user->id)->get();
                                                        foreach ($userChList as $chlis){
                                                            getinfo($chlis);
                                                        }
                                                        ?>
                                                    </div>
                                    </div>
                                    <?
                            }
                        }

                        @endphp

                        @foreach($dolsh as $dol)
                            @php
                                $userBox= \App\User::where("jobtitle_id",$dol->id)->get();
                            @endphp
                            @foreach($userBox as $us_date)
                                {{getinfo($us_date)}}
                            @endforeach

                        @endforeach


                    </div>
                </div>

            </div>
        </div>
    </div>

@endsection
