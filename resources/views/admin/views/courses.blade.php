@extends('views.layouts.app')

@section('content')

    @php
        $typeControl=null;
            if(isset($_REQUEST["coursetype"])){
              $typeControl=  ($_REQUEST["coursetype"]);
            }
    @endphp
    <div class="manager">
<!--        <div class="manager_menu">-->
<!--            <a href="{{url_custom('/admin/courses')}}"-->
<!--               class="courses-menu courses-menu-mb  {{!isset($_REQUEST["coursetype"])?'courses-menu-bk':''}}  ">-->
<!--                <div class="largetext">Все курсы</div>-->
<!--                <div class="list_counter">{{\App\Course::where("delete","0")->where("type","courses")->count()}}</div>-->
<!--            </a>-->
<!--            @foreach(\App\CourseType::orderby("sort")->where("type","courses")->get() as $keyxs=>$catalog)-->
<!--                <a href="{{url_custom('/admin/courses?coursetype='.$catalog->id)}}"-->
<!--                   class="courses-menu {{isset($_REQUEST["coursetype"])?($_REQUEST["coursetype"]==$catalog->id?'courses-menu-bk':''):''}}">-->
<!--                    <div class="largetext">{{$keyxs+1}}. {{$catalog->name}}</div>-->
<!--                    <div-->
<!--                        class="list_counter">{{\App\Course::where("courseType_id",$catalog->id)->where("type","courses")->where("delete","0")->count()}}</div>-->
<!--                </a>-->
<!--            @endforeach-->
<!--        </div>-->
        <div class="manager_main">
            <div class="manager_main_head">
                <h1 class="interface_view_title with_toggle">
                    <span class="header_title_toggle"></span>
                    Стандарты работы
                </h1>
                {{--                <div class="interface_view_header_filters" style="--}}
                {{--    margin-lefT: auto;--}}
                {{--    margin-right: 2rem;--}}
                {{--">--}}
                {{--                    <input type="search" class="text input_text" value="" placeholder="Поиск курсов">--}}
                {{--                </div>--}}

                @if (\App\Permission_date::check("course", "insert", 'Db', 0))
                    <div class="form_bl">
                        <a href="{{url_custom('/admin/courses/add/info/0')}}" title="Добавить курс" type="button"
                           class="button button_icon_only" data-tooltipped=""
                           aria-describedby="tippy-tooltip-1"
                           data-original-title="Управление студентами"><i
                                class="notranslate icn icn-course_add " aria-hidden="true"
                                role="presentation"></i></a>
                    </div>
                @endif
            </div>
            <div class="manager_main_curses">
                @php
                    $caCurst=\App\CourseType::where("type","courses")->orderby("sort")->get();
                    if(!is_null($typeControl)){
                        $caCurst=$caCurst->where("id",$typeControl);
                    }
                @endphp
                @foreach($caCurst as $keyxs=>$catalog)
                    <div class="interface">
                        <div class="interface_head">
                            <h2>
                                {{$keyxs+1}}. {{$catalog->name}}
                            </h2>
                        </div>
                        <div class="interface_main">

                            @foreach(\App\Course::where("courseType_id",$catalog->id)->where("type","courses")->where("delete","0")->get() as $cours)
                                <div class="curs">
                                    <a href="{{url_custom('/admin/courses/'.$cours->id)}}" class="a-clock"></a>


                                    @php
                                        $check_list=  (\App\CourseUser::where("course_id",$cours->id)->where("user_id",Auth::user()->id)->first());
                                        $topsd="0.5";
                                    @endphp

                                    @if (\App\Permission_date::check("course", "update", 'Db', 0))
                                        <a href="{{url_custom('/admin/courses/add/info/'.$cours->id)}}"
                                           class="setting_edit">
                                            <i class="icn icn-edit"></i>
                                        </a>
                                        @php
                                            $topsd="3.5";
                                        @endphp
                                    @endif


                                    @if($cours->shared=="1" || !is_null($check_list))
                                        <div
                                           style=" margin-right: auto; top: {{$topsd}}rem; "
                                           class="setting_edit">
                                            <i class="icn icn-locked"></i>
                                        </div>
                                    @else
                                        <div
                                           style=" margin-right: auto; top: {{$topsd}}rem;background-color: green; "
                                           class="setting_edit">
                                            <i class="icn icn-unlocked"></i>
                                        </div>
                                    @endif


                                    <div class="curs_img">
                                        <div class="prop">
                                            <div class="prop_img">
                                                <div class="prop_img_src"
                                                     style="background-image: url('{{$cours->images}}');background-size: contain; background-position: center center; background-repeat: no-repeat;">

                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="curs_main">
                                        <div class="title">
                                            <div>
                                                {{$cours->name}}
                                            </div>
                                        </div>
                                        <div class="content">
                                            <p>
                                                {{$cours->mini}}
                                            </p>
                                        </div>

                                    </div>
                                </div>
                            @endforeach

                        </div>
                    </div>
                @endforeach
            </div>
        </div>


@endsection
