@extends('auth.layouts.app')

@section('content')
<div class="bulpForm">

    <div class="bulpForm_body">
        <div class="" style="text-align: center;   ">
            <div class="icons_box" style="">
                <img src="/public/media/admin/img/user.png" alt="">
            </div>
        </div>
        <div class="container_img" style=" position: absolute; top: 0; left: 0; width: 100%; padding: 10px 15px; ">
            <img width="134" src="/public/media/client/images/logo_n_i.png">
        </div>
        <div class="bulpForm_body_head" style=" padding-top: 0; ">
            <h2 class="msg">Авторизация</h2>
        </div>
        <form id="sign_in" class="bulpForm_body_form" role="form" method="POST" action="{{ url_custom('/login') }}">
            {{ csrf_field() }}


            {{maskInputNew($errors, ["name"=>"login", "type"=>"text", "ph"=>"Имя пользователя", "ll"=>false])}}
            {{maskInputNew($errors, ["name"=>"password", "type"=>"password", "ph"=>"Пароль", "ll"=>false])}}


            <div class="autocheck">
                <button type="submit" class="btn btn-sm btn-outline-secondary waves-effect px-4 btn-avriya">Войти
                </button>
            </div>
            <div class="navlink">
                <div class="form-check">
                    <div class="custom-control custom-checkbox">
                        <input class="custom-control-input" type="checkbox" name="remember" id="remember">
                        <label class="custom-control-label" for="remember">Запомнить меня</label>
                    </div>
                </div>
                <a href="{{ url('/admin/password/reset') }}">Забыли пароль?</a>
            </div>
        </form>
    </div>
</div>

<style>
    html, body {
        font-size: 16px !important;
    }

    body {
        background-size: cover;
        background-position: center center;
        font-family: 'Open Sans', sans-serif;
        background: rgba(246, 203, 24, 0.4);
    }

    .bulpForm_body {
        background: #F6CB18;
    }

    .bulpForm {
        width: 30rem;
    }

    .btn-avriya {
        margin: auto;
        background-color: #A6C437;
        border: none;
        color: #fff;
        background-position: center;
        border-radius: 16px;
        font-size: 20px;
        width: 240px;
        padding: 8px 0px;
    }

    .form-group {
        margin-bottom: 2rem;
    }

    .form-check {
        padding-left: 0;
    }

    .form-control-new::placeholder {
        color: #DFDFDF;
    }

    .form-control-new {
        height: 60px;
        border: none;
        border-radius: 8px;
        padding-left: 60px;
        background-image: url(/public/media/admin/img/user_login.png) !important;
        background-size: 27px;
        background-repeat: no-repeat;
        background-position: 15px 18px;
        font-size: 1.1rem !important;
    }

    .form-control-new-i2 {
        background-image: url(/public/media/admin/img/pass.png) !important;
    }

    .navlink {
        color: #6C6C6C;
        font-size: 0.89rem;
        margin-top: 2.22rem;
    }

    .icons_box {
        width: 5.83rem;
        height: 5.83rem;
        transform: translateY(-50%);
        background-color: #E60D81;
        display: flex;
        align-items: center;
        justify-content: center;
        border-radius: 10rem;
        margin: auto;
    }

    .icons_box img {
        width: 2.88rem;
    }
</style>
@endsection
