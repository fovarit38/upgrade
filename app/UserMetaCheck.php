<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserMetaCheck extends Model
{
    protected $dates = [
        'done_time'
    ];
}
