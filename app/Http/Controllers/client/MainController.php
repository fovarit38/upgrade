<?php

namespace App\Http\Controllers\client;

use App\Http\Controllers\Controller;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use Image;
use Mail;
use Auth;

class MainController extends Controller
{

    public function __construct()
    {


    }

    public function token_create($token)
    {
        $_1c = \App\_1c_User::where("_token", $token)->first();
        if (!is_null($_1c)) {

            $user_email = \App\User::where("email", $_1c->email)->first();
            $user_tel = null;
            if (validate_phone_number($_1c->personal_phone)) {
                $user_tel = \App\User::where("email", validate_phone_number($_1c->personal_phone))->get();
            }

            if (!is_null($user_email)) {
                Auth::login($user_email);
                return redirect()->to(url_custom('/admin'));
            } else if (!is_null($user_tel)) {
                Auth::login($user_tel);
                return redirect()->to(url_custom('/admin'));
            } else {
                return 'error';
            }
        }
    }


    public
    function pre()
    {

        return view('views.pre');
    }

    public
    function index()
    {


        return view('views.main');
    }

    public
    function send_se(Request $request)
    {
        $request = $request->all();

        if (isset($request["oficials_id"])) {
            $request["oficials"] = \App\Official::find($request["oficials_id"])->name;
        }

        Mail::send("orders", ["data" => $request], function ($message) use ($request) {
            $message->from("test12312s@mail.ru", "test12312s")->to("training@asem.kz")->subject("Ваш заявка");
        });

        return redirect()->to("/")->with("alert", "Спасибо, ваша заявка принята.");
    }

    public
    function contact()
    {
        return view('views.contact');
    }

    public
    function gallery()
    {
        return view('views.gallery');
    }

    public
    function news($id)
    {

        $news = \App\Gallery::find($id);
        return view('views.news_one', compact('news'));

    }

    public
    function about()
    {
        return view('views.about');
    }

}
