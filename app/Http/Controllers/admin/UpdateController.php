<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Cache;
use Schema;
use Str;
use Hash;
use App\Mail\MessageShipped;
use Mail;

class UpdateController extends Controller
{


    public function __construct()
    {
        $this->middleware('auth');
    }


    public function update_model(Request $request)
    {
        $files = $request;
        $request = $request->all();

        if (isset($request["name_date"])) {
            $request["name_save"] = implode(" ", $request["name_date"]);
            unset($request["name_date"]);

        }
        $profilUser = null;
        if (isset($request["user_role"])) {
            $profilUser = $request["user_role"];
            unset($request["user_role"]);
        }


        foreach ($_FILES as $nameInput => $fileout) {
            if (isset($request[$nameInput])) {
                if ($files->hasFile($nameInput)) {

                    $photo_path = $files->file($nameInput);
                    $type = explode(".", $photo_path->getClientOriginalName());
                    if (count($type) > 1) {
                        $m_path = Str::random(5) . "_file." . $type[1];
                        $moveTo = 'media/Update/' . str_replace("/", "_", $fileout["type"]) . '/';
                        $photo_path->move($moveTo, $m_path);
                        $request[$nameInput] = "/public/" . $moveTo . $m_path;
                        if (isset($request[$nameInput . "_alt"])) {
                            unset($request[$nameInput . "_alt"]);
                        }
                    }
                } else {
                    unset($request[$nameInput]);
                }
            }
        }


        $id = $request["id"];
        $model_name = $request["model_name"];
        $model = null;
        if ($id == 0) {
            $model = app("\App\\$model_name");
        } else {
            $model = app("\App\\$model_name");
            $model = $model->find($id);
        }

        if ($model_name == "Shop") {
            if (!isset($request["product_id_save"])) {
                $request["product_id_save"] = [];
            }
        }

        if ($model_name == "Permission_grup") {
            \App\Permission_date::where("permision_group", $request["id"])->delete();
            if (isset($request["permission"])) {
                foreach ($request["permission"] as $keyxs => $select) {
                    foreach ($select as $sls) {
                        $p_select = new \App\Permission_date;
                        $p_select->crud = $keyxs;
                        $p_select->permision_group = $request["id"];
                        $p_select->table = $sls;
                        $p_select->type = 'db';
                        $p_select->assess = 1;
                        $p_select->save();
                    }
                }
            }

        }

        if (is_null($model)) {
            return redirect()->back()->with("alert", "Модель не найденна");
        }


        foreach ($request as $key => $input) {
            $input_save = explode("_sa", str_replace("_alt", "", $key));
            $input_pass = explode("_", $key);
            $is_pass = end($input_pass);
            $is_save = end($input_save);

            if ("sa" . $is_save == "save") {
                if (is_array($input)) {
                    $model->{$input_save[0]} = json_encode($input, JSON_UNESCAPED_UNICODE);
                } else {
                    if ($input_save[0] == "tel") {
                        if (validate_phone_number($input) == false) {
                            $model->{$input_save[0]} = $input;
                        } else {
                            $model->{$input_save[0]} = validate_phone_number($input);
                        }
                    } else {
                        $model->{$input_save[0]} = $input;
                    }
                }
            } else if ($is_pass == "pass") {
                if (str_replace(" ", "", $input)) {
                    $model->{$input_pass[0]} = Hash::make($input);

                    if ($model_name == "User") {
                        $register = new \App\Register;
                        $register->name = "изменение пароля";
                        $register->index_inv = "изминение паролья у пользователя " . $request["name_save"];
                        $register->name_key = "user_pass";
                        $register->save();
                    }
                }
            }
        }
        $model->save();


        $columns = Schema::getColumnListing($model->getTable());
        if (in_array("path", $columns) && in_array("id", $columns) && in_array("slug", $columns)) {
            $columSlug = "";
            if (in_array("title", $columns)) {
                $columSlug = "title";
            } else if (in_array("name", $columns)) {
                $columSlug = "name";
            }
            if ($columSlug != "") {
                $urlSlug = Urlcode(LC($model->{$columSlug}, "ru"));
                $firstEditSlug = true;
                while (!is_null(\app("\App\\$model_name")->where("slug", $urlSlug)->first())) {
                    $urlSlug .= ($firstEditSlug ? "-" : "") . $model->id;
                    $firstEditSlug = false;
                }
                if ($urlSlug != "") {
                    $model->slug = $urlSlug;
                    $model->path = $model->slug;
                    $model->save();
                }
            }
        }

        if (!is_null($profilUser)) {
            \App\Permission_user::where("user_id", $model->id)->delete();
            $userProfile = new \App\Permission_user;
            $userProfile->grup_id = $profilUser;
            $userProfile->user_id = $model->id;
            $userProfile->save();
        }


        if (isset($request["remove"])) {
            if ($model_name == "Course") {

                $model->delete = 1;
                $model->save();

                $register = new \App\Register;
                $register->name = "Удаление курса";
                $register->index_inv = "Удалён курс " . $model->name;
                $register->name_key = "course_remove";
                $register->save();

                return redirect()->to("/admin/" . ($model->type == "library" ? 'library' : 'courses'))->with("alert", ($model->type == "library" ? 'Материал из библиотеки' : 'Курс') . " удален");
            }
        }


        if (isset($request["message"])) {
            if (isset($request["message"]["sms"])) {
//                $request["message"]["sms"] = "+77089681134";
                file_get_contents('https://smsc.ru/sys/send.php?login=almaty-kense&psw=Temporary123&phones=' . (str_replace("+", "", $request["message"]["sms"])) . '&mes=' . str_replace("\n", "", Strip_tags($request["message_save"])));
            }
            if (isset($request["message"]["email"])) {
                Mail::to(explode(",", $request["message"]["email"]))->send(new MessageShipped(["heading" => "Уведомление с сайта as7.kz", "message" => $request["message_save"]]));
            }
            if (isset($request["message"]["bitrix"])) {
                $sendAPi = \App\Cicada\CRest::call(
                    'im.notify', ["to" => $request["message"]["bitrix"], "message" => $request["message_save"]]
                );
            }
        }

        return redirect()->to(str_replace("{id}", $model->id, ($request["path"])))->with("alert", "Сохранено");
    }


    public function status_set($id, Request $request)
    {

    }

}
