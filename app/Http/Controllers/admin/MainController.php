<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Cache;
use Illuminate\Support\Facades\Auth;
use TableClass;
use Str;
use Mail;
use Illuminate\Support\Facades\Schema;
use NCANodeClient;

class MainController extends Controller
{


    public function __construct()
    {
        $this->middleware('auth');


        $this->middleware(function ($request, $next) {
            if (Auth::user()->ban == "1") {
                echo "Вы заблокированы";
                exit();
            }
            return $next($request);
        });

    }


    public function ecp()
    {

        return view('views.users_one_ecp');
    }

    public function ecp_post(Request $request)
    {
        $files = $request;
        $request = $request->all();
        if ($_FILES["file"]["error"] != 0) {
            return redirect()->back()->with("alert", 'Проблема с Файлом для подписания');
        }
        if ($_FILES["ecp"]["error"] != 0) {
            return redirect()->back()->with("alert", 'Проблема с ЕЦП ключом для подписания');
        }
        $nca = new NCANodeClient('http://127.0.0.1:14579');

        if ($nca->nodeInfo() == "error") {
            return redirect()->back()->with("alert", 'Сервер не доступен');
        }

        $ecp_info = $nca->pkcs12Info(base64_encode(file_get_contents(($_FILES["ecp"]["tmp_name"]))), $request["pass"]);

        if (is_array($ecp_info)) {
            if (isset($ecp_info["status"])) {
                if (isset($ecp_info["message"])) {
                    if ($ecp_info["status"] > 0) {
                        return redirect()->back()->with("alert", $ecp_info["message"]);
                    }
                }
            }
        }

        if ($ecp_info->getRaw()["keyUsage"] == "AUTH") {
            return redirect()->back()->with("alert", 'ВЫ выбрали не тот ЕЦП ключ для подписания, выберите RSA256 ключ');
        }


        $p12InBase64 = base64_encode(file_get_contents(($_FILES["ecp"]["tmp_name"])));
        $info_ssh = base64_encode(file_get_contents($_FILES["file"]["tmp_name"]));
        $info = $nca->rawSign($info_ssh, $p12InBase64, $request["pass"]);
        $text = (base64_decode($info["result"]["cms"]));

//        $photo_path = $files->file("ecp");
//        $type = explode(".", $photo_path->getClientOriginalName());
//        if (count($type) > 1) {
//            $m_path = Str::random(8) . "_file." . $type[1];
//            $moveTo = 'media/Update/' . str_replace("/", "_", $_FILES["ecp"]["type"]) . '/';
//            $photo_path->move($moveTo, $m_path);
//            $request["ecp"] = "/public/" . $moveTo . $m_path;
//        }


        $file_path = $files->file("file");

        $user_names = $ecp_info->getRaw()["subject"];


        $type = explode(".", $file_path->getClientOriginalName());
        if (count($type) > 1) {
            $m_path = Str::random(8) . "_file." . $type[1];
            $moveTo = 'media/Update/' . str_replace("/", "_", $_FILES["file"]["type"]) . '/';
            $file_path->move($moveTo, $m_path);
            $request["file"] = "/public/" . $moveTo . $m_path;

            $file_get_o = "/media/ecp/" . Str::random(8) . "." . $type[1] . ".cms";
            $fp = fopen("." . $file_get_o, "w");
            fwrite($fp, $text);
            fclose($fp);


            $eds = new \App\EdsItem;
            $eds->user_id = Auth::id();
            $eds->file = $request["file"];
            $eds->file_out = "/public/" . $file_get_o;
            $eds->user_name = json_encode($user_names);
            $eds->save();
        }

        return redirect()->back()->with("alert", 'Файл подписан');
//        $info = $nca->cmsSign($files, $p12InBase64, "Aa1234");
//        $text = base64_decode($info["cms"]);
//        $fp = fopen("file.cms", "w");
//        fwrite($fp, $text);
//        fclose($fp);
    }

    public function test_add()
    {

        return view('views.courses.edit.testing.one');
    }

    public function test_add_otvet()
    {

        return view('views.courses.edit.testing.onde_otvet');
    }


    public function task_add()
    {

        return view('views.courses.edit.task.one');
    }

    public function task_add_otvet()
    {

        return view('views.courses.edit.task.onde_otvet');
    }


    public function index()
    {

//        return redirect()->to(url_custom('/admin/users/' . Auth::user()->id));
        return view('recruitment.main');
    }

    public function recruitment()
    {

        return view('recruitment.main');
    }

    public function recruitment_work()
    {

        return view('recruitment.work');
    }

    public function recruitment_job()
    {
        return view('recruitment.job');
    }

    public function recruitment_order()
    {
        return view('recruitment.order');
    }

    public function recruitment_candidates()
    {
        return view('recruitment.candidates');
    }

    public function recruitment_podbor()
    {
        return view('recruitment.podbor');
    }

    public function recruitment_cron()
    {
        return view('recruitment.cron');
    }

    public function recruitment_vakansi()
    {
        return view('recruitment.vakansi');
    }

    public function recruitment_basa()
    {
        return view('recruitment.basa');
    }

    public function recruitment_otchet()
    {
        return view('recruitment.otchet');
    }

    public function recruitment_npc()
    {
        return view('recruitment.nps');
    }

    public function recruitment_personala()
    {
        return view('recruitment.personala');
    }

    public function recruitment_recruting()
    {
        return view('recruitment.recruting');
    }

    public function recruitment_otcperconal()
    {
        return view('recruitment.otcperconal');
    }

    public function recruitment_reglamenti()
    {
        return view('recruitment.reglamenti');
    }

    public function recruitment_reglmint_bisnes()
    {
        return view('recruitment.reglmint.bisnes');
    }

    public function recruitment_reglmint_docent()
    {
        return view('recruitment.reglmint.docent');
    }

    public function recruitment_reglmint_tamplase()
    {
        return view('recruitment.reglmint.tamplase');
    }


    public function adaptacia()
    {
        return view('adaptacia.main');

    }

    public function adaptacia_new_work()
    {
        return view('adaptacia.new.work');
    }

    public function adaptacia_new_organiz()
    {
        return view('adaptacia.new.organiz');
    }

    public function adaptacia_new_communucat()
    {
        return view('adaptacia.new.communucat');
    }

    public function adaptacia_new_material()
    {
        return view('adaptacia.new.material');
    }


    public function ocenka()
    {
        return view('ocenka.main');
    }


    public function kadrovoe()
    {
        return view('kadrovoe.main');
    }


    public function kadrovoe_base()
    {
        return view('kadrovoe.page.base');
    }

    public function kadrovoe_document()
    {
        return view('kadrovoe.page.document');
    }
    public function kadrovoe_commandirovka()
    {
        return view('kadrovoe.page.comandirovkaa');
    }


    public function trating()
    {
        return view('trating.main');
    }


    public function courses()
    {

        ch_pr(\App\Permission_date::check("course", "select", 'Db', 0));

        $nav = ['courses'];
        return view('views.courses', compact('nav'));

    }


    public function courses_create($id)
    {

        $nav = ['courses'];
        $course = \App\Course::where("delete", "0")->find($id);
        $code = "";
        if (!is_null($course)) {

            $chec_is = \App\UserMetaCheck::where("course_id", $course->id)->where("user_id", Auth::user()->id)->first();

            if (is_null($chec_is)) {

                $date = \App\CourseUser::where("user_id", Auth::user()->id)->where("course_id", $course->id)->first();

                $code = Str::random(8);
                $user_new_test = new \App\UserMetaCheck;
                $user_new_test->name = $course->name;
                $user_new_test->course_id = $course->id;
                $user_new_test->code = $code;
                $user_new_test->user_id = Auth::user()->id;
                if (isset($date->created_at)) {
                    $user_new_test->setdate = $date->created_at;

                }

                $user_new_test->save();
            } else {
                $code = $chec_is->code;
            }

            return redirect()->to(url_custom('/admin/courses/' . $id . '/start/' . $code));
        }
    }


    public function courses_next(Request $request, $id, $single)
    {
        $request = $request->all();


        $chec_is = \App\UserMetaCheck::where("re-publish", "0")->where("code", $single)->where("course_id", $id)->first();
        $save = new \App\UserMetaCheckItem;
        $save->importance = $request["importance"]["name"];
        $save->type = $request["type"]["name"];
        $save->type_id = $request["type"]["id"];
        $save->userMetaCheck_id = $chec_is->id;
        $save->icon_type = $request["icon_type"];
        $save->save();


        if (isset($request["save"])) {
            if (isset($request["save"]["text"])) {
                $saveParrent = new \App\UserMetaCheckItem;
                $saveParrent->importance = $request["save"]["text"];
                $saveParrent->type = "course_meta_data";
                $saveParrent->type_id = $request["save"]["id"];
                $saveParrent->userMetaCheck_id = $chec_is->id;
                $saveParrent->userMetaCheckItem_id = $save->id;
                $saveParrent->save();
            }
        }

        if (isset($request["task"])) {
            foreach ($request["task"] as $keyxs => $keyd) {

                $old_key = \App\CourseMetaData::find($keyxs);
                $Paresing = new \App\UserMetaCheckItem;
                $Paresing->importance = $old_key->importance;
                $Paresing->type = "course_meta_data";
                $Paresing->type_id = $keyxs;
                $Paresing->userMetaCheck_id = $chec_is->id;
                $Paresing->userMetaCheckItem_id = $save->id;
                $Paresing->save();

                $saveParrent = new \App\UserMetaCheckItem;
                $saveParrent->importance = $keyd;
                $saveParrent->type = "course_meta_data";
                $saveParrent->userMetaCheck_id = $chec_is->id;
                $saveParrent->userMetaCheckItem_id = $Paresing->id;
                $saveParrent->save();

            }
        }

        if (isset($request["list"])) {
            foreach ($request["list"] as $keyxs => $keyd) {

                $old_key = \App\CourseMetaData::find($keyxs);
                $saveParrent = new \App\UserMetaCheckItem;
                $saveParrent->importance = $old_key->importance;
                $saveParrent->type = "course_meta_data";
                $saveParrent->type_id = $old_key->id;
                $saveParrent->userMetaCheck_id = $chec_is->id;
                $saveParrent->userMetaCheckItem_id = $save->id;
                $saveParrent->save();

                $test_list = \App\CourseMetaData::where("courseMetaData_id", $old_key->id)->get();
                foreach ($test_list as $ks) {
                    $savenes = new \App\UserMetaCheckItem;
                    $savenes->importance = $ks->importance;
                    $savenes->type = "course_meta_data";
                    $savenes->type_id = $ks->id;
                    $savenes->userMetaCheck_id = $chec_is->id;
                    $savenes->boolead_true = $ks->boolead;
                    if (in_array($ks->id, $keyd)) {
                        $savenes->boolead = 1;
                    }
                    $savenes->userMetaCheckItem_id = $saveParrent->id;
                    $savenes->save();
                }
            }
        }


        return redirect()->to(url_custom('/admin/courses/' . $id . '/start/' . $single));
    }


    public function courses_start($id, $code)
    {
        $nav = ['courses'];
        $course = \App\Course::where("delete", "0")->find($id);
        $chec_is = \App\UserMetaCheck::where("re-publish", "0")->where("code", $code)->where("course_id", $course->id)->first();
        $pre_ingnor = [];

        if (!is_null($chec_is)) {
            $dateOld = \App\UserMetaCheckItem::where("userMetaCheck_id", $chec_is->id)->where("status", "progressing")->get()->where("type", "course_metas")->groupby("type_id")->toarray();
            if (count($dateOld) > 0) {
                $pre_ingnor = array_keys($dateOld);
            }
        }

        $courseMeta = \App\CourseMeta::where("course_id", $id)->orderby("sort")->get();
        if (count($courseMeta) == count($pre_ingnor)) {
            $codesave = Str::random(8);
            $chec_is->status = "done";
            $chec_is->done_time = date("Y-m-d G:i:s");
            $chec_is->save();
            foreach (\App\UserMetaCheckItem::where("userMetaCheck_id", $chec_is->id)->where("status", "progressing")->get() as $save_link) {
                $save_link->status = "done";
                $save_link->end_sing_code = $codesave;
                $save_link->save();
            }
            return redirect()->to(url_custom('/admin/courses/' . $id))->with("alert", "Вы прошли курс");
        }
        return view('views.courses_start', compact('pre_ingnor', 'courseMeta', 'nav', 'chec_is', 'id', 'course'));
    }

    public function courses_one($id)
    {
        $nav = ['courses'];
        $course = \App\Course::where("delete", "0")->find($id);

        $chec_is = \App\UserMetaCheck::where("re-publish", "0")->where("course_id", $course->id)->first();


        $pre_ingnor = [];
        if (!is_null($chec_is)) {
            $dateOld = \App\UserMetaCheckItem::where("userMetaCheck_id", $chec_is->id)->where("status", "progressing")->get()->where("type", "course_metas")->groupby("type_id")->toarray();
            if (count($dateOld) > 0) {
                $pre_ingnor = array_keys($dateOld);
            }
        }

        $courseMeta = \App\CourseMeta::where("course_id", $id)->get();
        $btn = [true, "Пройти обучение"];
        if (count($courseMeta) == 0) {
            $btn = [false, "Нет материалов для обучения"];
        } else {
            $procesing = array_keys(\App\UserMetaCheck::where("user_id", Auth::user()->id)->where("course_id", $id)->get()->groupby("id")->toarray());

            $procesing_done = \App\UserMetaCheckItem::wherein("userMetaCheck_id", $procesing)->where("status", "progressing")->first();

            if (is_null($procesing_done)) {

            } else {
                if (is_null($procesing)) {
                    $btn = [true, "Перепройти обучение"];
                } else {
                    $btn = [true, "Продолжить обучение"];
                }
            }
        }
        return view('views.courses_one', compact('nav', 'id', 'course', 'btn'));
    }


    public function courses_checktest($code)
    {
        if (\App\Permission_date::check("courses_check", "select", 'Db', 0)) {
            $nav = ['courses'];
            $info = \App\UserMetaCheck::where("code", $code)->first();
            $model_list = \App\UserMetaCheckItem::where("userMetaCheck_id", $info->id)->where("status", "done")->where("type", "course_metas")->get();
            return view('views.courses.check.courses_school', compact('nav', 'info', 'model_list', 'code'));
        } else {
            return "Доступ закрыт";
        }
    }

    public function courses_bonus($code)
    {
        if (\App\Permission_date::check("courses_check", "select", 'Db', 0)) {
            $nav = ['courses'];

            $info = \App\UserMetaCheck::where("code", $code)->first();
            $model_list = \App\UserMetaCheckItem::where("userMetaCheck_id", $info->id)->where("type", "course_metas")->get();
            return view('views.courses.check.bonus_list', compact('nav', 'info', 'model_list', 'code'));
        } else {
            return "Доступ закрыт";
        }
    }

    public function courses_bonus_add(Request $request)
    {
        if (\App\Permission_date::check("courses_check", "select", 'Db', 0)) {
            $request = $request->all();
            $bonus_add = new \App\BonusAdd;
            if ($request["id"] != "0") {
                $bonus_add = \App\BonusAdd::find($request["id"]);
            }
            $bonus_add->code = $request["code"];
            $bonus_add->user_add = Auth::user()->id;
            $bonus_add->user_id = $request["user_id"];
            $bonus_add->bonus = !is_null($request["bonus_save"]) ? $request["bonus_save"] : 0;
            $bonus_add->medal = !is_null($request["medal_save"]) ? $request["medal_save"] : 0;
            $bonus_add->efficiency = !is_null($request["efficiency_save"]) ? $request["efficiency_save"] : 0;
            $bonus_add->save();
            return redirect()->to(url_custom('/admin/courses/check/' . $request["code"] . '/bonus'));
        } else {
            return "Доступ закрыт";
        }
    }

    public function courses_point(Request $request)
    {
        $request = $request->all();

    }

    public function user_update(Request $request)
    {
        $request = $request->all();

    }

    public function users()
    {
        if (\App\Permission_date::check("users", "select", 'Db', 0)) {
            $users = \App\User::get();
            $nav = ['users'];
            return view('views.users', compact('users', 'nav'));
        } else {
            return "Доступ закрыт";
        }
    }


    public function plans()
    {
        if (\App\Permission_date::check("users", "select", 'Db', 0)) {
            $nav = ['plans'];
            $caCurst = \App\CourseType::orderby("sort")->get();
            return view('views.users', compact('caCurst', 'nav'));
        }
    }

    public function users_one($id)
    {
        $user = \App\User::find($id);
        $nav = ['users'];
        return view('views.users_one', compact('user', 'nav'));
    }

    public function users_one_edit($id)
    {
        $user_check = false;
        if (\App\Permission_date::check("users", "select", 'Db', 0)) {
            $user_check = true;
        }
        if ($id == Auth::user()->id) {
            $user_check = true;
        }

        if ($user_check) {
            $user = \App\User::find($id);
            $nav = ['users'];
            return view('views.users_one_edit', compact('user', 'nav'));
        } else {
            return "Доступ закрыт";
        }


    }


    public function courses_list()
    {
        $nav = ['courses'];
        return view('views.courses.edit_list', compact('nav'));
    }

    public function courses_info($id)
    {
        if ($id == 0) {
            ch_pr(\App\Permission_date::check("course", "insert", 'Db', 0));
        } else {
            ch_pr(\App\Permission_date::check("course", "update", 'Db', 0));
        }

        $nav = ['courses'];
        $course = \App\Course::where("delete", "0")->find($id);
        $model_name = "Course";
        return view('views.courses.edit.courses_info', compact('id', 'nav', 'model_name', 'course'));

    }


    public function users_1s_list()
    {
        $nav = [];
        $users = \App\_1c_User::get();
        return view('views.users_1s', compact('nav', 'users'));


    }

    public function users_1s(Request $request)
    {
//        $request = $request->all();
//        $user1c = \App\_1c_User::find($request["id"]);
//        if (is_null($user1c)) {
//            return redirect()->back();
//        }
//
//
//        $url_token = $_SERVER['HTTP_ORIGIN'] . '/user/confirm/' . $user1c->_token;
//

        $users = \App\_1c_User::find(208);

        $sendAPi = \App\Cicada\CRest::call(
            'im.notify', ["to" => $users->id_1c, "message" => "Уведомление, пользователь " . ($users->name) . " телефон " . ($users->personal_mobile) . " должность " . ($users->work_position) . " компания " . ($users->work_company)]
        );

//        dd($sendAPi);
//
//        if (isset($sendAPi["result"])) {
//            $user1c->confirm_send = 1;
//            $user1c->save();
//        }
//        $sendAPi = \App\Cicada\CRest::call(
//            'im.notify', ["to" => $user1c->id_1c, "message" => "Для авторизации перейдите по ссылки " .$url_token]
//        );
        $addTask = \App\Cicada\CRest::call(
            'user.get', ["active" => 0]
        );
        dd($addTask);
        $id_in = [];
        for ($i = 0; $i < 1000; $i++) {
            array_push($id_in, $i);
        }
        $id_in = array_chunk($id_in, 40);
        $keyxs = array_keys(\App\_1c_User::get()->groupby("id_1c")->toarray());

        $return_user = [];
        foreach ($id_in as $ach) {
            $addTask = \App\Cicada\CRest::call(
                'user.get', ["ID" => $ach]
            );
            $return_ars = [];
            foreach ($addTask["result"] as $tabls) {
                unset($tabls["UF_DEPARTMENT"]);
                $tabls["id_1c"] = $tabls["ID"];
                unset($tabls["ID"]);
                array_push($return_ars, $tabls);
            }
            $return_user = array_merge($return_user, $return_ars);
        }

        foreach ($return_user as $user_sling) {
            if (!in_array($user_sling["id_1c"], $keyxs)) {
                $newsuser = new \App\_1c_User;
                $newsuser->_token = rand(1000000, 9999999);
                foreach ($user_sling as $keyxsas => $val) {
                    eval('$newsuser->' . $keyxsas . '="' . htmlspecialchars($val, ENT_QUOTES) . '";');
                }
                $newsuser->save();
            }
        }

//        Schema::table("_1c__users", function ($table) {
//            $table->string('name')->nullable();
//        });

//        foreach ($addTask["result"] as $tabls) {
//            unset($tabls["UF_DEPARTMENT"]);
//            $tabls["id_1c"] = $tabls["ID"];
//            unset($tabls["ID"]);
//
//            Schema::table("_1c__users", function ($table) use ($tabls) {
//                foreach ($tabls as $keyxs => $tablsa) {
//                    $columns = Schema::getColumnListing("_1c__users");
//                    if (!in_array(mb_strtolower($keyxs), $columns)) {
//                        eval('$table->string("' . mb_strtolower($keyxs) . '")->nullable();');
//                    }
//                }
//            });
//            dd($tabls);
//        }


    }

    public function permission()
    {
        if (\App\Permission_date::check("permission", "select", 'Db', 0)) {
            $nav = ['courses'];

            return view('views.permission.list', compact('nav'));
        } else {
            return "Доступ закрыт";
        }
    }

    public function courses_school($id)
    {
        if ($id == 0) {
            ch_pr(\App\Permission_date::check("course", "insert", 'Db', 0));
        } else {
            ch_pr(\App\Permission_date::check("course", "update", 'Db', 0));
        }
        $nav = ['courses'];
        $course = \App\Course::where("delete", "0")->find($id);
        $model_name = "Course";
        return view('views.courses.edit.courses_school', compact('id', 'nav', 'model_name', 'course'));

    }

    public function courses_exam()
    {


        $nav = ['courses'];
        return view('views.courses.edit.courses_exam', compact('nav'));

    }

    public function courses_check()
    {
        ch_pr(\App\Permission_date::check("courses_check", "select", 'Db', 0));

        $nav = ['check'];
        return view('views.courses_check', compact('nav'));

    }

    public function courses_add_user(Request $request)
    {
        $request = $request->all();


        $userNew = new \App\CourseUser;
        if ($request["useridadd"] != 0) {
            $userNew = \App\CourseUser::find($request["useridadd"]);
        }
        $userNew->courseDateEnd = $request["courseDateEnd_save"];
        $userNew->course_id = $request["curseid"];
        $userNew->user_id = $request["user"];
        $userNew->save();
        if (isset($request["remove"])) {
            $userNew->delete();
        }
        return redirect()->to(url_custom('/admin/courses/add/collaborator/' . $request["curseid"]))->with("alert", "Пользователь назначен на курс");


    }

    public function courses_collaborator($id)
    {
        if ($id == 0) {
            ch_pr(\App\Permission_date::check("course", "insert", 'Db', 0));
        } else {
            ch_pr(\App\Permission_date::check("course", "update", 'Db', 0));
        }
        $nav = ['courses'];
        $course = \App\Course::where("delete", "0")->find($id);
        $model_name = "Course";
        return view('views.courses.edit.courses_collaborator', compact('id', 'nav', 'model_name', 'course'));
    }

    public function table()
    {
        return view('main');
    }


    public function developer($status)
    {
        if ($status != "true") {
            $status = false;
        }
        Cache::forever('dev', $status);


        return redirect()->back();
    }


    public function orders()
    {
        return view('views.orders');
    }


    public function s_text()
    {
        ch_pr(\App\Permission_date::check("StaticText", "select", 'Db', 0));

        $model_name = "StaticText";
        $thead_nav = \App\StaticText::get()->groupby("page");
        $tbody = [];

        $page = '';
        if (isset($_REQUEST["page"])) {
            $tbody = \App\StaticText::where("page", $_REQUEST["page"])->get();
            $page = $_REQUEST["page"];
        }

        $thead = ["id" => "id", "name_key" => "name_key"];

        $table_link = ["/admin/model//", "id"];
        $nav = [''];
        return view('views.s_text', compact("model_name", 'nav', 'page', "thead_nav", "tbody", "thead", "table_link"));

    }


    public function update_text(Request $request)
    {
        $files = $request;
        $request = $request->all();
        $request = $request["save"];

        if (isset($_FILES["save"])) {
            foreach ($_FILES["save"]["name"] as $nameInput => $fileout) {
                if (isset($request[$nameInput])) {
                    if ($files->hasFile("save." . $nameInput)) {
                        $photo_path = $files->file("save." . $nameInput);
                        $type = explode(".", $photo_path->getClientOriginalName());
                        if (count($type) > 1) {
                            $m_path = Str::random(5) . "_file." . $type[1];
                            $moveTo = 'media/Update/' . str_replace("/", "_", $_FILES["save"]["type"][$nameInput]) . '/';
                            $photo_path->move($moveTo, $m_path);
                            $request[$nameInput] = "/public/" . $moveTo . $m_path;

                        }
                    }
                }
            }
        }

        foreach ($request as $key => $colum) {
            $s_text = \App\StaticText::where("name_key", $key)->first();
            if (is_array($colum)) {
                $s_text->content = json_encode($colum, JSON_UNESCAPED_UNICODE);
            } else {

                $s_text->content = $colum;

            }
            $s_text->save();
        }
        return redirect()->back();
    }


    public function itemadd(Request $request)
    {

        if ($request["courses"]["id"] == 0) {
            ch_pr(\App\Permission_date::check("course", "insert", 'Db', 0));
        } else {
            ch_pr(\App\Permission_date::check("course", "update", 'Db', 0));
        }

        $files = $request;
        $request = $request->all();
        if (isset($request["remove"])) {
            \App\CourseMeta::find($request["remove"])->delete();
            return redirect()->to(url_custom('/admin/courses/add/school/' . $request["courses"]["id"]));
        }

        $gl_block = new \App\CourseMeta;
        if ($request["id"] != 0) {
            $gl_block = \App\CourseMeta::find($request["id"]);
        }
        foreach ($_FILES as $nameInput => $fileout) {
            if (isset($request[$nameInput])) {
                if ($files->hasFile($nameInput)) {

                    $photo_path = $files->file($nameInput);
                    $type = explode(".", $photo_path->getClientOriginalName());
                    if (count($type) > 1) {
                        $m_path = Str::random(5) . "_file." . $type[1];
                        $moveTo = 'media/Update/' . str_replace("/", "_", $fileout["type"]) . '/';
                        $photo_path->move($moveTo, $m_path);
                        $request[$nameInput] = "/public/" . $moveTo . $m_path;
                        if (isset($request[$nameInput . "_alt"])) {
                            unset($request[$nameInput . "_alt"]);
                        }
                    }
                } else {
                    unset($request[$nameInput]);
                }
            }
        }
        $gl_block->name = $request["name_save"];
        $gl_block->type = $request["type"];
        $gl_block->course_id = $request["courses"]["id"];
        $gl_block->save();

        \App\CourseMetaData::where("CourseMeta_id", $gl_block->id)->delete();


        if (isset($request["importance"])) {
            if (is_array($request["importance"])) {

                if (isset($request["importance"]["test"])) {
                    foreach ($request["importance"]["test"] as $testing) {

                        $courseMetaData = new \App\CourseMetaData;
                        $courseMetaData->importance = $testing["name"];
                        $courseMetaData->CourseMeta_id = $gl_block->id;
                        $courseMetaData->type = $request["type"];
                        $courseMetaData->save();

                        if (isset($testing["brews"])) {
                            foreach ($testing["brews"] as $testing_save) {
                                $courseMetaDataSa = new \App\CourseMetaData;
                                $courseMetaDataSa->importance = $testing_save["name"];
                                $courseMetaDataSa->CourseMeta_id = $gl_block->id;
                                $courseMetaDataSa->type = $request["type"];
                                $courseMetaDataSa->boolead = isset($testing_save["boolead"]) ? $testing_save["boolead"] : 0;
                                $courseMetaDataSa->courseMetaData_id = $courseMetaData->id;
                                $courseMetaDataSa->save();
                            }
                        }
                    }
                } else if (isset($request["importance"]["task"])) {

                    foreach ($request["importance"]["task"] as $testing) {
                        $courseMetaData = new \App\CourseMetaData;
                        $courseMetaData->importance = $testing["name"];
                        $courseMetaData->CourseMeta_id = $gl_block->id;
                        $courseMetaData->type = $request["type"];
                        $courseMetaData->save();
                    }

                }


            } else {
                $courseMetaData = new \App\CourseMetaData;
                $courseMetaData->importance = $request["importance"];
                $courseMetaData->CourseMeta_id = $gl_block->id;
                $courseMetaData->type = $request["type"];
                $courseMetaData->save();
            }
        } else if ($request["type"] == "pdf") {
            $courseMetaData = new \App\CourseMetaData;
            $courseMetaData->importance = $request["importance"];
            $courseMetaData->CourseMeta_id = $gl_block->id;
            $courseMetaData->type = $request["type"];
            $courseMetaData->save();
        }
        return redirect()->to(url_custom('/admin/courses/add/school/' . $request["courses"]["id"] . "?add=" . $request["type"] . "&edit=" . $gl_block->id));
    }

    public function update_position(Request $request)
    {
        $request = $request->all();
        $courseMeta = \App\CourseMeta::find($request["id"]);
        $courseMeta->sort = $request["position"];
        $courseMeta->save();
    }


    public function compani_list()
    {
        ch_pr(\App\Permission_date::check("compani", "select", 'Db', 0));

        $table_link = \App\Compani::get();
        return view('views.compani.list', compact("table_link"));

    }

    public function book()
    {
        $nav = ['book'];
        return view('views.book', compact('nav'));
    }
    public function personal()
    {
        $nav = ['personal'];
        return view('views.personal', compact('nav'));
    }
    public function regulations()
    {
        $nav = ['regulations'];
        return view('views.regulations', compact('nav'));
    }
    public function video()
    {
        ch_pr(\App\Permission_date::check("library", "select", 'Db', 0));
        $nav = ['video'];
        return view('views.library.library', compact('nav'));
    }

    public function library()
    {
        ch_pr(\App\Permission_date::check("library", "select", 'Db', 0));
        $nav = ['library'];
        return view('views.library.library', compact('nav'));
    }
    public function library_one($id)
    {
        $nav = ['courses'];
        $course = \App\Course::where("delete", "0")->find($id);
        $courseMeta = \App\CourseMeta::where("course_id", $id)->orderby("sort")->get();
        return view('views.library_start', compact('id', 'course', 'courseMeta'));
    }

    public function delete_fs($model, $id)
    {
        $model = app("\App\\$model");
        $model = $model->find($id);
        $model->delete();
        return redirect()->back();
    }


    public function setting()
    {
        $nav = ['setting'];
        return view('views.setting', compact('nav'));
    }

    public function user_ban($id, $ban)
    {
        $users = \App\User::find($id);
        $users->ban = $ban;
        $users->save();
        return redirect()->to(url_custom("/admin/users"));
    }

    public function orgshema()
    {
        $nav = [''];

        return view('views.orgshema', compact('nav'));
    }

}
