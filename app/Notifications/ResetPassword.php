<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class ResetPassword extends Notification
{
    use Queueable;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    protected $token;

    public function __construct($token)
    {

        $this->token = $token;
        //
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param mixed $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param mixed $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {

        return (new MailMessage)
            ->subject('Востановление пароля для сайта as7.kz')
            ->greeting('Востановление пароля для пользователя, ' . $notifiable->username)
            ->line('Вы получили это письмо, потому что мы получили запрос на сброс пароля для вашей учетной записи. Нажмите кнопку ниже, чтобы сбросить пароль:')
            ->action('Сброс пароля', 'https://' . $_SERVER['SERVER_NAME'] . "" . url_custom('/admin/password/reset/') . $this->token . '?email=' . urlencode($notifiable->email))
            ->line('Если вы не запрашивали сброс пароля, проигнорируйте данное письмо.')
            ->line('Спасибо за использование, ' . config('app.name'));
    }


    /**
     * Get the array representation of the notification.
     *
     * @param mixed $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
