<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class MessageShipped extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    protected $data;


    public function __construct($data)
    {
        $this->data = $data;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $headbox = $this->data["heading"];
        unset($this->data["heading"]);
        unset($this->data["_token"]);
        unset($this->data["_token"]);

        return $this->from(env("MAIL_USERNAME"))->subject($headbox)
            ->view('mail.message')->with("data", $this->data);
    }
}
