<?php


namespace App\Domains\Contracts;


interface MainContract
{
    const ID    =   'id';

    const PRODUCT_ID    =   'product_id';
    const FILTER_ID     =   'filter_id';
    const CATEGORY_ID   =   'category_id';
    const IS_SHOW   =   'is_show';
    const TITLE =   'title';
    const SLUG =   'slug';

}
