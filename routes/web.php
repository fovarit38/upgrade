<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

$locale = Request::segment(1);
$lang = true;

Route::group(['prefix' => url_routes() . '/admin/'], function () {
    Auth::routes();



    Route::get('', 'admin\MainController@index');
    Route::get('dev/{dev_status}', 'admin\MainController@developer');


    Route::get('table', 'admin\TablesController@index');



    Route::get('recruitment', 'admin\MainController@recruitment');
    Route::get('recruitment/work', 'admin\MainController@recruitment_work');
    Route::get('recruitment/job', 'admin\MainController@recruitment_job');
    Route::get('recruitment/order', 'admin\MainController@recruitment_order');
    Route::get('recruitment/candidates', 'admin\MainController@recruitment_candidates');
    Route::get('recruitment/podbor', 'admin\MainController@recruitment_podbor');
    Route::get('recruitment/cron', 'admin\MainController@recruitment_cron');
    Route::get('recruitment/npc', 'admin\MainController@recruitment_npc');
    Route::get('recruitment/personala', 'admin\MainController@recruitment_personala');
    Route::get('recruitment/recruting', 'admin\MainController@recruitment_recruting');
    Route::get('recruitment/reglamenti', 'admin\MainController@recruitment_reglamenti');


    Route::get('recruitment/otcperconal', 'admin\MainController@recruitment_otcperconal');


    Route::get('recruitment/bisnes', 'admin\MainController@recruitment_reglmint_bisnes');
    Route::get('recruitment/docent', 'admin\MainController@recruitment_reglmint_docent');
    Route::get('recruitment/tamplase', 'admin\MainController@recruitment_reglmint_tamplase');




    Route::get('recruitment/vakansi', 'admin\MainController@recruitment_vakansi');
    Route::get('recruitment/basa', 'admin\MainController@recruitment_basa');
    Route::get('recruitment/otchet', 'admin\MainController@recruitment_otchet');



    Route::get('trating', 'admin\MainController@trating');


    Route::get('adaptacia', 'admin\MainController@adaptacia');

    Route::get('adaptacia/work', 'admin\MainController@adaptacia_new_work');
    Route::get('adaptacia/organiz', 'admin\MainController@adaptacia_new_organiz');
    Route::get('adaptacia/commu', 'admin\MainController@adaptacia_new_communucat');
    Route::get('adaptacia/material', 'admin\MainController@adaptacia_new_material');


    Route::get('ocenka', 'admin\MainController@ocenka');


    Route::get('kadrovoe', 'admin\MainController@kadrovoe');

    Route::get('kadrovoe/base', 'admin\MainController@kadrovoe_base');
    Route::get('kadrovoe/s_', 'admin\MainController@kadrovoe_s_');
    Route::get('kadrovoe/document', 'admin\MainController@kadrovoe_document');
    Route::get('kadrovoe/commandirovka', 'admin\MainController@kadrovoe_commandirovka');
    Route::get('kadrovoe/lk', 'admin\MainController@kadrovoe_lk');
    Route::get('kadrovoe/time', 'admin\MainController@kadrovoe_time');
    Route::get('kadrovoe/pricas', 'admin\MainController@kadrovoe_pricas');
    Route::get('kadrovoe/otpuska', 'admin\MainController@kadrovoe_otpuska');
    Route::get('kadrovoe/router', 'admin\MainController@kadrovoe_router');


    Route::post('table', 'admin\GenerationController@model_position');


    Route::group(['prefix' => '/test/'], function () {
        Route::post('add', 'admin\MainController@test_add');
        Route::post('add-otvet', 'admin\MainController@test_add_otvet');
    });

    Route::group(['prefix' => 'task/'], function () {
        Route::post('add', 'admin\MainController@task_add');
        Route::post('add-otvet', 'admin\MainController@task_add_otvet');
    });

    Route::group(['prefix' => 'compani/'], function () {
        Route::get('list', 'admin\MainController@compani_list');
        Route::post('add-otvet', 'admin\MainController@task_add_otvet');
    });

    Route::post('users/update', 'admin\MainController@user_update');

    Route::post('update/position', 'admin\MainController@update_position');


    Route::get('delete/{model}/{id}', 'admin\MainController@delete_fs');


    Route::get('book', 'admin\MainController@book');
    Route::get('personal', 'admin\MainController@personal');
    Route::get('regulations', 'admin\MainController@regulations');
    Route::get('video', 'admin\MainController@video');

    Route::get('library', 'admin\MainController@library');
    Route::get('library/{id}', 'admin\MainController@library_one');

    Route::get('instructions', 'admin\MainController@library');
    Route::get('instructions/{id}', 'admin\MainController@library_one');

    Route::get('courses', 'admin\MainController@courses');
    Route::get('courses/{id}', 'admin\MainController@courses_one');

    Route::get('courses/{id}/create', 'admin\MainController@courses_create');

    Route::get('courses/{id}/start', 'admin\MainController@courses_create');
    Route::get('courses/{id}/start/{code}', 'admin\MainController@courses_start');
    Route::post('courses/{id}/start/{code}/next', 'admin\MainController@courses_next');



    Route::get('courses/check/{code}', 'admin\MainController@courses_checktest');
    Route::get('courses/check/{code}/bonus', 'admin\MainController@courses_bonus');
    Route::post('courses/check/add', 'admin\MainController@courses_bonus_add');



    Route::post('courses/{id}/point', 'admin\MainController@courses_point');


    Route::get('permission', 'admin\MainController@permission');

    Route::get('users-1s', 'admin\MainController@users_1s');

    Route::get('users-1s', 'admin\MainController@users_1s_list');

    Route::get('users', 'admin\MainController@users');
    Route::get('users/{id}', 'admin\MainController@users_one');
    Route::get('users/{id}/edit', 'admin\MainController@users_one_edit');


    Route::get('courses_list', 'admin\MainController@courses_list');
    Route::get('setting', 'admin\MainController@setting');


    Route::get('courses/add/info/{id}', 'admin\MainController@courses_info');
    Route::get('courses/add/school/{id}', 'admin\MainController@courses_school');
    Route::get('courses/add/exam/{id}', 'admin\MainController@courses_exam');
    Route::get('courses/add/collaborator/{id}', 'admin\MainController@courses_collaborator');


    Route::post('courses/add/users', 'admin\MainController@courses_add_user');


    Route::get('plans', 'admin\MainController@plans');

    Route::post('itemadd', 'admin\MainController@itemadd');


    Route::get('courses_check', 'admin\MainController@courses_check');

    Route::post('table_meta_update', 'admin\TablesController@table_meta_update');
    Route::get('table/{model_name}', 'admin\TablesController@edit');
    Route::get('column_name', 'admin\TablesController@column');
    Route::get('column_name/edit/{id}', 'admin\TablesController@column_edit');
    Route::post('update', 'admin\UpdateController@update_model');

    Route::group(['prefix' => 'orders/'], function () {
        Route::post('edit/{id}/updateStatus', 'admin\OrderController@status_set');
    });


    Route::post('model/{model_name}', 'admin\GenerationController@model_position');
    Route::get('model/{model_name}', 'admin\GenerationController@model_catalog');
    Route::get('model/{model_name}/{id}', 'admin\GenerationController@model_save');

    Route::get('StaticText', 'admin\MainController@s_text');
    Route::get('ecp', 'admin\MainController@ecp');
    Route::post('update_text', 'admin\MainController@update_text');
    Route::get('user/ban/{id}/{ban}', 'admin\MainController@user_ban');
    Route::get('user/orgshema', 'admin\MainController@orgshema');
    Route::post('ecp/post', 'admin\MainController@ecp_post');
});


Route::group(['prefix' => url_routes() . '/'], function () {
    Auth::routes();
    Route::get('/authorization/{_token}', 'client\MainController@token_create');
    Route::get('', 'client\MainController@pre');
    Route::get('index', 'client\MainController@index');
    Route::get('contact', 'client\MainController@contact');
    Route::get('gallery', 'client\MainController@gallery');
    Route::get('about', 'client\MainController@about');
    Route::get('news/{id}', 'client\MainController@news');
    Route::post('/send_se', 'client\MainController@send_se');
});
