<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCitiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cities', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name');
            $table->string('name_key');
            $table->string('sites_name_key');
            $table->string('coordinates')->default("");
            $table->integer('sort');
            $table->integer('visable')->default(0);
            $table->timestamps();
        });

        \App\City::create([
            'name' => 'Алматы',
            'name_key' => 'almaty',
            'sort' => '0',
            'visable' => 1,
            'sites_name_key' => "mello_kz",
        ]);

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cities');
    }
}
