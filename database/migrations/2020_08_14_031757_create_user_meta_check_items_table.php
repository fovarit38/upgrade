<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUserMetaCheckItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_meta_check_items', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('boolead')->default(0);
            $table->string('importance')->nullable();
            $table->integer('userMetaCheck_id');
            $table->integer('userMetaCheckItem_id')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_meta_check_items');
    }
}
