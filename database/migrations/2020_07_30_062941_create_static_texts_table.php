<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateStaticTextsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('static_texts', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name_key')->nullable();
            $table->text('content')->nullable();
            $table->string('page')->nullable();
            $table->timestamps();
        });
        $model =  \App\Model_list::insert(
            [
                ["name_key"=>"StaticText","name"=>"Текста"],
            ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('static_texts');
    }
}
