<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUserMetaChecksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_meta_checks', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('code');
            $table->text('name');
            $table->integer('course_id');
            $table->integer('user_id');
            $table->integer('re-publish')->default(0);
            $table->string('status')->default("progressing");
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_meta_checks');
    }
}
